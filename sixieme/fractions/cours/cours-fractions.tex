\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Fractions -- Leçon}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{\thesection\arabic{subsection}.}
\renewcommand{\thesubsubsection}{\thesubsection\alph{subsubsection}.}

% Divisions
% =========
%
% Objectifs
% ---------
%
% 1. Représenter des partages à l'aide de fractions
% 2. Modifier l'écriture d'une fraction
% 3. Calculer la fraction d'une quantité
%
% Contenus
% --------
% 
% [ ] Définition d'une fraction
%     [ ] Numérateur
%     [ ] Dénominateur
%     [ ] Barre de fraction
% [ ] Définition de soustraction / différence
%
% Attendus fin d'année 6e
% -----------------------
%
% Voir le doc. de Patrick, qui est très bien. (TODO: le transcrire ici...)


\title{Leçon 7 : Fractions}
\date{}
\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Vocabulaire}

\begin{defi}{Fraction, numérateur, dénominateur}
  Une \textbf{fraction}, que l'on note 
  \[
    \frac{a}{b},
  \]
  est constituée de deux nombres entiers $a$ et $b$, avec $b$ différent de
  zéro, que l'on sépare par une barre.
  \begin{itemize}
    \item Le nombre $a$, au-dessus de la barre de fraction, s'appelle le
      \textbf{numérateur}.
    \item Le nombre $b$, en-dessous de la barre de fraction, s'appelle le
      \textbf{dénominateur}.
  \end{itemize}
\end{defi}

\begin{exemple}
  Voici des exemples de fractions.
  \[
    \frac{1}{3};\;
    \frac{2}{7};\;\frac{11}{42};\;\frac{1993}{123456};\;\frac{0}{5};\;\frac{150}{10};\;\frac{7}{1};\;\frac{42}{11};\;\ldots
  \]
\end{exemple}

\begin{regle}
  Pour lire une fraction, on lit d'abord le nombre du numérateur puis le nombre
  du dénominateur en ajoutant le suffixe « ièmes ». À l'exception des
  dénominateurs $b=2$, $b=3$ et $b=4$ qui se lisent respectivement « demi », «
  tiers », et « quarts ».
\end{regle}

\begin{exemple}
  \begin{multicols}{2}
  \begin{itemize}[label=$\bullet$]
    \item La fraction $\displaystyle\frac{3}{7}$ se lit « trois septièmes ».
    \item La fraction $\displaystyle\frac{5}{2}$ se lit « cinq demi ».
    \item La fraction $\displaystyle\frac{2}{3}$ se lit « deux tiers ».
    \item La fraction $\displaystyle\frac{4}{4}$ se lit « quatre quarts ».
  \end{itemize}
  \end{multicols}
\end{exemple}

\section{Fractions et partages}

\begin{prop}
  Une fraction $\displaystyle\frac{a}{b}$ peut être utilisée pour représenter un
  partage. Le  dénominateur $b$ représente le nombre de parties en laquelle la
  quantité a été partagée. Le numérateur $a$ représente le nombre de parties que
  l'on considère.
\end{prop}

\begin{exemple}
  La bande ci-dessous a été partagée en $7$ morceaux égaux. Chaque morceau
  représente le septième de la bande, que l'on écrit $\dfrac{1}{7}$. La partie
  colorée représente $\dfrac{3}{7}$.
  \begin{center}
    \begin{tikzpicture}
      \draw (0,0) -- (14,0) -- (14, 1) -- (0,1) -- (0,0);
      \foreach \x in {1, 2, ..., 6}{
        \draw (2*\x, 0) -- (2*\x, 1);
      }
      \foreach \x in {0, 1, 2}{
        \fill[red, opacity=.25] (2*\x, 0) -- (2*\x, 1) -- (2*\x+2, 1) --
        (2*\x+2, 0);
      }
    \end{tikzpicture}
  \end{center}
\end{exemple}

\begin{app}
  À côté de chacune des figures suivantes, donner la fraction qui représente la
  partie colorée.
  \vspace{-2mm}
  \begin{center}
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 5}{
      \draw (0,0) -- (\x*360/5:2);
    }
    \foreach \x in {0, 1}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/5:2) arc (\x*360/5:\x*360/5+360/5:2);
    }

  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 11}{
      \draw (0,0) -- (\x*360/11:2);
    }
    \foreach \x in {0, 1, ..., 6}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/11:2) arc
      (\x*360/11:\x*360/11+360/11:2);
    }
  \end{tikzpicture}
  \hfill
  \begin{tikzpicture}
    \draw (0,0) -- (4, 0) -- (4, 4) -- (0, 4) -- (0, 0);
    \draw (2,0) -- (2, 4) (0, 2) -- (4, 2);
    \fill[red, opacity=.25] (0,0) --++(2,0) --++(0,2) --++(-2,0);
    \fill[red, opacity=.25] (2,0) --++(2,0) --++(0,2) --++(-2,0);
    \fill[red, opacity=.25] (2,2) --++(2,0) --++(0,2) --++(-2,0);
  \end{tikzpicture}
  \end{center}
\end{app}

\begin{exemple}
  \label{exemple:sup1}
  Les fractions dont le numérateur est supérieur au dénominateur correspondent à
  une quantité dont on a plus d'une unité. Ci-dessous on a représenté la
  fraction $\dfrac{8}{5}$.
  \vspace{-2mm}
  \begin{center}
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 5}{
      \draw (0,0) -- (\x*360/5:2);
    }
    \foreach \x in {0, 1, 2, 3, 4}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/5:2) arc (\x*360/5:\x*360/5+360/5:2);
    }
    \draw (5,0) circle (2cm);
    \foreach \x in {1, 2, ..., 5}{
      \draw (5,0) --++ (\x*360/5:2);
    }
    \foreach \x in {0, 1, 2}{
      \fill[red, opacity=.25] (5,0) --++ (\x*360/5:2) arc (\x*360/5:\x*360/5+360/5:2);
    }
  \end{tikzpicture}
%  \vspace{-2mm}
\end{center}
\end{exemple}

\section{Fraction et nombres}
\subsection{Une fraction vue comme un nombre}

\begin{defi}{Nombre fraction}
  \newcommand{\fa}{\textcolor{red}{a}}
  \newcommand{\fb}{\textcolor{blue}{b}}
  La fraction $\dfrac{\fa}{\fb}$ est le résultat du quotient de $\fa$ par
  $\fb$. Autrement dit, il s'agit du nombre qui, multiplié par $\fb$, donne
  $\fa$. Ou encore 
  \[
    \frac{\fa}{\fb}\times\fb = \fa.
  \]
\end{defi}

\begin{exemple}
  \hspace{-3mm}
  \begin{tabular}{ll}
  Une unité : &
  \begin{tikzpicture}
    \draw (0,0)--++(3,0)--++(0,.5)--++(-3,0)--++(0,-.5);
    \fill[red, opacity=.25] (0,0)--++(3,0)--++(0,.5)--++(-3,0);
  \end{tikzpicture}.\\
  Quatre unités : &
  \begin{tikzpicture}
    \foreach \x in {0, 1, 2, 3}{
    \draw (\x*3,0)--++(3,0)--++(0,.5)--++(-3,0)--++(0,-.5);
    \fill[red, opacity=.25] (\x*3,0)--++(3,0)--++(0,.5)--++(-3,0);
  }
  \end{tikzpicture}\\
  $\dfrac{4}{3}$ d'unités : &
  \begin{tikzpicture}
    \foreach \x in {0, 1, 2, 3}{
    \draw (\x,0)--++(1,0)--++(0,.5)--++(-1,0)--++(0,-.5);
    \fill[blue, opacity=.25] (\x,0)--++(1,0)--++(0,.5)--++(-1,0);
  }
  \end{tikzpicture}\\
  $3\times\dfrac{4}{3}$ d'unités : &
  \begin{tikzpicture}
    \foreach \x in {0, 1, 2}{
    \draw (\x*4,0)--++(4,0)--++(0,.5)--++(-4,0)--++(0,-.5);
    \fill[blue, opacity=.25] (\x*4,0)--++(4,0)--++(0,.5)--++(-4,0);
  }
  \end{tikzpicture}
  \end{tabular}\\
  On retrouve bien l'égalité $\dfrac{4}{3}\times3=4$.
\end{exemple}

\begin{rmq}
  Certains nombres ne peuvent pas s'écrire sous forme décimale, c'est le cas par
  exemple du quotient de la division de $1$ par $3$. On écrit alors
    $\dfrac{1}{3}$ à la place de $0,333333333333\ldots$.
\end{rmq}

\subsection{Comparaison d'une fraction à $1$}

\begin{prop}
Il y a $3$ cas possibles :
\begin{itemize}[label=$\bullet$]
  \item Si le numérateur est \textbf{inférieur} au dénominateur, alors la
    fraction est \textbf{inférieure} à $1$.
  \item Si le numérateur est \textbf{égal} au dénominateur, alors la
    fraction est \textbf{égale} à $1$.
  \item Si le numérateur est \textbf{supérieur} au dénominateur, alors la
    fraction est \textbf{supérieure} à $1$.
\end{itemize}
\end{prop}

\begin{exemple}
  On a par exemple $\dfrac{8}{5}>1$. On retrouve ce qu'on a vu à
  l'exemple~\ref{exemple:sup1}.
  \vspace{-2mm}
  \begin{center}
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 5}{
      \draw (0,0) -- (\x*360/5:2);
    }
    \foreach \x in {0, 1, 2, 3, 4}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/5:2) arc (\x*360/5:\x*360/5+360/5:2);
    }
    \draw (5,0) circle (2cm);
    \foreach \x in {1, 2, ..., 5}{
      \draw (5,0) --++ (\x*360/5:2);
    }
    \foreach \x in {0, 1, 2}{
      \fill[red, opacity=.25] (5,0) --++ (\x*360/5:2) arc (\x*360/5:\x*360/5+360/5:2);
    }
  \end{tikzpicture}
\end{center}
  \vspace{-2mm}
La quantité de disques colorés est \textbf{supérieure} à $1$ disque car on a un
disque entièrement coloré et une partie d'un autre disque qui n'est pas
entièrement coloré.
\end{exemple}

\begin{exemple}
  On a $\dfrac{9}{13}<1$ et $\dfrac{7}{7}=1$, ce que l'on peut représenter
  par les figures ci-dessous.
  \vspace{-2mm}
  \begin{center}
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 13}{
      \draw (0,0) -- (\x*360/13:2);
    }
    \foreach \x in {0, 1, ..., 8}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/13:2) arc
      (\x*360/13:\x*360/13+360/13:2);
    }
    \draw (5,0) circle (2cm);
    \foreach \x in {1, 2, ..., 7}{
      \draw (5,0) --++ (\x*360/7:2);
    }
    \foreach \x in {0, 1, ..., 6}{
      \fill[red, opacity=.25] (5,0) --++ (\x*360/7:2) arc (\x*360/7:\x*360/7+360/7:2);
    }
  \end{tikzpicture}
  \vspace{-2mm}
\end{center}
  Le disque de gauche n'est pas complètement coloré, la quantité colorée est
  donc \textbf{inférieure} à $1$ disque. Le cercle de droite est complètement
  coloré, on a donc exactement $1$ disque coloré.
\end{exemple}

\subsection{Encadrement d'une fraction}

\begin{prop}
  Pour encadrer une fraction (vue comme un nombre) entre deux entiers, on
  effectue la \textbf{division euclidienne} du numérateur par le dénominateur.
  On obtient un quotient entier qui correspond au plus grand entier inférieur à
  la fraction.
\end{prop}
\begin{exemple}
  \label{exemple:encad}
  Pour encadrer la fraction $\dfrac{39}{7}$, on effectue la division euclidienne
  de $39$ par $7$ et on trouve un quotient entier de $5$. On a alors
  $5<\dfrac{39}{7}<6$.
\end{exemple}

\subsection{Décomposition d'une fraction}

\begin{prop}
  Toute fraction peut se décomposer en la somme d'un entier et d'une fraction
  inférieure à $1$.
\end{prop}
\begin{exemple}
  On reprend l'exemple~\ref{exemple:encad} avec la fraction $\dfrac{39}{7}$. Le
  reste de la division euclidienne de $39$ par $7$ est $4$, en effet
  $39=5\times7+4$. On a ainsi $\dfrac{39}{7} = 5+\frac{4}{7}$. On peut
  représenter la fraction $\dfrac{39}{7}$ comme suit.
  \begin{center}
    \begin{tikzpicture}
      \foreach \c in {0, 1, ..., 4}{
        \draw (\c*2.7,0) circle (1.2cm);
    \foreach \x in {1, 2, ..., 7}{
      \draw (\c*2.7,0) --++ (\x*360/7:1.2);
    }
    \foreach \x in {0, 1, ..., 6}{
      \fill[red, opacity=.25] (\c*2.7,0) --++ (\x*360/7:1.2) arc
      (\x*360/7:\x*360/7+360/7:1.2);
    }
  }
      \foreach \c in {5}{
        \draw (\c*2.7,0) circle (1.2cm);
    \foreach \x in {1, 2, ..., 7}{
      \draw (\c*2.7,0) --++ (\x*360/7:1.2);
    }
    \foreach \x in {0, 1, ..., 3}{
      \fill[red, opacity=.25] (\c*2.7,0) --++ (\x*360/7:1.2) arc
      (\x*360/7:\x*360/7+360/7:1.2);
    }
      }
    \end{tikzpicture}
  \end{center}
\end{exemple}

\section{Fractions et additions}

\begin{prop}
  Pour additionner deux fractions qui ont le \textbf{même dénominateur}, on
  additionne les numérateurs et on laisse le dénominateur inchangé.
\end{prop}

\begin{exemple}
  \begin{minipage}{.5\textwidth}
  On a par exemple $\dfrac{1}{3}+\dfrac{1}{3}=\dfrac{2}{3}$.
  \begin{center}
    \begin{tikzpicture}[scale=.8]
    \draw (0,0) circle (1cm);
    \foreach \x in {1, 2, ..., 3}{
      \draw (0,0) -- (\x*360/3:1);
    }
    \foreach \x in {0}{
      \fill[red, opacity=.25] (0,0) --++ (\x*360/3:1) arc
      (\x*360/3:\x*360/3+360/3:1);
    }

    \node at (1.5,0) {\Huge$+$};

    \draw (3,0) circle (1cm);
    \foreach \x in {1, 2, ..., 3}{
      \draw (3,0) --++ (\x*360/3:1);
    }
    \foreach \x in {0}{
      \fill[red, opacity=.25] (3,0) --++ (\x*360/3:1) arc
      (\x*360/3:\x*360/3+360/3:1);
    }

    \node at (4.5,0) {\Huge$=$};

    \draw (6,0) circle (1cm);
    \foreach \x in {1, 2, ..., 3}{
      \draw (6,0) --++ (\x*360/3:1);
    }
    \foreach \x in {0,1}{
      \fill[red, opacity=.25] (6,0) --++ (\x*360/3:1) arc
      (\x*360/3:\x*360/3+360/3:1);
    }
  \end{tikzpicture}
\end{center}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    On a aussi $\dfrac{3}{4}+\dfrac{2}{4}=\dfrac{5}{4}$.
  \begin{center}
    \begin{tikzpicture}[scale=.8]
    \draw (0,0) circle (1cm);
    \foreach \x in {1, 2, ..., 4}{
      \draw (0,0) -- (\x*360/4:1);
    }
    \foreach \x in {0, 1, 2}{
      \fill[red, opacity=.25] (0,0) --++ (\x*360/4:1) arc
      (\x*360/4:\x*360/4+360/4:1);
    }

    \node at (1.5,0) {\Huge$+$};

    \draw (3,0) circle (1cm);
    \foreach \x in {1, 2, ..., 4}{
      \draw (3,0) --++ (\x*360/4:1);
    }
    \foreach \x in {0, 1}{
      \fill[red, opacity=.25] (3,0) --++ (\x*360/4:1) arc
      (\x*360/4:\x*360/4+360/4:1);
    }

    \node at (4.5,0) {\Huge$=$};

    \draw (6,0) circle (1cm);
    \foreach \x in {1, 2, ..., 4}{
      \draw (6,0) --++ (\x*360/4:1);
    }
    \foreach \x in {0,1, 2, 3}{
      \fill[red, opacity=.25] (6,0) --++ (\x*360/4:1) arc
      (\x*360/4:\x*360/4+360/4:1);
    }
    \draw (8.2,0) circle (1cm);
    \foreach \x in {1, 2, ..., 4}{
      \draw (8.2,0) --++ (\x*360/4:1);
    }
    \foreach \x in {0}{
      \fill[red, opacity=.25] (8.2,0) --++ (\x*360/4:1) arc
      (\x*360/4:\x*360/4+360/4:1);
    }

  \end{tikzpicture}
\end{center}
  \end{minipage}
\end{exemple}

\begin{app}
  Donner les résultats des additions suivantes.
  \begin{align*}
    \textbf{a)}\;& \frac{2}{3}+\frac{5}{3} = \reponse{1cm} &
    \textbf{b)}\;& \frac{4}{11}+\frac{1}{11} = \reponse{1cm} &
    \textbf{c)}\;& \frac{39}{113}+\frac{100}{113} = \reponse{1cm} &
    \textbf{d)}\;& \frac{32}{2}+\frac{27}{2} = \reponse{1cm} 
  \end{align*}
\end{app}

\section{Fraction et demi-droite graduée}


\begin{exemple}
Sur la droite graduée ci-dessous, on a partagé le segment qui représente l'unité
en cinq parties égales. Chaque partie représente donc $\dfrac{1}{5}$ de l'unité.
Le point $N$ a pour \textbf{abscisse} $\dfrac{7}{5}$.
  \begin{center}
    \begin{tikzpicture}
      \draw[-latex] (0,0)--(16,0); 
      \foreach \x in {0,1,...,15}{
        \draw (\x,-.1) --++(0,.2) node[above]{$\frac{\x}{5}$};
      }
      \foreach \x in {0,1,...,3}{
        \draw (\x*5,-.2) --++(0,.4);
        \node at (\x*5, -.5) {$\x$};
      }
      \coordinate (N) at (7,0);
      \drawCrossP N;
      \node at (7,-.5) {$N$};
    \end{tikzpicture}
  \end{center}
\end{exemple}

\begin{app}
  Donner l'abscisse des points sur la droite graduée ci-dessous.
  \begin{center}
    \begin{tikzpicture}
      \draw[-latex] (0,0)--(16,0); 
      \foreach \x in {0,1,...,15}{
        \draw (\x,-.1) --++(0,.2);
      }
      \foreach \x in {0,1,...,5}{
        \draw (\x*3,-.2) --++ (0,.4) node[above] {$\x$};
      }
      
      \coordinate (A) at (7,0);
      \drawCrossP A;
      \node at (7,-.5) {$A$};

      \coordinate (B) at (2,0);
      \drawCrossP B;
      \node at (2,-.5) {$B$};

      \coordinate (C) at (11,0);
      \drawCrossP C;
      \node at (11,-.5) {$C$};

      \coordinate (D) at (15,0);
      \drawCrossP D;
      \node at (15,-.5) {$D$};
    \end{tikzpicture}
  \end{center}
\end{app}

\section{Fractions égales}

\begin{prop}
  Une fraction ne change pas lorsqu'on multiplie (ou divise) le numérateur et le
  dénominateur par un même nombre non nul.
\end{prop}
\begin{exemple}
  On a par exemple $\dfrac{3}{4}=\dfrac{6}{8}=\dfrac{9}{12}$. C'est ce qu'on
  observe aussi dans les figure ci-dessous.
  \begin{center}
  \begin{tikzpicture}
    \draw (0,0) circle (2cm);
    \foreach \x in {1, 2, ..., 4}{
      \draw (0,0) -- (\x*360/4:2);
    }
    \foreach \x in {0, 1, ..., 2}{
      \fill[red, opacity=.25] (0,0) -- (\x*360/4:2) arc
      (\x*360/4:\x*360/4+360/4:2);
    }
    \draw (5,0) circle (2cm);
    \foreach \x in {1, 2, ..., 8}{
      \draw (5,0) --++ (\x*360/8:2);
    }
    \foreach \x in {0, 1, ..., 5}{
      \fill[red, opacity=.25] (5,0) --++ (\x*360/8:2) arc (\x*360/8:\x*360/8+360/8:2);
    }
    \draw (10,0) circle (2cm);
    \foreach \x in {1, 2, ..., 12}{
      \draw (10,0) --++ (\x*360/12:2);
    }
    \foreach \x in {0, 1, ..., 8}{
      \fill[red, opacity=.25] (10,0) --++ (\x*360/12:2) arc
      (\x*360/12:\x*360/12+360/12:2);
    }
  \end{tikzpicture}
\end{center}

\end{exemple}
\begin{app}
  Remplir les pointillés avec le symbole $=$ ou $\neq$.
  \begin{align*}
    \textbf{1.}\;& \frac{1}{3}\reponse{1cm}\frac{2}{6} &
    \textbf{2.}\;& \frac{4}{7}\reponse{1cm}\frac{10}{21} &
    \textbf{3.}\;& \frac{2}{5}\reponse{1cm}\frac{14}{35} &
    \textbf{4.}\;& \frac{14}{8}\reponse{1cm}\frac{7}{4}
  \end{align*}
\end{app}

\section{Calculer la fraction d'une quantité}

\begin{prop}
  Prendre la fraction d'une quantité revient à multiplier cette quantité par la
  fraction en question.
\end{prop}
\begin{exemple}
  Prendre les deux cinquièmes de $300$ revient à calculer $\dfrac{2}{5}\times300$. On
  a trois façons de faire.
  \begin{align*}
    \frac{2}{5}\times300 &= 2\times\frac{300}{5} &
    \frac{2}{5}\times300 &= \frac{2\times300}{5} &
    \frac{2}{5}\times300 &= 0,4\times300 \\
    &= 2\times60 &
    &= \frac{600}{5} &
    &= 120 \\
    &= 120 &
    &= 120
  \end{align*}
\end{exemple}
\begin{app}
  Combien font les trois quarts de $20$ euros ?
\end{app}

\end{document}
