\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Limites de suites -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{1.5cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}
\renewcommand{\P}{\mathcal P}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% Approche intuitive de
%
% [x] la notion de limite, finie ou infinie, d’une suite
% [x] des opérations sur les limites
% [x] du passage à la limite dans les inégalités
% [x] et du théorème des gendarmes.
%
% et aussi
%
% [x] limite d'une suite géométrique de raison positive
% [x] limite de la somme des termes d’une suite géométrique de raison positive
%     strictement inférieure à 1
% [x] suites arithmético-géométrique
%
% Capacités attendues
% -------------------
% 
% + Modéliser un problème par une suite donnée par une formule explicite ou une
%   relation de récurrence.
% + Calculer une limite de suite géométrique, de la somme des termes d’une suite
%   géométrique de raison positive et strictement inférieure à 1.
% + Représenter graphiquement une suite donnée par une relation de récurrence
%   un+1 = f(un) où f est une fonction continue d’un intervalle I dans lui-même.
%   Conjecturer le comportement global ou asymptotique d’une telle suite.
% + Pour une récurrence arithmético-géométrique : recherche d’une suite
%   constante solution particulière ; utilisation de cette suite pour déterminer
%   toutes les solutions.
%
% Démonstrations
% --------------
%
% + Limite des sommes des termes d’une suite géométrique de raison positive
%   strictement inférieure à 1.
%
% Exemples d'algorithme
% ---------------------
%
% [ ] Recherche de seuils
% [ ] Recherche de valeurs approchées de pi, e, \sqrt 2, \ln 2, *etc.*
% [ ] Pour une suite récurrente un+1 = f(un), calcul des termes successifs.

\title{\vspace{-20mm}Chapitre 1 : Limites de suites}
\date{\vspace{-14mm}
\href{https://erou.forge.apps.education.fr/math-comp/limites-suites.html}{
  \includegraphics[scale=.6]{qrcode.png}}
\vspace{-14mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Notion de limite}

\begin{intuition}
  On va s'intéresser au comportement des termes $u_n$ quand $n$ est très grand.
\end{intuition}

\begin{defi}{Suite ayant pour limite un nombre réel}
  Une suite $(u_n)$ \textbf{a pour limite un réel} $\ell$ quand $n$ tend vers $+\infty$,
 si les termes $u_n$ deviennent tous aussi proches de $\ell$ que l'on veut en
 prenant $n$ suffisamment grand.\\
 On dit alors que $(u_n)$ \textbf{converge} vers
  $\ell$ et on note
  \[
    \lim_{n\to+\infty}u_n=\ell.
  \]
\end{defi}

\noindent\begin{minipage}{.495\textwidth}
\begin{exemple}
  La suite $(u_n)$ définie sur $\mathbb{N}$ par $u_n=\frac{1}{n+1}$ converge
  vers $0$.
\end{exemple}
\end{minipage}
\hfill
\begin{minipage}{.495\textwidth}
\begin{exemple}
  La suite $(u_n)$ définie sur $\mathbb{N}$ par $u_n=2+\frac{1}{3^n}$ converge
  vers $2$.
\end{exemple}
\end{minipage}

\begin{defi}{Suite ayant pour limite l'infini}
  Une suite $(u_n)$ \textbf{a pour limite} $+\infty$ (respectivement $-\infty$)
  quand $n$ tend vers $+\infty$,  si les termes $u_n$ deviennent tous aussi
  grands (respectivement petits) l'on veut en prenant $n$ suffisamment grand.\\
 On dit alors que $(u_n)$ \textbf{diverge} et on note
  \[
    \lim_{n\to+\infty}u_n=+\infty\text{ (respectivement
    }\lim_{n\to+\infty}u_n=-\infty\text{)}.
  \]
\end{defi}

\noindent\begin{minipage}{.495\textwidth}
\begin{exemple}
  La suite $(u_n)$ définie sur $\mathbb{N}$ par $u_n=n$ diverge vers $+\infty$.
\end{exemple}
\end{minipage}
\hfill
\begin{minipage}{.495\textwidth}
\begin{exemple}
  La suite $(u_n)$ définie sur $\mathbb{N}$ par $u_n=-(n^2+n+1)$ diverge 
  vers $-\infty$.
\end{exemple}
\end{minipage}

\section{Propriété des limites}
\subsection{Quelques limites de référence}
\begin{prop}
  Les suites $(\sqrt n)_{n\in\mathbb{N}}$, $(n)_{n\in\mathbb{N}}$ et
  $(n^k)_{n\in\mathbb{N}}$ où $k\in\mathbb{N}^*$ est un entier non nul tendent
  vers $+\infty$ quand $n$ tend vers $+\infty$.
\end{prop}
\begin{prop}
  Les suites $\left( \cfrac{1}{\sqrt n} \right)_{n\in\mathbb{N}}$, $\left(
  \cfrac{1}{n} \right)_{n\in\mathbb{N}}$ et
  $\left( \cfrac{1}{n^k} \right)_{n\in\mathbb{N}}$ où $k\in\mathbb{N}^*$ est un entier non nul tendent
  vers $0$ quand $n$ tend vers $+\infty$.
\end{prop}
\subsection{Opération sur les limites}
\subsubsection{Limite d'une somme de suites}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants :

\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.9\textwidth}{|c|Y|Y|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell$ & $\ell$ & $+\infty$ &
    $-\infty$ & $+\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ \\
    \hline
    \textbf{alors $(u_n + v_n)$ a pour limite} & $\ell+\ell'$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
où \textbf{F.I.} signifie « Forme Indéterminée » et veut dire qu'il n'est pas
possible de déterminer la limite de façon générale dans ce cas là. Il faut alors
faire au cas par cas.
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=\frac{1}{n}$ et $v_n=n^2$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(u_n+v_n)$.
    \item Déterminer la limite de la suite $\left( 3+\frac{1}{\sqrt n} \right)$.
  \end{enumerate}
\end{app}

\subsubsection{Limite d'un produit de suites}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.

\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell>0$ & $\ell>0$ & $\ell < 0$ &
    $\ell<0$ & $+\infty$ & $+\infty$ & $-\infty$ & $0$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & $-\infty$ & $\pm\infty$ \\
    \hline
    \textbf{alors $(u_n \times v_n)$ a pour limite} & $\ell\times\ell'$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ & $+\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=\frac{1}{n}-2$ et $v_n=\sqrt n$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(u_n\times v_n)$.
    \item Déterminer la limite de la suite $(-1\times n)$.
  \end{enumerate}
\end{app}

\subsubsection{Limite d'un quotient de suites}
\begin{notation}
  Soit $(u_n)$ une suite. On note
  \[
    \lim_{n\to+\infty}(u_n) = 0^+ \quad\text{(respectivement
    }\lim_{n\to+\infty}(u_n)=0^-\text{)}
  \]
  lorsque $\lim_{n\to+\infty}(u_n)=0$ et $u_n>0$ (respectivement $u_n<0$) à partir d'un
  certain rang.
\end{notation}
\begin{exemple}
  On a $\lim_{n\to+\infty}\left( \frac{1}{n} \right)=0^+$ et
  $\lim_{n\to+\infty}\left( \frac{-1}{n} \right)=0^-$. Mais
  $\lim_{n\to+\infty}\left( \frac{(-1)^n}{n} \right)=0$.
\end{exemple}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites telles que $(v_n)$ ne s'annule jamais et
  soient $\ell, \ell'\in\mathbb{R}$ deux réels. On a alors les résultats
  suivants.
  \vspace{2mm}
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell$ & $+\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'\neq0$ & $\pm\infty$ & $\ell'>0$ &
    $\ell'<0$ & $\ell'>0$ & $\ell'<0$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{alors $\left( \frac{u_n}{v_n} \right)$ a pour limite} &
    $\frac{\ell}{\ell'}$ & $0$ & $+\infty$ & $-\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} & \textbf{F.I.} \\
    \hline
  \end{tabularx}\vspace{2mm}
  \begin{tabularx}{.98\textwidth}{|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell>0$ ou $+\infty$ & $\ell>0$ ou
    $+\infty$ & $\ell<0$ ou $-\infty$ & $\ell<0$ ou $-\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $0^+$ & $0^-$ & $0^+$ & $0^-$ \\
    \hline
    \textbf{alors $\left( \frac{u_n}{v_n} \right)$ a pour limite} & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ \\
    \hline
  \end{tabularx}

\end{center}
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=n^3$ et $v_n=-5+\frac{1}{n}$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(\frac{u_n}{v_n})$.
    \item Déterminer la limite de la suite $w$ définie par
      $w_n=\cfrac{\frac{5}{n}+7}{8+\frac{2}{n}}$.
  \end{enumerate}
\end{app}

\begin{app}
  Soit $(u_n)$ la suite définie sur $\mathbb{N}$ par $u_n = n^2 - n$.
  \begin{enumerate}
    \item Peut-on déterminer la limite de la suite $(u_n)$ en utilisant les
      opérations sur les limites ?
    \item \begin{enumerate}
        \item Factoriser l'expression $u_n=n^2-n$.
        \item En déduire la limite de la suite $u$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{app}
  Soit $(u_n)$ la suite définie sur $\mathbb{N}$ par $u_n =
  \cfrac{4n^2}{n+1}$.
  \begin{enumerate}
    \item Peut-on déterminer la limite de la suite $(u_n)$ en utilisant les
      opérations sur les limites ?
    \item \begin{enumerate}
        \item Mettre en facteur $n$ au dénominateur de $u_n$ et prouver que
          $u_n=\cfrac{4n}{1+\frac{1}{n}}$.
        \item En déduire la limite de la suite $u$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{methode}
  Pour lever une indéterminée, on peut factoriser ou (plus rarement) développer.
  Dans un \textbf{quotient}, on factorise le numérateur et le dénominateur par
  le terme de plus haut degré.
\end{methode}

\subsection{Théorèmes de comparaison}

\begin{thm}
  Soient $(u_n)$ et $(v_n)$ deux suites. On suppose qu'il existe un entier $N$
  tel pour tout $n>N$, on a $u_n\leq v_n$.
  \begin{itemize}
    \item Si $\lim_{n\to+\infty}(u_n) = +\infty$, alors
      $\lim_{n\to+\infty}(v_n)=+\infty$.
    \item Si $\lim_{n\to+\infty}(v_n) = -\infty$, alors
      $\lim_{n\to+\infty}(u_n)=-\infty$.
  \end{itemize}
\end{thm}

\begin{app}
  Déterminer la limite de la suite $(u_n)$ définie sur $\mathbb{N}$ par
  $u_n=n+2\sin(n)$.
\end{app}

\begin{thmnom}{Théorème des gendarmes}
  Soient $(u_n)$, $(v_n)$ et $(w_n)$ trois suites telles que $u_n\leq v_n\leq
  w_n$ à partir d'un certain rang. Si $(u_n)$ et $(w_n)$ convergent vers une
  même limite $\ell$, alors $(v_n)$ converge aussi vers $\ell$.
\end{thmnom}
\begin{app}
  Déterminer $\lim_{n\to+\infty}\left( 1+\cfrac{\sin(n)}{n} \right)$.
\end{app}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites convergentes, telles que $u_n\leq v_n$ à
  partir d'un certain rang. On note $\lim_{n\to+\infty}(u_n)=\ell$ et
  $\lim_{n\to+\infty}(v_n)=\ell'$. Alors $\ell\leq\ell'$.
\end{propadm}

\section{Quelques cas particuliers}
\begin{propnom}{Limite d'une suite géométrique}
  Soit $(u_n)$ une suite géométrique de raison $q\in\mathbb{R^+}$ et de premier
  terme $u_p$ avec $p\in\mathbb{N}$.\\[2mm]
  \begin{minipage}{.45\textwidth}
  \begin{itemize}
    \item Si $0 \leq q < 1$, alors $\lim_{n\to+\infty}u_n = 0$.
    \item Si $q=1$, alors $\lim_{n\to+\infty}(u_n) = u_p$.
  \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}{.55\textwidth}
  \begin{itemize}
    \item Si $q>1$ et $u_p < 0$, alors $\lim_{n\to+\infty}u_n=-\infty$.
    \item Si $q>1$ et $u_p > 0$, alors $\lim_{n\to+\infty}u_n=+\infty$.
  \end{itemize}
  \end{minipage}
\end{propnom}

\begin{app}
  Déterminer la limite des suites dont le terme général, pour $n\in\mathbb{N}$,
  est le suivant.
  \begin{align*}
    \textbf{1.}\;& u_n = 2+\left( \frac{4}{5} \right)^n &
    \textbf{2.}\;& v_n = 2^n-100 &
    \textbf{3.}\;& w_n = \left( \cos(0) - \sin(0) \right)^n
  \end{align*}
\end{app}

\begin{propnom}{Limite de la somme des termes d'une suite géométrique}
  Soit $(u_n)$ une suite géométrique de premier terme $u_p$, avec
  $p\in\mathbb{N}$, et de raison $q$, avec $0\leq q < 1$. Soit $S_n$ la somme
  des $n$ premiers termes de la suite $(u_n)$. On a
  \[
    \lim_{n\to+\infty} S_n=\frac{u_p}{1-q}
  \]
\end{propnom}

\begin{app}
  Soit $(u_n)$ la suite définie par $u_0=5$ et, pour $n\in\mathbb{N}$, par
  $u_{n+1}=\frac{u_n}{3}$. Calculer la limite de la somme des termes de la suite
  $(u_n)$.
\end{app}

\subsection{Les suites arithmético-géométriques}

\begin{defi}{Suite arithmético-géométrique}
  Une suite $(u_n)$ est dite \textbf{arithmético-géométrique} s'il existe
  $a,b\in\mathbb{R}$ deux réels tels que pour tout $n\in\mathbb{N}$, on a
  $u_{n+1}=au_n+b$.
\end{defi}

\begin{prop}
  Soit $(u_n)$ une suite arithmético-géométrique de premier terme $u_p$, avec
  $p\in\mathbb{N}$, et telle que pour tout entier $n\geq p$, on a
  $u_{n+1}=au_n+b$ avec $a\neq1$ et $b\neq0$. Soit $\ell$ le réel tel que
  $\ell=a\ell+b$. Alors la suite $(v_n)$ définie pour tout entier $n\geq p$ par
  $v_n=u_n-\ell$ est une suite géométrique de raison $a$ et de premier terme
  $v_p=u_p-\ell$.
\end{prop}

\begin{app}
  Soit $(u_n)$ la suite définie par $u_0=6$ et pour tout $n\in\mathbb{N}$, par
  $u_{n+1}=3u_n-4$. Déterminer l'expression de $u_n$ en fonction de $n$.
\end{app}

\end{document}
