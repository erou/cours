\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Limites et continuité -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{1.6cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
% 
% [x] Notion de limite. Lien avec la continuité et les asymptotes horizontales
%     ou verticales. Limites des fonctions de référence (carré, cube, racine
%     carrée, inverse, exponentielle, logarithme).
% [x] Théorème des valeurs intermédiaires (admis). Cas des fonctions strictement
%     monotones.
% [ ] Réciproque d’une fonction continue strictement monotone sur un intervalle,
%     représentation graphique.
% [ ] Fonction logarithme népérien : réciproque de la fonction exponentielle.
%     Limites, représentation graphique. Équation fonctionnelle. Fonction
%     dérivée.
% [ ] Fonction dérivée de
%     [ ] x ↦ f(a x + b)
%     [ ] x ↦ exp(u(x))
%     [ ] x ↦ ln(u(x))
%     [ ] x ↦ u(x)^2
%
% Capacités attendues
% -------------------
% 
% [ ] Calculer une fonction dérivée, calculer des limites. Dresser un tableau de
%     variation.
% [ ] Dans le cadre de la résolution de problème, utiliser le calcul des
%     limites, l’allure des courbes représentatives des fonctions inverse, carré,
%     cube, racine carrée, exponentielle et logarithme.
% [ ] Exploiter le tableau de variation pour déterminer le nombre de solutions
%     d'une équation du type f(x) = k, pour résoudre une inéquation du type 
%     f(x) <= k.
% [ ] Déterminer des valeurs approchées, un encadrement d’une solution d’une équation
%     du type f(x) = k.
% [ ] Utiliser l’équation fonctionnelle de l’exponentielle ou du logarithme pour
%     transformer une écriture, résoudre une équation, une inéquation.
% [ ] Utiliser la relation ln(q^n) = n×ln(q) pour déterminer un seuil.
%
% Démonstrations
% --------------
%
% [ ] Relations ln (ab) = ln a + ln b, ln (1/a) = - ln a.
% [ ] Calcul de la fonction dérivée du logarithme, en admettant sa dérivabilité.
% [ ] Calcul de la fonction dérivée de ln u, de exp u.
%
% Exemples d'algorithme
% ---------------------
%
% [ ] Méthodes de recherche de valeurs approchées d’une solution d’équation du type
%     f(x) = k : balayage, dichotomie, méthode de Newton.
% [ ] Algorithme de Briggs pour le calcul de logarithmes.

\title{\vspace{-20mm}Chapitre 3 : Limites et continuité}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/math-comp/limites-continuite.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\begin{intuition}
  On va s'intéresser au comportement de $f(x)$ quand $x$ se rapproche d'un réel
  ou de l'infini.
\end{intuition}

\section{Limite d'une fonction en l'infini}

\noindent Soit $f$ une fonction définie sur $\mathbb{R}$ ou sur un intervalle de la forme
$[a;+\infty[$ ou $]a;+\infty[$ avec $a\in\mathbb{R}$.

\begin{defi}{Limite infinie}
  On dit que $f(x)$ tend vers $+\infty$ quand $x$ tend vers $+\infty$ et on note
  \[
    \lim_{x\to+\infty}f(x) = +\infty
  \]
  si pour tout $A\in\mathbb{R}$, il existe $m\in\mathbb{R}$ tel que si $x\geq m$ alors
  $f(x)\geq A$.\\
  On définit de manière similaire les limites
\begin{align*}
  &\lim_{x\to+\infty}f(x) = -\infty &
  &\lim_{x\to-\infty}f(x) = +\infty &
  &\lim_{x\to-\infty}f(x) = -\infty
\end{align*}
\end{defi}

\begin{intuition}
  Autrement dit, $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$ signifie que la fonction $f$
  prend des valeurs aussi grandes que l'on veut lorsque $x$ grandit.
\end{intuition}

\begin{exemple}
  \begin{minipage}{.5\textwidth}
    Soit $f:x\mapsto\sqrt x$ définie sur $[0;+\infty[$. La fonction $f$ est
      strictement croissante et si $A=100$ par exemple, on peut prendre
      $m=10\,000$. Si $x\geq10\,000$, alors
      \begin{align*}
      \sqrt x &\geq\sqrt{10\,000} \\
      &\geq 100.
      \end{align*}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=15,
          ymax=5]
          \addplot[red, very thick, samples=201, domain=0:15]{sqrt(x)};
        \end{axis}
        \draw[thick, dashed, blue] (0.5,2) node[left]{$A$} -- (8, 2);
        \draw[thick, dashed, green!60!black] (5,0.5) node[below]{$m$} -- (5, 3);
        \fill[opacity=.2, red] (5, 2) -- (8,2) -- (8,3) -- (5, 3);
        \node[thick, red] at (.85, 1.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
  On peut faire le même raisonnement quelque soit la valeur de $A\in\mathbb{R}$,
  on en déduit donc que
  \[
    \lim_{x\to+\infty}f(x) = +\infty.
  \]
\end{exemple}

\begin{defi}{Limite finie en l'infini}%\\[-7mm]
  On dit que $f$ a pour limite $\ell$ quand $x$ tend vers $+\infty$ et on note
  \[
    \lim_{x\to+\infty}f(x) = \ell
  \]
  si pour tout $\varepsilon>0$, il existe $m\in\mathbb{R}$ tel que si $x\geq m$
  alors $\mid f(x) - \ell\mid < \varepsilon$.\\
  On définit de manière similaire $\displaystyle\lim_{x\to-\infty}f(x)=\ell.$
\end{defi}

\begin{intuition}
  \begin{minipage}{.45\textwidth}
  Autrement dit, $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ signifie que la fonction $f$
  prend des valeurs aussi proches de $\ell$ que l'on veut lorsque $x$ grandit.
  Formellement, pour $\varepsilon>0$, il existe $m\in\mathbb{R}$ tel que
  \[
  f(x)\in]\ell-\varepsilon; \ell+\varepsilon[
  \]
  dès que $x\geq m$.
  \end{minipage}
  \begin{minipage}{.55\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=15,
          ymax=5.5]
          \addplot[red, very thick, samples=201,
          domain=0:15]{1+4*cos(deg(x))/(x+1)};
        \end{axis}
        \draw[thick, dashed, blue] (0.5,1) node[left]{$\ell$} -- (8, 1);
        \draw[thick, dashed, green!60!black] (0.5,1.25)
        node[left]{$\ell+\varepsilon$} -- (8,
        1.25);
        \draw[thick, dashed, green!60!black] (0.5,0.75)
        node[left]{$\ell-\varepsilon$} -- (8,
        .75);
        \draw[thick, dashed, orange!90!black] (3.79,0.5) node[below]{$m$} -- (3.79,
        1.25);
        \fill[opacity=.3, orange] (3.79, .75) -- (8,.75) -- (8,1.25) -- (3.79,
        1.25);
        \node[thick, red] at (1, 2.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{intuition}

\begin{exemple}~\\[-3mm]
   \begin{minipage}{.47\textwidth}
   Soit $f$ la fonction définie sur $]0;+\infty[$ par
     \[
       \forall x>0,\,f(x)=\frac{1}{x}.
     \]
     Si on prend $\varepsilon=0,2$, alors pour $m=5$ et $x\geq m$ on a
   $f(x)\in[-0,2;0,2]$. On peut montrer que ce raisonnement se généralise
   quelque soit la
 \end{minipage}
  \begin{minipage}{.53\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.5, xmin=-1, xmax=15,
          ymax=1.5, y=2cm]
          \addplot[red, very thick, samples=201,
          domain=.1:15]{1/x};
        \end{axis}
        \draw[thick, dashed, green!60!black] (0.5,1.4) node[left]{$0,2$} -- (8,1.4);
        \draw[thick, dashed, green!60!black] (0.5,0.6) node[left]{$-0,2$} -- (8,.6);
        \draw[thick, dashed, orange!90!black] (3,0.65) node[below]{$m=5$} -- (3, 4);
        \node[thick, red] at (1, 2.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
   valeur de $\varepsilon>0$, ainsi on a
   $\displaystyle\lim_{x\to+\infty}\left( f(x) \right)=0$.
\end{exemple}

\begin{defi}{Asymptote horizontale}
  Si $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ ou
  $\displaystyle\lim_{x\to-\infty}f(x)=\ell$ alors on dit qu la droite
  d'équation $y=\ell$ est une \textbf{asymptote horizontale} à la courbe
  représentative de la fonction $f$.
\end{defi}

\begin{exemple}
On reprend l'exemple de la fonction $f$ définie sur $]0;+\infty[$ par $\forall
x>0,\,f(x)=\frac{1}{x}$ et on note $\mathscr C_f$ sa courbe représentative.
L'axe des abscisse est une asymptote à $\mathscr C_f$.
\end{exemple}

\section{Limite d'une fonction en une valeur réelle}

\noindent Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ ($a$
peut aussi être une borne de $I$).

\begin{defi}{Limite infinie}
  On dit que $f$ a pour limite $+\infty$ lorsque $x$ tend vers $a$ si, quel que
  soit le réel $A\in\mathbb{R}$, il existe $\delta>0$ tel que pour tout $x\in
  I$, si $\mid x-a\mid<\delta$, alors $f(x)>A$. On note alors
  $\displaystyle\lim_{x\to a}f(x)=+\infty$.\\[-1mm]
  On définit de manière similaire $\displaystyle\lim_{x\to a}f(x) = -\infty$.
\end{defi}

\begin{intuition}~\\[-12mm]
  \begin{minipage}{.7\textwidth}
  Autrement dit, on a $\displaystyle \lim_{x\to a}f(x)=+\infty$ si les valeurs
  de $f(x)$ deviennent aussi grandes que l'on veut dès lors que $x$ se rapproche
  de $a$.
  \end{minipage}
  \begin{minipage}{.3\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-3, xmax=5,
          ymax=5.5]
          \addplot[red, very thick, samples=201,
          domain=-5:.95]{.5*x+.5+.1/((x-1)^2)};
          \addplot[red, very thick, samples=201,
          domain=1.05:5]{.5*x+.5+.1/((x-1)^2)};
        \end{axis}
        \draw[thick, dashed, green!40!black] (2,0.35) node[below]{$a$} -- (2,3.25);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{intuition}

\begin{defi}{Asymptote verticale}
  Lorsque la limite de $f$ en un réel $a$ est $+\infty$ ou $-\infty$, on dit que
  la droite d'équation $x=a$ est une \textbf{asymptote verticale} à la courbe
  représentative de $f$.
\end{defi}

\begin{exemple}
  L'axe des ordonnées est une asymptote verticale à la courbe représentative de
  la fonction inverse.
\end{exemple}

\begin{defi}{Limite finie}
  Dire que $f$ a pour limite $\ell$ quand $x$ tend vers $a$ signifie que, quel
  que soit $\varepsilon>0$, il existe $\delta>0$ tel que pour tout $x\in I$, si
  $\mid x-a\mid<\delta$, alors $\mid f(x)-\ell\mid<\varepsilon$.
\end{defi}

%\vspace{-5mm}
\begin{intuition}
  Autrement dit, les valeurs de $f(x)$ se rapprochent aussi près que l'on veut
  de $\ell$ dès lors que $x$ se rapproche de $a$.
\end{intuition}
%\vspace{-3mm}

\begin{notation}
  On dit que $f$ admet une \textbf{limite à gauche} de $a$ et on note
  $\displaystyle\lim_{x\to a^-}f(x)$ lorsque $f$ admet une limite quand $x$ tend
  vers $a$ avec $x<a$.\\
  On dit que $f$ admet une \textbf{limite à droite} de $a$ et on note
  $\displaystyle\lim_{x\to a^+}f(x)$ lorsque $f$ admet une limite quand $x$ tend
  vers $a$ avec $x>a$.
\end{notation}

\section{Propriétés sur les limites}
\subsection{Limites des fonctions de référence}

\begin{propnom}{Fonction inverse}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty}\frac{1}{x}=0 &
    \bullet& \lim_{x\to-\infty}\frac{1}{x}=0 &
    \bullet& \lim_{x\to0^-}\frac{1}{x}=-\infty &
    \bullet& \lim_{x\to0^+}\frac{1}{x}=+\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Fonction puissance}
  On a les limites suivantes.
  \begin{align*}
    \bullet\;& \forall n\in\mathbb{N},\lim_{x\to+\infty}x^n=+\infty &
    \bullet\;& \text{pour $n$ pair :} \lim_{x\to-\infty}x^n=+\infty &
    \bullet\;& \text{pour $n$ impair :} \lim_{x\to-\infty}x^n=-\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Fonction exponentielle}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty}e^x = +\infty &
    \bullet& \lim_{x\to-\infty}e^x = 0 &
    \bullet& \lim_{x\to+\infty}e^{-x} = 0 &
    \bullet& \lim_{x\to-\infty}e^{-x} = +\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Racine carrée}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty} \sqrt x = +\infty &
    \bullet& \lim_{x\to0^+} \sqrt x = 0 &
    \bullet& \lim_{x\to+\infty}\frac{1}{\sqrt x} = 0 &
    \bullet& \lim_{x\to0^+}\frac{1}{\sqrt x} = +\infty
  \end{align*}
\end{propnom}


\begin{propadm}
  Soit $a\in\mathbb{R}$ un réel.
  \begin{multicols}{2}
    \begin{itemize}[label=$\bullet$]
      \item Si $a\geq0$, $\displaystyle\lim_{x\to a}\sqrt x=\sqrt a$.
      \item $\displaystyle\lim_{x\to a}e^x=e^a$.
      \item Si $P$ est un polynôme, $\displaystyle\lim_{x\to a}P(x)=P(a)$.
      \item Si $F$ est un quotient de polynômes défini en $a$, alors
        $\displaystyle\lim_{x\to a}F(x)=F(a)$.
      \item $\displaystyle\lim_{x\to a}\cos(x)=\cos(a)$ et
        $\displaystyle\lim_{x\to a}\sin(x)=\sin(a)$.
    \end{itemize}
  \end{multicols}
\end{propadm}

\subsection{Opérations sur les limites}
\subsubsection{Limite d'une somme de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.9\textwidth}{|c|Y|Y|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell$ & $\ell$ & $+\infty$ &
    $-\infty$ & $+\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ \\
    \hline
    \textbf{alors $f+g$ a pour limite} & $\ell+\ell'$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
où \textbf{F.I.} signifie « Forme Indéterminée » et veut dire qu'il n'est pas
possible de déterminer la limite de façon générale dans ce cas là. Il faut alors
faire au cas par cas.
\end{propadm}

\begin{rmq}
  Dans la propriété ci-dessus, la limite peut avoir lieu en un réel
  $a\in\mathbb{R}$, en $a^+$, en $a^-$, ou en $\pm\infty$.
\end{rmq}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}\left( x^2+\frac{1}{x} \right) &
    \textbf{b)}\;& \lim_{x\to0^+}\left( \frac{1}{x}+\frac{1}{\sqrt x} \right) &
    \textbf{c)}\;& \lim_{x\to-\infty}\left( 2+x^3 \right)
  \end{align*}
\end{app}

\subsubsection{Limite d'un produit de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell>0$ & $\ell>0$ & $\ell < 0$ &
    $\ell<0$ & $+\infty$ & $+\infty$ & $-\infty$ & $0$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & $-\infty$ & $\pm\infty$ \\
    \hline
    \textbf{alors $f\times g$ a pour limite} & $\ell\times\ell'$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ & $+\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}(1-x)x^2 &
    \textbf{b)}\;& \lim_{x\to-\infty}\left( \frac{1}{x}-1 \right)x^3 &
    \textbf{c)}\;& \lim_{x\to+\infty}\left( \left( 2-\frac{1}{x}
    \right)\times\left( 3+\frac{1}{x} \right) \right)
  \end{align*}
\end{app}

\subsubsection{Limite d'un quotient de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions telles que $g$ ne s'annule jamais et
  soient $\ell, \ell'\in\mathbb{R}$ deux réels. On a alors les résultats
  suivants.
  \vspace{2mm}
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell$ & $+\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'\neq0$ & $\pm\infty$ & $\ell'>0$ &
    $\ell'<0$ & $\ell'>0$ & $\ell'<0$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{alors $\frac{f}{g}$ a pour limite} &
    $\frac{\ell}{\ell'}$ & $0$ & $+\infty$ & $-\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} & \textbf{F.I.} \\
    \hline
  \end{tabularx}\vspace{2mm}
  \begin{tabularx}{.98\textwidth}{|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell>0$ ou $+\infty$ & $\ell>0$ ou
    $+\infty$ & $\ell<0$ ou $-\infty$ & $\ell<0$ ou $-\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $0^+$ & $0^-$ & $0^+$ & $0^-$ \\
    \hline
    \textbf{alors $\frac{f}{g}$ a pour limite} & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to +\infty}\frac{1+\frac{1}{x}}{1+\sqrt x} &
    \textbf{b)}\;& \lim_{x\to 2}\frac{x^2}{(x-2)^2} &
    \textbf{c)}\;& \lim_{x\to 0^+}\frac{1+\frac{1}{x}}{2+\sqrt x}
  \end{align*}
\end{app}

\begin{app}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par $\forall x\in
  \mathbb{R},\,f(x)=x^3-3x+1$.
  \begin{enumerate}
    \item Peut-on utiliser les opérations sur les limites pour déterminer la
      limite de $f$ en $+\infty$ ?
    \item \begin{enumerate}
        \item Factoriser par $x^3$ dans l'expression de $f$.
        \item En déduire la limite de $f$ en $+\infty$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{methode}
  Pour lever une indéterminée, on peut factoriser ou (plus rarement) développer.
  Dans un \textbf{quotient}, on factorise le numérateur et le dénominateur par
  le terme de plus haut degré.
\end{methode}

\subsection{Limites et comparaisons}

\begin{thmnom}{Comparaison}
  Soit $f$ et $g$ deux fonctions définies sur $I=\mathbb{R}$ ou sur un intervalle
  de la forme $I=[a;+\infty[$ ou $I=]a;+\infty[$ avec $a\in\mathbb{R}$.
    \begin{itemize}
      \item Si, pour tout $x\in I$, $f(x)\geq g(x)$, et
        $\displaystyle\lim_{x\to+\infty}g(x)=+\infty$, alors
        $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$.
      \item Si, pour tout $x\in I$, $f(x)\leq g(x)$, et
        $\displaystyle\lim_{x\to+\infty}g(x)=-\infty$, alors
        $\displaystyle\lim_{x\to+\infty}f(x)=-\infty$.
    \end{itemize}
\end{thmnom}

\begin{app}
  Déterminer la limite en $+\infty$ de la fonction $f$ définie sur
  $\mathbb{R}$ par $f:x\mapsto\sqrt{x^2+2}$.
\end{app}

\begin{thmnom}{Théorème d'encadrement (ou des « gendarmes »)}
  Soit $f$, $g$ et $h$ trois fonctions définies sur $I$, où $I=\mathbb{R}$ ou
  $I$ est un intervalle de la forme $[A;+\infty[$ ou $]A;+\infty[$. On suppose
  que, pour tout $x\in I$, on a $f(x)\leq g(x)\leq h(x)$. Si, pour un réel
  $\ell\in\mathbb{R}$, on a $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ et
  $\displaystyle\lim_{x\to+\infty}h(x)=\ell$, alors
  $\displaystyle\lim_{x\to+\infty}g(x)=\ell$.
\end{thmnom}

\begin{exemple}~\\[-7mm]
  \begin{minipage}{.63\textwidth}
  On considère la fonction $f$ définie sur $]0;+\infty[$ par
    \(
      f(x)=\frac{\cos(x)}{x}
    \).
    Pour $x>0$, on a $-1\leq\cos(x)\leq1$. Ainsi
    \(
      \frac{-1}{x}\leq f(x)\leq\frac{1}{x}
    \),
    et $\displaystyle\lim_{x\to+\infty}\left(\frac{-1}{x}\right)=
    \displaystyle\lim_{x\to+\infty}\left(\frac{1}{x}\right)=0$. On en déduit
    $$\displaystyle\lim_{x\to+\infty}\left(f(x)\right)=0.$$
  \end{minipage}
  \begin{minipage}{.37\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=62,
          ymax=1, y=2cm, x=1mm]
          \addplot[red, thick, samples=201, domain=0.1:60]{cos(deg(x))/x)};
          \addplot[green!60!black, thick, samples=201, domain=0.1:60]{1/x)};
          \addplot[green!60!black, thick, samples=201, domain=0.1:60]{-1/x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}
\begin{app}
  On considère la fonction $f$ définie sur $\mathbb{R}$
  par 
  \(
  f:x\mapsto\frac{2+\cos(x)}{x^2+1}.
  \)
  \begin{enumerate}
    \item Prouver que $\forall x\in\mathbb{R},\;\frac{1}{x^2+1}\leq f(x)\leq
      \frac{3}{x^2+1}$.
    \item En déduire la limite de $f$ en $+\infty$.
  \end{enumerate}
\end{app}

\section{Notion de continuité}
\subsection{Définition}

\begin{defi}{Continuité}
  Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ un réel
  appartenant à $I$. On dit que la fonction $f$ est \textbf{continue en un point} $a$ si
  $f$ admet une limite en $a$ et
  \[
    \lim_{x\to a}f(x)=f(a).
  \]
  On dit que $f$ est \textbf{continue sur un intevalle} $I$ si $f$ est continue en tout
  point de $I$.
\end{defi}

\begin{intuition}
  Une fonction continue est une fonction que l'on peut tracer « à main levée »,
  c'est-à-dire dont la courbe représentative est « en un seul morceau » : elle
  n'a pas de « saut » en certaines valeurs.
\end{intuition}

\begin{exemple}
  On a représenté ci-dessous deux fonctions $f$ et $g$. Celle de gauche, nommée
  $f$, est continue alors
  que celle de droite, nommée $g$, ne l'est pas : il y a un saut en $x=1$. On a
  en particulier
  \[
    \lim_{x\to1^-}g(x) \neq \lim_{x\to1^+}g(x).
  \]
  \\\noindent
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]
        \addplot[red, domain=-5.5:5.5, samples=201] {(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \end{axis}
        \node[red] at (3.3,3.3) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
    \hfill
   \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]
        \addplot[blue!70!black, domain=-5.5:1, samples=201] {(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \addplot[blue!70!black, domain=1:5.5, samples=201] {2+(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \end{axis}
        \node[blue!70!black] at (3.7,4.2) {$\mathscr C_g$};
      \end{tikzpicture}
    \end{center}
    \hfill
  \end{minipage}
\end{exemple}

\begin{app}
  On définit la fonction $f$ sur $\mathbb{R}$ par
  $f(x)=
  \begin{cases}
    x+6 & \text{si } x\leq 3\\
    x^2 & \text{si } x > 3
  \end{cases}
  $. Cette fonction est-elle continue en $3$ ?
\end{app}

\subsection{Opérations et fonctions continues}

\begin{propadm}
  Toutes les fonctions de référence (polynômes, inverse, valeur absolue,
  exponentielle, racine carrée, \emph{etc.}) sont continues sur leur intervalle
  de définition.
\end{propadm}

\begin{propadm}
  Toute fonction dérivable sur un intervalle $I$ est continue sur $I$.
\end{propadm}

\section{Le théorème des valeurs intermédiaires}
\subsection{Cas général}
\noindent
\begin{thmnom}{Théorème des valeurs intermédaires}
\begin{minipage}{.6\textwidth}
  Si $f$ est continue sur $\left[ a; b \right]$ alors, pour tout réel $k$
  compris entre $f(a)$ et $f(b)$, l'équation $f(x)=k$ admet \textbf{au moins}
  une solution dans $\left[ a; b \right]$.
  \end{minipage}
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.1, xmin=-.1, xmax=5,
          ymax=5, y=1cm, x=1cm, xlabel=$x$, ylabel=$y$]
          \addplot[green!50!black, thick, samples=301, domain=.5:4.5]{4-.5*x+cos(deg(3.14*x)};
        \end{axis}

        \node[green!50!black] at (2.5, 4) {$\mathscr C_f$};

        \draw[red!70!black, dashed, thick] (.6, .1) node[below] {$a$} --++ (0, 3.75);
        \draw[red!70!black, dashed, thick] (4.6, .1) node[below] {$b$} --++ (0, 1.75);

        \draw[blue!70!black, dashed, thick] (.1, 3.85) node[left] {$f(a)$} --++
        (0.5, 0);
        \draw[blue!70!black, dashed, thick] (.1, 1.85) node[left] {$f(b)$} --++
        (4.5, 0);
        \draw[blue!70!black, very thick] (.1, 1.85) -- (.1, 3.85);
        \draw[red!70!black, very thick] (.6, .1) -- (4.6, .1);

        \draw[orange!50!black, dotted, very thick] (.1, 3.2) node[left] {$k$} --++ (4.5, 0);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{thmnom}
\noindent
\begin{minipage}{.47\textwidth}
\begin{rmq}
  Autrement dit, tout réel $k$ compris entre $f(a)$ et $f(b)$ admet
  \textbf{au moins} un antécédent par $f$ dans $\left[ a; b \right]$.
\end{rmq}
\end{minipage}
\hfill
\begin{minipage}{.47\textwidth}
\begin{intuition}
  Ce résultat signifie qu'une fonction continue sur $\left[ a; b \right]$ passe
  au moins une fois par toutes les valeurs comprises entre $f(a)$ et $f(b)$.
\end{intuition}
\end{minipage}

\subsection{Cas des fonctions strictement monotones}

\begin{thm}
  Si $f$ est une fonction continue et strictement monotone sur $\left[ a; b
  \right]$, alors pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l'équation
  $f(x)=k$ admet une \textbf{unique} solution dans $\left[ a; b \right]$.
\end{thm}
\noindent
\begin{exemple}~\\[-5mm]
  \begin{minipage}{.6\textwidth}
   Dans l'illustration ci-contre, on a représenté la fonction
   \[
     f:x\mapsto1+\dfrac{x+x^2}{10}.
   \]
   Cette fonction est strictement croissante.
   On voit que pour toute valeur $k\in[a;b]$, on aura une unique solution
   comprise entre $f(a)$ et $f(b)$.
  \end{minipage}
  \hfill
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}%[scale=.8]
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.1, xmin=-.1, xmax=5,
          ymax=5, y=1cm, x=1cm, xlabel=$x$, ylabel=$y$]
          \addplot[green!50!black, thick, samples=301, domain=.5:4.5]{1+.1*x+.1*x^2};
        \end{axis}

        \coordinate (a) at (.6, .1);
        \coordinate (b) at (4.6, .1);
        \coordinate (fa) at (.1, 1.175);
        \coordinate (fb) at (.1, 3.575);

        \node[green!50!black] at (4.5, 4) {$\mathscr C_f$};

        \draw[red!70!black, dashed, thick] (a) node[below] {$a$} --++
        ($(0,0)!(fa)!(0,1)-(0,.1)$);
        \draw[red!70!black, dashed, thick] (b) node[below] {$b$} --++
        ($(0,0)!(fb)!(0,1)-(0,.1)$);

        \draw[blue!70!black, dashed, thick] (fa) node[left] {$f(a)$} --++
        (0.5, 0);
        \draw[blue!70!black, dashed, thick] (fb) node[left] {$f(b)$} --++
        (4.5, 0);
        \draw[blue!70!black, very thick] (fa) -- (fb);
        \draw[red!70!black, very thick] (a) -- (b);

        \draw[orange!50!black, dotted, very thick] (.1, 2.2) node[left] {$k$} --++ (4.5, 0);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{app}
  Soit la fonction $f$ définie sur $\left[ -2; +\infty \right[$ par
  $f(x)=x^3-3x^2+3$.
  \begin{enumerate}
    \item Dresser le tableau de variations de la fonction $f$, on admettra que
      $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$.
    \item \begin{enumerate}
        \item Montrer que l'équation $f(x)=1$ admet au moins une solution dans
          $\left[ -2; +\infty \right[$.
        \item Montrer que l'équation $f(x)=5$ admet une unique solution $\alpha$
          dans $[-2; +\infty[$. Donner un encadrement au dixième près de
            $\alpha$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\end{document}
