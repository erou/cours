\documentclass[11pt, twocolumn]{article}

\newcommand{\titrechapitre}{Lois discrètes -- Exercices}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{0mm}
\renewcommand{\P}{\mathcal P}

\setlength{\columnseprule}{1pt}

\input{/home/erou/cours/latex/layout-color.tex}

\begin{document}

% Loi uniforme
% ------------

\begin{exobox}
  Lorsqu'il emprunte des livres à la bibliothèque, Youssef en emprunte $1$, $2$,
  $3$, $4$ ou $5$ avec la même probabilité. On appelle $L$ la variable aléatoire
  donnant le nombre de livres empruntés par Youssef.
  \begin{enumerate}
    \item Quelle loi suit $L$ ?
    \item En déduire l'espérance $E(L)$.
    \item Déterminer $P(L\geq4)$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Dans un groupe de six amies, deux ont $1$ frère, deux ont $2$ frères et deux
  ont $3$ frères. On choisit une de ces amies au hasard et on considère la
  variable aléatoire $F$ donnant son nombre de frère(s).
  \begin{enumerate}
    \item Justifier que $F$ suit une loi uniforme.
    \item Calculer $P(F\leq 2)$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Pour son jeu de rôle, Sélima utilise un dé équilibré à vingt faces numérotées
  de $1$ à $20$.
  \begin{enumerate}
    \item Quelle est la loi suivie par la variable aléatoire $D$ donnant le
      résultat d'un lancer ?
    \item Calculer la probabilité d'obtenir une réussite critique, c'est-à-dire
      un $20$, lors d'un lancer.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Bilal reçoit entre $1$ et $15$ courriels par jour et les probabilités de ces
  différents nombres sont égales.
  \begin{enumerate}
    \item Quelle loi suit la variable aléatoire $C$ donnant le nombre de
      courriels reçus par Bilal pendant un jour ?
    \item En déduire $E(C)$ et $V(C)$.
    \item Quelle est la probabilité qu'il reçoive plus de $4$ courriels ?
    \item Bilal part en vacances pendant $20$ jours. Environ combien de
      courriels peut-il s'attendre à avoir en rentrant de vacances ?
  \end{enumerate}
\end{exobox}

% Loi de Bernoulli
% ----------------

\begin{exobox}
  Dans un lycée, il y a $11$ professeurs de mathématiques parmi les $100$
  professeurs. On tire au sort un professeur parmi ces $100$ et on considère la
  variable aléatoire $M$ donnant le nombre de professeurs de mathématiques
  obtenu ($0$ ou $1$).
  \begin{enumerate}
    \item \begin{enumerate}
        \item Justifier que $M$ suit une loi de Bernoulli.
        \item Donner son paramètre $p$.
      \end{enumerate} 
    \item Donner l'espérance de $M$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Soit la variable $R$ donnant le nombre de boules roses obtenues ($0$ ou $1$)
  lorsque l'on tire une boule dans une urne contenant $10$ boules roses et $5$
  boules vertes.
  \begin{enumerate}
    \item Justifier que $R$ suit la loi $\mathscr B\left( \frac{2}{3} \right)$.
    \item Combien faut-il ajouter de boules vertes dans l'urne pour que $R$
      suive la loi $\mathscr B\left( \frac{1}{4} \right)$ ?
  \end{enumerate}
\end{exobox}

\begin{exodur}
  On lance un dé équilibré à $12$ faces numérotées de $1$ à $12$ et on
  considère la variable aléatoire $X$ donnant le chiffre des dizaines du
  résultat obtenu.
  \begin{enumerate}
    \item \begin{enumerate}
        \item Justifier que $X$ suit une loi de Bernoulli.
        \item Préciser son paramètre $p$.
      \end{enumerate}
  \end{enumerate}
  \noindent On lance maintenant deux dés équilibrés, l'un à quatre faces et
  l'autre à huit faces et on s'intéresse à $Y$ la variable aléatoire donnant
  le chiffre des dizaines de la somme des deux nombres obtenus.
  \begin{enumerate}
      \setcounter{enumi}{1}
    \item Donner la loi de $Y$.
  \end{enumerate}
\end{exodur}

% Schéma de Bernoulli
% -------------------

\begin{exobox}
  La probabilité qu'un appel aux pompiers soit injustifié est $0,19$.
  \begin{enumerate}
    \item Quelle hypothèse doit-on faire pour pouvoir assimiler la répétition
      de $4$ appels aux pompiers, selon qu'ils sont injustifiés ou non, à un
      schéma de Bernoulli ?
    \item Selon cette hypothèse, représenter ce schéma de Bernoulli par un
      arbre.
    \item Calculer la probabilité
      \begin{enumerate}
        \item qu'exactement deux des quatre appels soient injustifiés ;
        \item qu'au moins un appel soit justifié.
      \end{enumerate}
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Lorsqu’il fait ses devoirs, François n’éteint jamais son téléphone. Quand il
  reçoit un message, il y a deux chances sur trois qu’il le regarde,
  indépendamment du fait qu’il ait regardé ou non les messages précédents.
  Pendant tout le temps qu’il a consacré à ses devoirs, il a reçu $3$ messages.
  \begin{enumerate}
    \item Justifier que cette situation est assimilable à un schéma
      de Bernoulli en spécifiant à quel événement correspond un succès.
    \item Représenter ce schéma par un arbre.
    \item Calculer la probabilité qu’il ait regardé au moins $2$ messages.
  \end{enumerate}
\end{exobox}

% Coefficients binomiaux
% ----------------------

\begin{exobox}1. Avec la calculatrice, donner $\binom{12}{5}$ et $\binom{12}{6}$.
  \begin{enumerate}
      \setcounter{enumi}{1}
    \item En déduire sans calculatrice :
      \begin{align*}
        \textbf{a)}\;& \binom{12}{7} &
        \textbf{b)}\;& \binom{13}{6} &
        \textbf{c)}\;& \binom{13}{7}
      \end{align*}
  \end{enumerate}
\end{exobox}

% Loi binomiale
% -------------

\begin{exobox}
  À la roulette la probabilité que la boule tombe sur rouge est
  $\frac{18}{37}$. On joue $20$ fois successivement à la roulette en misant
  systématiquement sur le rouge et on appelle $X$ la variable aléatoire donnant
  le nombre de parties gagnées.
  \begin{enumerate}
    \item Quelle loi suit $X$ ? Justifier.
    \item Calculer la probabilité de gagner $9$ parties.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  On considère que la probabilité qu’un élève de Terminale ait $18$ ans ou plus
  durant l’année scolaire est $0,67$.
  \begin{enumerate}
    \item Dans une classe de Terminale de $35$ élèves, quelle est la loi suivie
      par la variable aléatoire $M$ donnant le nombre d’élèves de la classe
      encore mineurs à la fin de l’année ? Justifier.
    \item Donner $P(M = 10), P(M = 11), P(M = 12)$.
    \item En déduire la probabilité qu’il y ait entre $10$ et $12$ élèves de la
      classe encore mineurs à la fin de l’année scolaire.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  La probabilité de gagner à un jeu de grattage est $0,1$. On considère $1\,000$
  joueurs ayant joué à ce jeu dont on suppose que leurs résultats (« Gagné » ou
  « Perdu ») sont indépendants et on appelle $G$ la variable aléatoire donnant
  le nombre de gagnants parmi ces $1\,000$ joueurs. Déterminer la probabilité
  qu’il y ait :
  \begin{enumerate}
    \item plus de $100$ gagnants.
    \item moins de $85$ gagnants.
    \item entre $95$ (inclus) et $105$ (inclus) gagnants.
    \item entre $90$ (exclu) et $110$ (exclu) gagnants.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Dans une population, la proportion de végétariens est de 12\%. On suppose
  cette population suffisamment grande pour pouvoir assimiler le tirage d’une
  personne dans cette population à un tirage avec remise. Une cantine servant
  $250$ repas à des personnes issues de cette population prévoit $32$ repas
  végétariens. Quelle est la probabilité que ce ne soit pas suffisant ?
\end{exobox}

\begin{exobox}
  On considère un entier $n$ inconnu et on donne\\
  \begin{minipage}{.14\textwidth}
    ci-contre le diagramme en barres associé à une variable aléatoire $X$
    suivant la loi $\mathscr B(n; p)$.
  \end{minipage}
  \hfill
  \begin{minipage}{.35\textwidth}
      \begin{center}
      \begin{tikzpicture}%[scale=.9]
    \begin{axis}[
        ybar,
        bar width=1.5mm,
        ymin=0,
        ymax=0.18,
        xmax=21,
        xtick={0, 2, ..., 20},
        ytick={0, 0.05, 0.1, 0.15},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$},
        xlabel={\small $k$},
        ylabel={\small $P(X=k)$},
        width=6cm,
        height=4cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 2.8430288029929676e-06
1 4.7383813383216124e-05
2 0.00037907050706572905
3 0.0019374714805581712
4 0.00710406209537996
5 0.01989137386706389
6 0.044203053037919764
7 0.07998647692575958
8 0.11997971538863939
9 0.15108556752643482
10 0.1611579386948638
11 0.14650721699533073
12 0.11395005766303502
13 0.07596670510869001
14 0.043409545776394304
15 0.021222444601792772
16 0.008842685250746989
17 0.0031209477355577616
18 0.0009247252549800775
19 0.00022712550122317698
20 4.5425100244635394e-05
21 7.21033337216435e-06
22 8.739798026865878e-07
23 7.59982437118772e-08
24 4.222124650659846e-09
25 1.1258999068426256e-10

        } \datatable

        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
\end{tikzpicture}
    \end{center}
  \end{minipage}
On admet que $E(X)\in\mathbb{N}$.
  \begin{enumerate}
    \item Donner une valeur possible pour $E(X)$.
    \item En déduire une valeur possible pour $n$.
  \end{enumerate}
\noindent
  \begin{minipage}{.14\textwidth}
    On donne ci-contre le diagramme en barres associé à une variable aléatoire
    $Y$ suivant la loi
  \end{minipage}
  \hfill
  \begin{minipage}{.35\textwidth}
      \begin{center}
      \begin{tikzpicture}%[scale=.9]
    \begin{axis}[
        ybar,
        bar width=2mm,
        ymin=0,
        ymax=0.22,
        xmin=11.99,
        xmax=27,
        xtick={12, 14, ..., 24},
        ytick={0, 0.05, 0.1, 0.15, 0.2},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$, $0.2$},
        xlabel={\small $k$},
        ylabel={\small $P(X=k)$},
        width=6cm,
        height=4cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 3.3554431999999813e-18
1 3.3554431999999816e-16
2 1.610612735999992e-14
3 4.939212390399977e-13
4 1.0866267258879951e-11
5 1.8255328994918322e-10
6 2.4340438659891105e-09
7 2.6426761973596063e-08
8 2.3784085776236467e-07
9 1.7970198142045335e-06
10 1.1500926810909018e-05
11 6.273232805950374e-05
12 0.00029275086427768424
13 0.0011710034571107372
14 0.004014868995808243
15 0.01177694905437085
16 0.029442372635927132
17 0.06234855381725748
18 0.11084187345290221
19 0.16334591877269805
20 0.1960151025272377
21 0.1866810500259407
22 0.13576803638250237
23 0.07083549724304473
24 0.023611832414348253
25 0.0037778931862957215
        } \datatable

        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
\end{tikzpicture}
    \end{center}
  \end{minipage}
  ~\\[-.5mm]$\mathscr B(n; p)$ où $p$ est inconnu et où $n$ est le nombre trouvé à la
 question 2.
 \begin{enumerate}
     \setcounter{enumi}{2}
   \item Proposer une valeur possible de $p$.
 \end{enumerate}
\end{exobox}

% Intervalles de fluctuation
% --------------------------

\begin{exobox}
  On considère une variable aléatoire $X$ qui suit la loi binomiale de
  paramètres $n = 54$ et $p = 0,45$. Déterminer le plus petit intervalle de
  fluctuation associé à $X$ de la forme $[0 ; b]$ :
  \begin{enumerate}
    \item au seuil de $0,95$ ;
    \item au seuil de $0,99$.
  \end{enumerate}
\end{exobox}

% Loi géométrique
% ---------------

\begin{exobox}
  Léa commence une séance d’entraînement de pétanque et on considère que la
  probabilité qu’elle réussisse un «~carreau~» est $0,2$. On suppose tous les
  essais indépendants. On appelle $X$ la variable aléatoire donnant le rang du
  premier carreau réussi.
  \begin{enumerate}
    \item Donner la loi suivie par X.
    \item Représenter la situation par un arbre pour les trois premiers essais
      et y lire $P(X = 2)$.
    \item Calculer $P(X = 5)$ puis l’interpréter.
    \item Calculer $P(X \leq 3)$ puis l’interpréter.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  On suppose que la probabilité de gagner une partie à une machine à sous est
  $0,001$. On suppose que toutes les parties sont indépendantes et on appelle
  $X$ la variable aléatoire donnant le rang de la première partie gagnée quand
  on joue plusieurs parties de suite.
  \begin{enumerate}
    \item Donner la loi suivie par X.
    \item En combien de parties en moyenne peut-on espérer gagner la première
      fois avec cette machine à sous ?
    \item Calculer $P(X > 500)$ puis l’interpréter.
    \item La mise à cette machine à sous est de $2$ euros. Quelle est la
      probabilité que l’on gagne avant de ne plus avoir d’argent si l’on dispose
      de $2000$ euros ?
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Elsa a une pièce truquée qu’elle lance jusqu’à obtenir «~pile~». Après de
  nombreux essais, elle a remarqué qu’en moyenne, il lui fallait $6$ lancers
  afin d’obtenir son premier «~pile~». Déterminer la probabilité d’obtenir
  «~pile~» en lançant cette pièce.
\end{exobox}

\begin{exodur}
  Une urne contient $20$ boules de quatre couleurs différentes : $7$ rouges,
  $10$ vertes, $2$ jaunes et $1$ bleue. On tire deux boules sans remise dans
  cette urne et on note à chaque fois la couleur obtenue.
  \begin{enumerate}
    \item Cette succession de deux épreuves est-elle une succession d’épreuves
      indépendantes ?
    \item La représenter par un arbre.
    \item \begin{enumerate}
        \item Déterminer la probabilité d’obtenir deux boules rouges.
        \item Déterminer la probabilité d’obtenir une boule verte et une boule
          jaune (sans tenir compte de l’ordre du tirage).
        \item Déterminer la probabilité que la première boule soit jaune sachant
          que la deuxième est rouge.
        \item Déterminer la probabilité que la première boule soit verte sachant
          que la deuxième est bleue.
      \end{enumerate}
  \end{enumerate}
\end{exodur}

% Et maintenant des exercices de synthèse ?

\begin{exobox}
  À un guichet, le temps d’attente exprimé en secondes est $1$, $2$, $\ldots$,
  $600$, ces six cents temps d’attente étant équiprobables. On appelle $T$ la
  variable aléatoire donnant le temps d’attente à ce guichet en seconde.
  \begin{enumerate}
    \item Quelle est la loi suivie par $T$ ?
    \item Calculer la probabilité qu’on attende $3$ minutes ou plus ?
    \item Sachant qu’elle a déjà attendu $2$ minutes, quelle est la probabilité
      que Fatou attende moins de $4$ minutes ?
  \end{enumerate}
\end{exobox}

% 93 p 189

\begin{exobox}
  En $10$ lancers successifs d'un dé équilibré à six faces, on n'a pas obtenu de
  $6$.
  \begin{enumerate}
    \item Quelle est la probabilité que l'on obtienne pas de $6$ avant le
      $14^\mathrm e$
      lancer ?
    \item Quelle est la probabilité que l'on obtienne un $6$ avant le $16^\mathrm e$
      lancer ?
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Dans un jeu, la mise est de $1$ euro et on gagne $9$ euros avec la probabilité
  $0,1$ (on ne gagne rien le reste du temps). On considère deux stratégies,
  précisément :
\begin{itemize}
    \item \textbf{Stratégie 1} : on joue $20$ parties.
    \item \textbf{Stratégie 2} : on joue jusqu’à gagner une partie et on
      s’arrête (on suppose que l’on dispose d’argent « infini »).
\end{itemize}

\begin{enumerate}
    \item Dire combien de parties on peut «~espérer~» gagner avec la stratégie
      1. Sans tenir compte de la mise, en déduire quelle somme on peut
      «~espérer~» gagner avec la stratégie 1.
    \item Quelle somme gagne-t-on avec la stratégie 2 ?
    \item \begin{enumerate}
        \item Quelle somme dépense-t-on avec la stratégie 1 ?
        \item Dire combien de parties on peut «~espérer~» jouer puis quelle
          somme on peut «~espérer~» miser avec la stratégie 2.
    \end{enumerate}
    \item Avec laquelle des deux stratégies peut-on espérer perdre le moins possible ?
\end{enumerate}
\end{exobox}

\begin{exobox}
  \textbf{A. Calcul de probabilités.} Le week-end, Michaela va souvent à la
  pêche. La probabilité qu’elle y aille le 
  samedi est de $0,7$, et la probabilité qu’elle y aille le dimanche est :
\begin{itemize}
    \item $0,3$ si elle y est allée le samedi,
    \item $0,9$ si elle n’y est pas allée le samedi.
\end{itemize}

\begin{enumerate}
    \item Expliquer pourquoi les deux épreuves consistant à regarder si Michaela
      va à la pêche le samedi et le dimanche ne sont pas indépendantes.
    \item Représenter la situation par un arbre pondéré puis déterminer la
      probabilité qu’elle aille au moins une fois à la pêche le week-end.
    \item Sachant qu’elle est allée à la pêche ce week-end, quelle est la
      probabilité qu’elle y soit allée samedi ?
\end{enumerate}

\noindent\textbf{B. Loi géométrique.} On considère que la probabilité que Michaela rentre
de la pêche après 17 h est de $0,4$, indépendamment d’une fois à l’autre. On
appelle \( X \) la variable aléatoire donnant le rang de la première fois où
elle rentre après 17 h depuis l’ouverture de la pêche.

\begin{enumerate}
    \item Quelle loi suit \( X \) ? Justifier.
    \item Calculer \( P(X < 4) \) et l’interpréter dans les termes de l’énoncé.
    \item Déterminer la probabilité qu’il faille attendre la 3e session de pêche
      pour qu’elle rentre après 17 h.
    \item Michaela est allée pêcher $4$ fois et est toujours rentrée avant 17 h.
      Quelle est la probabilité qu’elle rentre avant 17 h pendant ses $9$
      premières sessions (les $4$ premières comprises) ?
\end{enumerate}
\end{exobox}

\begin{exobox}
  \textbf{Choisir la bonne réponse.}
Quand on réalise un grand nombre de fois les expériences aléatoires suivantes, 
en moyenne, obtient-on un nombre plus élevé en regardant :
\begin{enumerate}
    \item le nombre obtenu quand on lance un dé équilibré à huit 
    faces numérotées de $1$ à $8$ ?
    \item Le nombre de PILE obtenus quand on lance $8$ fois une 
    pièce de monnaie équilibrée ?
\end{enumerate}
\end{exobox}

\begin{exobox}
  On considère les deux expériences aléatoires ci-dessous :
\begin{itemize}
    \item on lance une fois un dé équilibré à quatre faces numérotées de $1$ 
    à $4$ : un succès correspond à un $4$ obtenu ;
    \item on lance quatre fois un dé équilibré à douze faces numérotées de 
    $1$ à $12$ : un succès correspond à un $12$ obtenu.
\end{itemize}
Sur un grand nombre de répétitions, en moyenne, laquelle 
de ces deux expériences aléatoires assure d’obtenir le plus de succès ?
\end{exobox}

\begin{exobox}
  On considère deux jeux dans lesquels soit on perd, soit on gagne $4$ euros.
  Le jeu n°1 coûte $2$ euros et la probabilité de gagner est
  $0,09$, et le jeu n°2 coûte $1$ euro et la probabilité de gagner est
  $0,05$.\\[2mm]
  Si on dispose de $20$ euros, quel est le jeu auquel il est
  préférable de jouer ?
\end{exobox}

\end{document}
