\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Lois discrètes -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage}%/\pageref{LastPage}}
\newcommand{\topbotmargins}{1.6cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}
\renewcommand{\P}{\mathcal P}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Loi uniforme sur {1,2,…,n}. Espérance.
% [x] Épreuve de Bernoulli. Loi de Bernoulli : définition, espérance et écart
%     type.
% [x] Schéma de Bernoulli. Représentation par un arbre.
% [x] Coefficients binomiaux : définition (nombre de façons d’obtenir k succès
%     dans un schéma de Bernoulli de taille n), triangle de Pascal, symétrie.
% [x] Variable aléatoire suivant une loi binomiale B(n,p). Interprétation :
%     nombre de succès dans le schéma de Bernoulli. Expression, espérance et
%     écart type (admis). Représentation graphique.
% [x] Loi géométrique : définition, expression, espérance (admise),
%     représentation graphique et propriété caractéristique (loi sans mémoire).
%
% Capacités attendues
% -------------------
% 
% + Identifier des situations où une variable aléatoire suit une loi de
%   Bernoulli, une loi binomiale ou une loi géométrique.
% + Déterminer des coefficients binomiaux à l'aide du triangle de Pascal.
% + Dans le cas où X suit une loi binomiale, calculer à l'aide d'une
%   calculatrice ou d’un logiciel, les probabilités des événements de type 
%   P(X = k) ou P(X <= k), etc. Calculer explicitement ces probabilités pour une
%   variable aléatoire suivant une loi géométrique.
% + Dans le cas où X suit une loi binomiale, déterminer un intervalle I pour
%   lequel la probabilité P(X ∈ I ) est inférieure à une valeur donnée α, ou
%   supérieure à 1 - α.
% + Dans le cadre de la résolution de problème, utiliser l’espérance des lois
%   précédentes.
% + Utiliser en situation la caractérisation d’une loi géométrique par l’absence
%   de mémoire.
% + Calculer des probabilités dans des situations faisant intervenir des
%   probabilités conditionnelles, des répétitions d’expériences aléatoires.
%
% Démonstrations
% --------------
%
% + Espérance et écart type d’une variable aléatoire suivant une loi de
%   Bernoulli.
% + Espérance d’une variable aléatoire uniforme sur {1,2,…,n}.
% + Espérance d’une variable aléatoire suivant une binomiale (n <= 3).
% + Caractérisation d’une loi géométrique par l’absence de mémoire.
%
% Exemples d'algorithme
% ---------------------
%
% [ ] Recherche de seuils
% [ ] Recherche de valeurs approchées de pi, e, \sqrt 2, \ln 2, *etc.*
% [ ] Pour une suite récurrente un+1 = f(un), calcul des termes successifs.

\title{\vspace{-20mm}Chapitre 2 : Lois discrètes}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/math-comp/lois-discretes.html}
\vspace{-7mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Définitions et notations}

\begin{defi}{Variable aléatoire}
  Soit $\Omega$ l'ensemble des issues d'une expérience aléatoire ($\Omega$ est
  l'\textbf{univers}). Une \textbf{variable aléatoire} sur $\Omega$ est une
  fonction définie sur $\Omega$ à valeurs dans $\mathbb{R}$.
\end{defi}

\begin{defi}{Événement défini à partir d'une variable aléatoire}
  Soient $X$ une variable aléatoire définie sur $\Omega$ et $x\in\mathbb{R}$ un
  nombre réel.
  \begin{itemize}
    \item L'événement « \textbf{$X$ prend la valeur $x$} » est l'ensemble des
      issues de $\Omega$ auxquelles on associe le réel $x$.
    \item L'événement « \textbf{$X$ prend des valeurs supérieures ou égales
      à $x$} » est l'ensemble des
      issues de $\Omega$ auxquelles on associe un réel inférieur ou égal à $x$.
    \item L'événement « \textbf{$X$ prend des valeurs inférieures ou égales
      à $x$} » est l'ensemble des
      issues de $\Omega$ auxquelles on associe un réel inférieur ou égal à $x$.
  \end{itemize}
\end{defi}

\begin{notation}
  Ces trois événements sont respectivement notés $\left\{ X=x \right\}$,
  $\left\{ X\geq x \right\}$ et $\left\{ X\leq x \right\}$.
\end{notation}

\begin{notation}
  L'ensemble des valeurs prises par une variable aléatoire $X$ définie sur un
  univers $\Omega$ est noté $X(\Omega)$.
\end{notation}

\begin{exemple}
  On lance un dé à six faces. Si on obtient un multiple de $3$, on gagne $2$
  euros; sinon, on perd $1$ euro. On note $X$ la variable aléatoire qui, à
  chaque lancer associe le gain obtenu (ce gain peut éventuellement être
  négatif).
  \begin{itemize}
    \item L'événement $\left\{ X=2 \right\}$ est réalisé lorsque l'on obtient un
      multiple de $3$.
    \item L'événement $\left\{ X\leq0 \right\}$ est réalisé lorsque le gain est
      négatif, c'est-à-dire lorsque l'on n'obtient pas un multiple de $3$.
    \item On a ici $X\left( \Omega \right)=\left\{ 2, -1 \right\}$.
  \end{itemize}
\end{exemple}

\begin{defi}{Probabilité d'un événement $\left\{ X=x_i \right\}$}
  La \textbf{probabilité d'un événement} $\left\{ X=x_i \right\}$ est la somme
  des probabilités des issues de $\Omega$ auxquelles on associe le réel $x_i$.
  On la note $P(\{X=x_i\})$ ou plus simplement $P(X=x_i)$.
\end{defi}

\begin{defi}{Loi de probabilité}
  Soit $X$ une variable aléatoire définie sur $\Omega$ et prenant les valeurs
  $x_1, x_2, \ldots, x_n$. Définir la
  \textbf{loi de probabilité} de la variable aléatoire $X$, c'est associer à
  chaque valeur $x_i$ prise par $X$ la probabilité de l'événement $P\left(
  \left\{ X=x_i \right\}\right)$.
\end{defi}

\noindent Dans les définitions suivantes, $X$ est une variable aléatoire réelle
définie sur un univers $\Omega$ prenant les valeurs $x_1, x_2, \ldots, x_n$ avec
les probabilités respectives $p_1, p_2, \ldots, p_n$.

\begin{defi}{Espérance}
  L'\textbf{espérance} de $X$ est le nombre réel, noté $E\left( X \right)$,
  défini par
  \[
    E\left( X \right) = p_1\times x_1+p_2\times x_2+\cdots+p_n\times x_n.
  \]
\end{defi}

\begin{intuition}
  Le nombre $E\left( X \right)$ peut s'interpréter comme la moyenne des valeurs
  prises par $X$ lorsque l'expérience aléatoire est répétée \textbf{un très
  grand nombre de fois}.
\end{intuition}

\begin{defi}{Variance et écart-type}
  La \textbf{variance} de $X$ est le réel positif, noté $\Var\left( X
  \right)$, défini par
  \[
    \Var\left( X \right) = p_1\times\left(
    x_1-E\left( X \right) \right)^2+\cdots+p_n\times\left( x_n-E\left( X
    \right) \right)^2.
  \]
  L'\textbf{écart-type} de $X$ est le nombre positif, noté $\sigma\left( X
  \right)$, défini par $\sigma\left( X \right)=\sqrt{\Var\left( X \right)}$.
\end{defi}

\begin{intuition}
  L'écart-type $\sigma(X)$ mesure à quel point les valeurs prises par
  $X$ vont s'écarter de la moyenne $E(X)$. Typiquement, $X$ prendra des valeurs
  éloignées de $\sigma(X)$ de sa moyenne $E(X)$.
\end{intuition}

\begin{exemple}
  Dans une classe, on a réalisé une évaluation de mathématique, on choisit une
  copie au hasard. Soit $X$ la variable aléatoire donnant la note obtenue par
  cette copie.
  \begin{itemize}
    \item Dans ce cas $E(X)$ représente la moyenne de la classe. Par exemple
      $E(X)=13$ signifie que la moyenne de toutes les copies est $13$.
    \item Si on a $\sigma(X)=3$, on peut imaginer qu'on va typiquement avoir des
      notes autour de $10$ et $16$ (même s'il peut y en avoir très en-dessous ou
      au-dessus). Si $\sigma(X)=5$, alors les notes seront plus dispersées, on
      aura typiquement des $8$ et des $18$.
  \end{itemize}
\end{exemple}

\section{Loi uniforme}

\begin{defi}{Loi uniforme}
  Une variable aléatoire $X$ suit la loi uniforme sur $\left\{ 1; 2; \ldots; n
  \right\}$ si elle prend pour valeurs les entiers $1$ à $n$ de manière
  équiprobable, c'est-à-dire si $P(X=k)=\frac{1}{n}$ pour tout entier
  $k$ entre $1$ et $n$.
\end{defi}

\begin{intuition}
  La loi uniforme représente la variable aléatoire obtenue lorsqu'on choisit au
  hasard un élément parmi $n$.
\end{intuition}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi uniforme sur $\left\{ 1; 2;
    \ldots; n \right\}$, on a alors
    \begin{align*}
      E(X) &= \frac{n+1}{2} &
      V(X) &= \frac{n^2-1}{12} &
      \sigma(X) &= \sqrt{\frac{n^2-1}{12}}.
    \end{align*}
\end{prop}

\begin{app}
  On lance un dé équilibré à huit faces numérotées de $1$ à $8$ et on considère
  la variable aléatoire $X$ donnant le résultat obtenu.
  \begin{enumerate}
    \item Quelle loi suit la variable aléatoire $X$ ?
    \item En déduire son espérance $E(X)$ et sa variance $V(X)$.
  \end{enumerate}
\end{app}

\section{Loi de Bernoulli}
\subsection{Épreuve de Bernoulli}

\begin{defi}{Épreuve de Bernoulli}
  Une \textbf{épreuve de Bernoulli} est une expérience aléatoire possédant deux
  issues : succès (noté $S$) et échec (noté $\overline S$ ou $E$).
\end{defi}

\begin{intuition}
  Une épreuve de Bernoulli permet de modéliser n'importe quelle expérience qui
  ne possède que deux alternatives ; l'une d'elle sera le succès et l'autre
  l'échec.
\end{intuition}

\begin{exemple}
  On dispose d'une urne contenant des tickets gagnants et des tickets perdants.
  On tire au hasard un ticket et on regarde s'il est gagnant ou non. Cette
  expérience aléatoire est une épreuve de Bernoulli.
\end{exemple}

\subsection{Définition et propriété de la loi de Bernoulli}

\begin{defi}{Loi de Bernoulli}
  \begin{minipage}{.6\textwidth}
    Soit $p\in]0; 1[$. Une variable aléatoire $X$ suit la loi de Bernoulli de
      paramètre $p$, notée $\mathscr B(p)$, si on a $P(X=0)=1-p$ et $P(X=1)=p$.
      On synthétise avec le tableau ci-contre.
  \end{minipage}
  \hfill
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tabularx}{.9\textwidth}{|c|Y|Y|}
        \hline
        $x_i$ & $0$ & $1$ \\
        \hline
        $P(X=x_i)$ & $1-p$ & $p$ \\
        \hline
      \end{tabularx}
    \end{center}
  \end{minipage}
\end{defi}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr B(p)$, on a alors
    \begin{align*}
      E(X) &= p &
      V(X) &= p(1-p) &
      \sigma(X) &= \sqrt{p(1-p)}.
    \end{align*}
\end{prop}

\begin{app}
  On lance une pièce de monnaie truquée de sorte que la probabilité d'obtenir «
  pile~» est de $10$\%, et on regarde le nombre de «~pile~» obtenus ($0$ ou
  $1$).
  \begin{enumerate}
    \item Quelle loi suit la variable aléatoire $X$ ?
    \item En déduire son espérance $E(X)$ et sa variance $V(X)$.
  \end{enumerate}
\end{app}


\section{Loi binomiale}
\subsection{Schéma de Bernoulli}

\begin{defi}{Schéma de Bernoulli}
  Un \textbf{schéma de Bernoulli} de taille $n\in\mathbb{N}$ est la répétition de $n$
  épreuves de Bernoulli identiques et idépendantes.
\end{defi}

\begin{exemple}%~\\[-7mm]
  \label{ex:bern}
  \begin{minipage}{.6\textwidth}
    On lance successivement une pièce de monnaie non équilibrée dont la
    probabilité d'un succès «~tomber sur pile~», noté $P$, est $0,4$. On note
    $F$ l'événement «~tomber sur face~».
    \begin{itemize}
      \item On obtient un schéma de Bernoulli de taille $2$ que l'on peut représenter par
        l'arbre ci-contre.
      \item On calcule les probabilités des différents événements avec les
        règles classiques sur les arbres.
    \end{itemize}
  \end{minipage}
    \begin{minipage}{.4\textwidth}
    \begin{center}
\begin{tikzpicture}[scale=.8,level 1/.style={sibling distance=2cm},
    level 2/.style={sibling distance=1cm}]
  \node {} [grow'=right, level distance=3cm]
  child {
    node {$P$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
    edge from parent node[below] {$0,6$}
    }
    edge from parent node[above] {$0,4$}
  }
  child {
    node {$F$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
      edge from parent node[below] {$0,6$}
    }
    edge from parent node[below] {$0,6$}
  };
\end{tikzpicture}
\end{center}
  \end{minipage}
\end{exemple}

\subsection{Coefficients binomiaux et triangle de Pascal}

\begin{defi}{Coefficients binomiaux}
  Pour $n$ et $k$ entiers naturels avec $0\leq k\leq n$, le coefficient binomial
  $\binom{n}{k}$, qui se lit «~$k$ parmi $n$~», est le nombre de façons
  d'obtenir $k$ succès dans un schéma de Bernoulli de taille $n$.
\end{defi}
\begin{rmq}
  Par convention, $\binom{0}{0}=1$.
\end{rmq}
\begin{exemple}
  Dans le schéma de Bernoulli de taille $2$ de l'exemple~\ref{ex:bern}, il y a
  $2$ façons d'obtenir $1$ succès («~tomber sur pile~»): pile puis face ou face
  puis pile, ainsi $\binom{2}{1}=2$.
\end{exemple}

\begin{prop}
  \label{prop:pascal}
  Pour $n$ et $k$ entiers avec $1\leq k\leq n$, on a
  \begin{align*}
    \binom{n}{0} &=1 &
    \binom{n}{n} &=1 &
    \binom{n-1}{k-1}+\binom{n-1}{k} &= \binom{n}{k}.
  \end{align*}
\end{prop}

\begin{exemple}
  \begin{minipage}{.6\textwidth}
    Les égalités de la propriété~\ref{prop:pascal} permettent de tracer le
    \textbf{triangle de Pascal}, c'est-à-dire d'obtenir les valeurs des
    coefficients binomiaux $\binom{n}{k}$ pour de plus grandes valeurs de $n$
    et $k$. On a ainsi $\binom{4}{1}+\binom{4}{2}=4+6=10=\binom{5}{2}$.
 \begin{center}
  \begin{tabularx}{.95\textwidth}{|c|Y|Y|Y|Y|Y|Y|}
    \hline
    & \small$k=0$ & \small$k=1$ & \small$k=2$ & \small$k=3$ & \small$k=4$ & \small$k=5$ \\
    \hline
    \small\( n = 0 \) & 1 & & & & & \\
    \hline
    \small\( n = 1 \) & 1 & 1 & & & & \\
    \hline
    \small\( n = 2 \) & 1 & 2 & 1 & & & \\
    \hline
    \small\( n = 3 \) & 1 & 3 & 3 & 1 & & \\
    \hline
    \small\( n = 4 \) & 1 & \textbf{4} & \textbf{6} & 4 & 1 & \\
    \hline
    \small\( n = 5 \) & 1 & 5 & \textbf{10} & 10 & 5 & 1 \\
    \hline
\end{tabularx}
\end{center}
  \end{minipage}
  \begin{minipage}{.4\textwidth}
 \begin{center}
\begin{tikzpicture}
  \foreach \n in {0, 1, 2, 3} {
        \foreach \k in {0, ..., \n} {
          \node at (\k - \n/2, -\n) {$\displaystyle\binom{\n}{\k}$};
        }
    }
    \foreach \n in {4} {
        \foreach \k in {0, 2, 3, 4} {
          \node at (\k - \n/2, -\n) {$\displaystyle\binom{\n}{\k}$};
        }
    }
    \foreach \n in {4} {
        \foreach \k in {1, 2} {
          \node at (\k - \n/2, -\n) {$\displaystyle\pmb{\binom{\n}{\k}}$};
        }
    }
    \foreach \n in {5} {
        \foreach \k in {0, 1, 3, 4, 5} {
          \node at (\k - \n/2, -\n) {$\displaystyle\binom{\n}{\k}$};
        }
    }
    \foreach \n in {5} {
        \foreach \k in {2} {
          \node at (\k - \n/2, -\n) {$\displaystyle\pmb{\binom{\n}{\k}}$};
        }
    }
\end{tikzpicture}
\end{center}
  \end{minipage}
\end{exemple}

\begin{prop}
  Pour $n$ et $k$ entiers naturels avec $0\leq k\leq n$, on a
  $\binom{n}{k} = \binom{n}{k}$.
\end{prop}

\begin{exemple}
  On constate une symétrie dans le tableau, par exemple
  $\binom{4}{3}=\binom{4}{4-3}=\binom{4}{1}=4$.
\end{exemple}

\subsection{Définition et propriété de la loi binomiale}

\begin{defi}{Loi binomiale}
  Soit $n\in\mathbb{N}^*$ et $p\in]0; 1[$. On considère le schéma de Bernoulli
  de taille $n$ dont les épreuves de Bernoulli ont pour probabilité de succès
  $p$. La \textbf{loi binomiale} de paramètres $n$ et $p$ est la loi de la
  variable aléatoire donnant le nombre de succès sur les $n$ répétitions. On la
  note $\mathscr B(n; p)$.
\end{defi}

\begin{exemple}
  On reprend l'exemple~\ref{ex:bern} : un schéma de Bernoulli de taille $2$ avec
  $p=0,4$. La variable $X$ donnant le nombre de succès à l'issue du schéma suit
  ainsi la loi $\mathscr B(2; 0,4)$.
\end{exemple}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr B(n; p)$. Pour tout
  entier $k$ entre $0$ et $n$, on a 
  \[
    P(X=k) = \binom{n}{k}\times p^k \times (1-p)^{n-k}.
  \]
\end{prop}

\begin{app}
  \label{app:cinq-dés}
  On lance cinq fois de suite un dé équilibré dont les faces sont numérotées de
  $1$ à $6$. On note $X$ la variable aléatoire comptant le nombre de $6$ obtenus
  à l'issue des cinq lancés.
  \begin{enumerate}
    \item Quelle loi suit la variable $X$ ?
    \item Quelle est la probabilité d'obtenir exactement un six à l'issue des
      cinq lancés ?
  \end{enumerate}
\end{app}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi binomiale $\mathscr B(n; p)$,
  on a alors
    \begin{align*}
      E(X) &= np &
      V(X) &= np(1-p) &
      \sigma(X) &= \sqrt{np(1-p)}.
    \end{align*}
\end{prop}

\begin{app}
  On reprend la variable $X$ de l'application~\ref{app:cinq-dés}. Donner
  l'espérance $E(X)$ et la variance $V(X)$.
\end{app}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi binomiale $\mathscr B(n; p)$.
  Le diagramme en barres associé à $X$ est en forme de cloque, approximativement
  centré sur son espérance $E(X)$.
\end{prop}

\begin{exemple}~\\[-6mm]
  \label{ex:binom-20-0.6}
  \begin{minipage}{.27\textwidth}
    On a représenté le diagramme en barres associé à une variable $X$ suivant la
    loi $\mathscr B(20; 0,6)$. Par exemple pour $k=10$, on a 
    \[
      P(X=10)\approx0,12.
    \]
    On a aussi $E(X)=12$ et on constate que le diagramme
    est en forme de cloche, centré autour de $k=12$.
  \end{minipage}
  \hfill
  \begin{minipage}{.7\textwidth}
    \begin{center}
      \begin{tikzpicture}%[scale=.9]
    \begin{axis}[
        ybar,
        bar width=3mm,
        ymin=0,
        ymax=0.18,
        xmax=21,
        xtick=data,
        ytick={0, 0.05, 0.1, 0.15},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$},
        xlabel={$k$},
        ylabel={$P(X=k)$},
        width=12cm,
        height=8cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 1.0995116277760013e-08
1 3.2985348833280036e-07
2 4.700412208742404e-06
3 4.230370987868163e-05
4 0.00026968615047659537
5 0.0012944935222876579
6 0.004854350708578717
7 0.014563052125736149
8 0.035497439556481845
9 0.0709948791129637
10 0.11714155053639011
11 0.15973847800416832
12 0.17970578775468937
13 0.1658822656197132
14 0.1244116992147849
15 0.07464701952887093
16 0.03499079040415825
17 0.01234969073087938
18 0.003087422682719845
19 0.00048748779200839646
20 3.6561584400629733e-05
        } \datatable

        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
\end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}

\subsection{Intervalles de fluctuation et loi binomiale}

\begin{defi}{Intervalle de fluctuation}
  Soit $X$ une variable aléatoire suivant une loi binomiale et soit $\alpha\in]0;1[$
    un réel. Un intervalle $\left[ a; b \right]$ avec $a<b$ deux réels tel que 
    \[
      P(a\leq X \leq b) \geq 1-\alpha
    \]
    est appelé \textbf{intervalle de fluctuation} au seuil $1-\alpha$ (ou au risque
    $\alpha$) associé à $X$.
\end{defi}

\begin{intuition}
  Si $I$ est un intervalle de fluctuation au risque $\alpha$, cela signifie que
  le risque pour que $X$ ne prenne pas une valeur dans l'intervalle $I$ est
  $\alpha$. Typiquement, on cherchera des intervalles de fluctuation avec un
  risque faible, c'est-à-dire un intervalle dans lequel $X$ va certainement
  prendre ses valeurs.
\end{intuition}

\begin{exemple}
  On reprend l'exemple~\ref{ex:binom-20-0.6} avec $\alpha=0,05=5\%$. On a
  $P(X\leq 7)\approx2,1\% \leq 2,5\%$ et $P(X\geq17)\approx1,6\%\leq2,5\%$. On
  en déduit que $\left[ 8, 16 \right]$ est un intervalle de fluctuation au
  risque $5\%$, c'est-à-dire que $X$ a plus de $95\%$ de chance de prendre des
  valeurs entre $8$ et $16$.
\end{exemple}

\section{Loi géométrique}

\begin{defi}{Loi géométrique}
  On considère une épreuve de Bernoulli pour laquelle la probabilité d'un succès
  est $p$ et on répète cette épreuve de Bernoulli de manière indépendante
  jusqu'à l'obtention d'un succès. On considère la variable aléatoire $X$
  donnant le nombre d'essais nécessaires pour obtenir ce succès. Par définition,
  $X$ suit la \textbf{loi géométrique} de paramètre $p$, notée $\mathscr G(p)$.
\end{defi}

\begin{exemple}
  \label{ex:de6}
  On lance un dé équilibré à six faces numérotées de $1$ à $6$ jusqu'à
  l'obtention d'un $6$. La variable aléatoire $X$ donnant le nombre de lancers
  nécessaires pour obtenir un $6$ est la loi géométrique de paramètre
  $p=\frac{1}{6}$.
\end{exemple}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr G(p)$, et
  $k\in\mathbb{N}^*$. On a alors
  \begin{align*}
    P(X=k) &= (1-p)^{k-1}\times p &
    P(X > k) &= (1-p)^k &
    P(X\leq k) &= 1-(1-p)^k
  \end{align*}
\end{prop}

\begin{intuition}
  Ces trois formules sont intuitives. La probabilité de succès est $p$ et celle
  de l'échec $1-p$.
  \begin{itemize}
    \item Réussir au bout du $k$-ième essai signifie $k-1$ échecs puis $1$ succès.
    \item Ne pas avoir encore réussi au bout du $k$-ième essai signifie $k$
      échecs.
    \item Avoir réussi en $k$ essais ou moins est le contraire d'avoir obtenu
      $k$ échecs.
  \end{itemize}
\end{intuition}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr G(p)$, on a alors
  $E(X) = \frac{1}{p}$.
\end{prop}

\begin{app}
  On reprend l'exemple~\ref{ex:de6} : on a un dé équilibré à six faces numérotée
  de $1$ à $6$. On considère $X$ la variable aléatoire donnant le nombre de
  lancers nécessaires pour obtenir un six.
  \begin{enumerate}
    \item Quelle est l'espérance de $X$ ?
    \item Quelle est la probabilité de faire un six en $3$ lancers ou moins ?
  \end{enumerate}
\end{app}

\begin{propnom}{Absence de mémoire}
  \label{prop:memo}
  Soit $X$ une variable aléatoire suivant une loi géométrique, et soit $k,
  t\in\mathbb{N}^*$ deux entiers non nuls, on a alors
  \[
    P_{X > k}\left( X > k+t \right) = P\left( X > t \right).
  \]
  \textbf{Réciproquement}, si une variable aléatoire à valeurs dans
  $\mathbb{N}^*$ vérifie
  $P_{X > k}\left( X > k+t \right) = P\left( X > t \right)$
  pour tous entiers non nuls $k,t\in\mathbb{N}^*$, alors $X$ suit une loi
  géométrique.
\end{propnom}

\begin{intuition}
  La propriété~\ref{prop:memo} signifie que «~obtenir $k+t$ échecs~» sachant
  qu'on en a déjà obtenu $k$ est la même chose que «~obtenir $t$ échecs~» : on
  «~oublie~» les $k$ premiers échecs, et c'est comme si on repartait de zéro.
\end{intuition}

\begin{exemple}
  On reprend encore l'exemple~\ref{ex:de6}. La propriété~\ref{prop:memo}
  implique qu'on a 
  \begin{align*}
    P_{X>5}\left( X > 8 \right) &= P\left( X > 3 \right) \\
    &= \left( 1-\frac{1}{6} \right)^3 \\
    &\approx 0,579.
  \end{align*}
  Ici, on a exprimé qu'obtenir $8$ échecs sachant qu'on avait déjà obtenu $5$
  échecs est la même chose qu'obtenir $3$ échecs dès le départ.
\end{exemple}

\begin{propnom}{Aspect graphique de la loi géométrique}
    Soit $X$ une variable aléatoire suivant la loi géométrique $\mathscr G(p)$.
    Son diagramme en barres associé correspond à une décroissance exponentielle
    et $p=P(X=1)$ est la hauteur de la première barre.
\end{propnom}

\begin{exemple}
    On a ici représenté le diagramme en barres lié à l'exemple~\ref{ex:de6},
    c'est-à-dire $p=\frac{1}{6}\approx0,17$.
    \begin{center}
      \begin{tikzpicture}%[scale=.9]
    \begin{axis}[
        ybar,
        bar width=3mm,
        xmin=0,
        ymin=0,
        ymax=0.19,
        xmax=31,
        xtick=data,
        ytick={0, 0.05, 0.1, 0.15},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$},
        xlabel={$k$},
        ylabel={$P(X=k)$},
        width=\textwidth,
        height=7cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
          k prob
1 0.16666666666666666
2 0.1388888888888889
3 0.11574074074074076
4 0.09645061728395063
5 0.08037551440329219
6 0.06697959533607684
7 0.05581632944673069
8 0.04651360787227558
9 0.038761339893562986
10 0.032301116577969156
11 0.02691759714830763
12 0.022431330956923026
13 0.018692775797435855
14 0.015577313164529882
15 0.012981094303774901
16 0.010817578586479085
17 0.009014648822065905
18 0.0075122073517215875
19 0.006260172793101323
20 0.005216810660917769
21 0.0043473422174314744
22 0.003622785181192896
23 0.0030189876509940797
24 0.002515823042495067
25 0.002096519202079222
26 0.0017470993350660188
27 0.0014559161125550157
28 0.0012132634271291797
29 0.0010110528559409832
30 0.0008425440466174861
        } \datatable

        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
\end{tikzpicture}
    \end{center}
\end{exemple}

\end{document}
