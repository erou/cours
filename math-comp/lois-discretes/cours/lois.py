# Lois.py
# =======
#
# Petits scripts pour obtenir des valeurs numériques à utiliser dans les
# fichiers de cours ou d'exercices, pour obtenir des graphiques corrects.
#
# *Nota bene :* ne pas faire confiance à ChatGPT !

from math import comb

def geom(p, k=20):
    for i in range(k):
        print(i+1, (1-p)**i * p)

def binom(n, p):
    for i in range(n+1):
        print(i, comb(n, i)*p**i*(1-p)**(n-i))

def frepbinom(n, p):
    s=0
    for i in range(n+1):
        s += comb(n, i)*p**i*(1-p)**(n-i)
        print(i, s)

frepbinom(100, .4)
