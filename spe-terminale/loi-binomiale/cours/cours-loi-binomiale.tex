\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Loi binomiale -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\renewcommand{\emptyset}{\varnothing}
\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Modèle de la succession d’épreuves indépendantes : la probabilité d’une
%     issue (x1,…,xn) est égale au produit des probabilités des composantes xi.
%     Représentation par un produit cartésien, par un arbre.
% [x] Épreuve de Bernoulli, loi de Bernoulli.
% [x] Schéma de Bernoulli : répétition de n épreuves de Bernoulli indépendantes.
% [x] Loi binomiale B(n,p) : loi du nombre de succès. Expression à l’aide des
%     coefficients binomiaux.
%
% Capacités attendues
% -------------------
%
% [ ] Modéliser une situation par une succession d’épreuves indépendantes, ou
%     une succession de deux ou trois épreuves quelconques. Représenter la
%     situation par un arbre. Calculer une probabilité en utilisant
%     l’indépendance, des probabilités conditionnelles, la formule des
%     probabilités totales.
% [ ] Modéliser une situation par un schéma de Bernoulli, par une loi binomiale.
%     Utiliser l’expression de la loi binomiale pour résoudre un problème de
%     seuil, de comparaison, d’optimisation relatif à des probabilités de nombre
%     de succès.
% [ ] Dans le cadre d’une résolution de problème modélisé par une variable
%     binomiale X, calculer numériquement une probabilité du type P(X = k), P(X
%     ⩽ k), P(k ⩽ X ⩽ k’ ), en s’aidant au besoin d’un algorithme ; chercher un
%     intervalle I pour lequel la probabilité P(X ∈ I ) est inférieure à une
%     valeur donnée α, ou supérieure à 1 - α.
%
% Démonstrations
% --------------
%
% + Expression de la probabilité de k succès dans le schéma de Bernoulli.
%
% Exemples d'algorithme
% ---------------------
%
% + Simulation de la planche de Galton.
% + Problème de la surréservation. Étant donné une variable aléatoire binomiale
%   X et un réel strictement positif α, détermination du plus petit entier k tel
%   que P(X > k) ⩽ α.
% + Simulation d’un échantillon d’une variable aléatoire.
%

\title{\vspace{-20mm}Chapitre 9 : Succession d'épreuves indépendantes et loi
binomiale}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/loi-binomiale.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Sucession d'épreuves indépendantes}

\begin{defi}{Univers associé à une succession d'épreuves indépendantes}
  Soit une succession de $n$ épreuves indépendantes dont les univers associés
  sont respectivement $\Omega_1, \Omega_2, \ldots, \Omega_n$. L'univers associé
  à cette succession de $n$ épreuves est le produit cartésien
  \[
    \Omega_1\times\Omega_2\times\cdots\times\Omega_n.
  \]
\end{defi}
\begin{exemple}
  \label{exemple:succession}
  On lance successivement une pièce, un dé cubique numéroté de $1$ à $6$, puis
  un dé tétraédrique numéroté de $1$ à $4$ on note les résultats.
  L'univers associé à cette succession de trois épreuves est
  \[
    \Omega = \left\{ \text{PILE}, \text{FACE} \right\}\times\left\{ 1; 2; 3; 4;
    5; 6 \right\}\times\left\{ 1; 2; 3; 4 \right\}.
  \]
\end{exemple}
\begin{rmq}
  Si les $n$ épreuves sont identiques et que l'on note $\Omega$ l'univers
  associé à chacune d'elle, l'univers associé à la succession de ces $n$
  épreuves indépendantes et identiques est $\Omega^n$.
\end{rmq}

\begin{prop}
  Soit une succession de $n$ épreuves indépendantes. La probabilité d'obtenir
  une issue $(x_1; x_2; \ldots; x_n)$ est
  \[
    P(x_1; x_2; \ldots; x_n) = P(x_1)\times P(x_2)\times \ldots\times P(x_n)
  \]
\end{prop}
\begin{exemple}
  On reprend l'exemple~\ref{exemple:succession}. La probabilité de l'issue
  $(\text{PILE}; 5; 1)$ est 
  \[
    P(\text{PILE}; 5; 1)=P(\text{PILE})\times P(5)\times
    P(1)=\frac{1}{2}\times\frac{1}{6}\times\frac{1}{4} = \frac{1}{48}.
  \]
\end{exemple}
\begin{rmq}
  On peut représenter les épreuves successives par un arbre, mais avec plus de
  trois épreuves, cela devient généralement très compliqué.
\end{rmq}

\begin{app}
  On considère un jeu composé de trois parties : on lance un dé équilibré à $20$
  faces, puis on pioche une boule dans une urne composée de $5$ boules
  indiscernables numérotées de $1$ à $5$, et enfin on fait tourner une roue
  composée de trois secteurs angulaires de même taille, numérotés $1$, $5$ et
  $10$. On gagne si on obtient uniquement des nombres pairs.\\
  Quelle est la probabilité de gagner à ce jeu ?
\end{app}

\section{Épreuve, loi et schéma de Bernoulli}

\begin{comment}
\begin{tikzpicture}[scale=.8,level 1/.style={sibling distance=2cm},
    level 2/.style={sibling distance=1cm}]
  \node {} [grow'=right, level distance=3cm]
  child {
    node {$S$}
    edge from parent node[above] {$0,5$}
  }
  child {
    node {$\overline S$}
    edge from parent node[below] {$0,5$}
  };
\end{tikzpicture}
\end{comment}
\begin{defi}{Épreuve de Bernoulli}
  Une \textbf{épreuve de Bernoulli} est une expérience aléatoire possédant deux
  issues : le succès (noté $S$) et l'échec (noté $\overline S$ ou $E$).
\end{defi}

\begin{intuition}
  Une épreuve de Bernoulli permet de modéliser n'importe quelle expérience qui
  ne possède que deux alternatives ; l'une d'elle sera le succès et l'autre
  l'échec.
\end{intuition}

\begin{exemple}
  On dispose d'une urne contenant des tickets gagnants et des tickets perdants.
  On tire au hasard un ticket et on regarde s'il est gagnant ou non. Cette
  expérience aléatoire est une épreuve de Bernoulli.
\end{exemple}

\begin{app}
  Citer une expérience qui constitue une épreuve de Bernoulli et une expérience
  qui ne consitue pas une épreuve de Bernoulli.
\end{app}

\begin{defi}{Loi de Bernoulli}
  \begin{minipage}{.6\textwidth}
    Soit $p\in]0; 1[$. Une variable aléatoire $X$ suit la loi de Bernoulli de
      paramètre $p$, notée $\mathscr B(p)$, si on a $P(X=0)=1-p$ et $P(X=1)=p$.
      On synthétise avec le tableau ci-contre.
  \end{minipage}
  \hfill
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tabularx}{.9\textwidth}{|c|Y|Y|}
        \hline
        $x_i$ & $0$ & $1$ \\
        \hline
        $P(X=x_i)$ & $1-p$ & $p$ \\
        \hline
      \end{tabularx}
    \end{center}
  \end{minipage}
\end{defi}
\begin{intuition}
  Une variable aléatoire $X$ qui suit une loi de Bernoulli $\mathscr B(p)$
  compte si on a obtenu un succès ou un échec à une épreuve de Bernoulli dont la
  probabilité de succès est $p$.
\end{intuition}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr B(p)$, on a alors
    \begin{align*}
      E(X) &= p &
      V(X) &= p(1-p) &
      \sigma(X) &= \sqrt{p(1-p)}.
    \end{align*}
\end{prop}

\begin{defi}{Schéma de Bernoulli}
  Un \textbf{schéma de Bernoulli} de taille $n\in\mathbb{N}$ est la succession de $n$
  épreuves de Bernoulli identiques et idépendantes.
\end{defi}

\begin{exemple}%~\\[-7mm]
  \label{ex:bern}
  \begin{minipage}{.6\textwidth}
    On lance successivement une pièce de monnaie non équilibrée dont la
    probabilité d'un succès «~tomber sur pile~», noté $P$, est $0,4$. On note
    $F$ l'événement «~tomber sur face~».
    \begin{itemize}
      \item On obtient un schéma de Bernoulli de taille $2$ que l'on peut représenter par
        l'arbre ci-contre.
      \item On calcule les probabilités des différents événements avec les
        règles classiques sur les arbres.
    \end{itemize}
  \end{minipage}
    \begin{minipage}{.4\textwidth}
    \begin{center}
\begin{tikzpicture}[scale=.8,level 1/.style={sibling distance=2cm},
    level 2/.style={sibling distance=1cm}]
  \node {} [grow'=right, level distance=3cm]
  child {
    node {$P$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
    edge from parent node[below] {$0,6$}
    }
    edge from parent node[above] {$0,4$}
  }
  child {
    node {$F$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
      edge from parent node[below] {$0,6$}
    }
    edge from parent node[below] {$0,6$}
  };
\end{tikzpicture}
\end{center}
  \end{minipage}
\end{exemple}

\section{Loi binomiale}
\subsection{Définitions}

\begin{defi}{Loi binomiale}
  Soit $n\in\mathbb{N}^*$ et $p\in]0; 1[$. On considère le schéma de Bernoulli
  de taille $n$ dont les épreuves de Bernoulli ont pour probabilité de succès
  $p$. La \textbf{loi binomiale} de paramètres $n$ et $p$ est la loi de la
  variable aléatoire donnant le nombre de succès sur les $n$ répétitions. On la
  note $\mathscr B(n; p)$.
\end{defi}

\begin{exemple}~\\[-5mm]
  \begin{minipage}{.6\textwidth}
  On reprend l'exemple~\ref{ex:bern} : un schéma de Bernoulli de taille $2$ avec
  $p=0,4$. La variable $X$ donnant le nombre de succès à l'issue du schéma suit
  ainsi la loi $\mathscr B(2; 0,4)$.
  \end{minipage}
    \begin{minipage}{.4\textwidth}
    \begin{center}
\begin{tikzpicture}[scale=.8,level 1/.style={sibling distance=2cm},
    level 2/.style={sibling distance=1cm}]
  \node {} [grow'=right, level distance=3cm]
  child {
    node {$P$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
    edge from parent node[below] {$0,6$}
    }
    edge from parent node[above] {$0,4$}
  }
  child {
    node {$F$}
    child {
      node {$P$}
      edge from parent node[above] {$0,4$}
    }
    child{
      node {$F$}
      edge from parent node[below] {$0,6$}
    }
    edge from parent node[below] {$0,6$}
  };
  \node at (7.5, 1.5) {$X=2$};
  \node at (7.5, .5) {$X=1$};
  \node at (7.5, -.5) {$X=1$};
  \node at (7.5, -1.5) {$X=0$};
\end{tikzpicture}
\end{center}
  \end{minipage}
\end{exemple}
\begin{app}
  Donner l'arbre de probabilité associé à un schéma de Bernoulli de taille $n=3$
  et de probabilité de succès $p=0,1$. Donner les valeurs de $X$ associées à
  chaque branche.
\end{app}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi $\mathscr B(n; p)$. Pour tout
  entier $k$ entre $0$ et $n$, on a
  \[
    P(X=k) = \binom{n}{k}\times p^k \times (1-p)^{n-k}.
  \]
\end{prop}

\begin{app}
  On lance six fois une pièce de monnaie équilibrée. On note $X$ la variable
  aléatoire qui compte le nombre de fois où l'on a obtenu face.
  \begin{multicols}{2}
  \begin{enumerate}
    \item Déterminer la loi de probabilité de $X$.
    \item Déterminer $P(X=0)$ et $P(X=1)$.
    \item Déterminer $P(X\leq1)$ et $P(X\leq4)$.
    \item Déterminer $P(X\geq5)$.
  \end{enumerate}
  \end{multicols}
\end{app}

\subsection{Représentation graphique}

\begin{prop}
  Soit $X$ une variable aléatoire suivant la loi binomiale $\mathscr B(n; p)$.
  Le diagramme en barres associé à $X$ est en forme de cloche, approximativement
  centré sur son espérance $E(X)=np$.
\end{prop}
\noindent
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}%[width=0.5\textwidth]
    \begin{axis}[
        ybar,
        bar width=2mm,
        ymin=0,
        ymax=0.25,
        xmax=21,
        xtick=data,
        xticklabels={,,$2$,,$4$,,$6$,,$8$,,$10$,,$12$,,$14$,,$16$,,$18$,,$20$},
        ytick={0, 0.05, 0.1, 0.15, 0.2},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$, $0.2$},
        xlabel={$k$},
        ylabel={$P(X=k)$},
        width=\textwidth,
        height=6cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 1.0995116277760013e-08
1 3.2985348833280036e-07
2 4.700412208742404e-06
3 4.230370987868163e-05
4 0.00026968615047659537
5 0.0012944935222876579
6 0.004854350708578717
7 0.014563052125736149
8 0.035497439556481845
9 0.0709948791129637
10 0.11714155053639011
11 0.15973847800416832
12 0.17970578775468937
13 0.1658822656197132
14 0.1244116992147849
15 0.07464701952887093
16 0.03499079040415825
17 0.01234969073087938
18 0.003087422682719845
19 0.00048748779200839646
20 3.6561584400629733e-05
        } \datatable

        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
    \node at (2,3) {$\mathscr B(20;0,6)$};
\end{tikzpicture}
    \end{center}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}%[width=0.5\textwidth]
    \begin{axis}[
        ybar,
        bar width=2mm,
        ymin=0,
        ymax=0.25,
        xmin=-1,
        xmax=21,
        xtick=data,
        ytick={0, 0.05, 0.1, 0.15, 0.2},
        xticklabels={,,$2$,,$4$,,$6$,,$8$,,$10$,,$12$,,$14$,,$16$,,$18$,,$20$},
        yticklabels={$0$, $0.05$, $0.1$, $0.15$, $0.2$},
        xlabel={$k$},
        ylabel={$P(X=k)$},
        width=\textwidth,
        height=6cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 0.011529215046068483
1 0.05764607523034241
2 0.13690942867206324
3 0.20536414300809488
4 0.21819940194610077
5 0.17455952155688062
6 0.1090997009730504
7 0.05454985048652519
8 0.022160876760150862
9 0.007386958920050287
10 0.0020314137030138287
11 0.0004616849325031429
12 8.656592484433929e-05
13 1.3317834591436814e-05
14 1.6647293239296018e-06
15 1.6647293239296019e-07
16 1.3005697843200014e-08
17 7.65041049600001e-10
18 3.187671040000004e-11
19 8.388608000000009e-13
20 1.0485760000000012e-14
        } \datatable
        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
    \node at (4,3) {$\mathscr B(20;0,2)$};
\end{tikzpicture}
    \end{center}
  \end{minipage}

\begin{intuition}
  Plus les valeurs de $p$ sont petites, et plus le nombre de succès est faible.
\end{intuition}

\subsection{Intervalle de fluctuation}

\begin{defi}{Intervalle de fluctuation}
Soit $X$ une variable aléatoire suivant une loi binomiale, $\alpha\in]0;1[$ et
  $a, b\in\mathbb{R}$ deux réels. Un intervalle $[a;b]$ tel que $P(a\leq X\leq
  b)\geq1-\alpha$ est appelé \textbf{intervalle de fluctuation} au seuil de
  $1-\alpha$ (ou au risque $\alpha$) associé à $X$.
\end{defi}
\begin{intuition}
  On prend souvent $\alpha=0,05$. Dans ce cas, trouver un intervalle de
  fluctuation au seuil $1-\alpha$ revient à trouver une plage de valeurs dans
  laquelle $X$ sera avec $95\%$ de chance. Autrement dit, le risque que $X$ ne
  fasse pas partie de l'intervalle de fluctuation est de $5\%$.
\end{intuition}
\begin{exemple}
  On considère une variable aléatoire $X$ suivant une loi binomiale $\mathscr
  B(100;0,4)$. On représente le diagramme en barres de $X$ ci-dessous. En
  théorie, $X$ peut prendre n'importe quelle valeur entière entre $0$ et $100$.
  En pratique, $X$ prendra des valeurs proches de son espérance $E(X)=40$. Plus
  précisément, on peut donner des intervalles de fluctuation au seuil
  $\alpha=0,05$.
  \begin{itemize}[label=$\bullet$]
    \item On a $P(X\leq48)\approx 95,8\%$, ce qui signifie que $[0;48]$ est un
      intervalle de fluctuation au seuil $0,95$. En pratique, $X$ prendra des
      valeurs inférieures à $48$ dans plus de $95\%$ des cas.
    \item On a $P(X\geq32)\approx 96,0\%$, ce qui signifie que $[32;100]$ est un
      intervalle de fluctuation au seuil $0,95$. En pratique, $X$ prendra des
      valeurs supérieures à $32$ dans plus de $95\%$ des cas.
    \item On a $P(X<31)\approx2,48\%$ et $P(X>50)\approx1,68\%$. Comme
      $P(X<31)+P(X>50)<0,05$, on en déduit $P(31\leq X\leq50)\geq0,95$, ce qui
      signifie que $[31; 50]$ est un intervalle de fluctuation au seuil $0,95$.
      En pratique, $X$ prendra des valeurs entre $31$ et $50$ dans plus de
      $95\%$ des cas.
  \end{itemize}
  \begin{center}
      \begin{tikzpicture}%[width=0.5\textwidth]
    \begin{axis}[
        ybar,
        bar width=3mm,
        ymin=0,
        ymax=0.12,
        xmin=20,
        xmax=60,
%        xtick={0,5,10,15,20,25,30,35,40,45,50,55},
        ytick={0, 0.02, 0.04, 0.06, 0.08, 0.1},
        yticklabels={$0$, $0.02$, $0.04$, $0.06$, $0.08$, $0.1$},
        xlabel={$k$},
        ylabel={$P(X=k)$},
        width=\textwidth,
        height=8cm,
        xlabel style={at={(axis description cs:1.05,0)}, anchor=west},
        ylabel style={at={(axis description cs:0,1.05)}, anchor=south},
        axis lines=middle,
        axis line style={->}
    ]
        \pgfplotstableread{
            k prob
0 6.533186235000685e-23
1 4.355457490000457e-21
2 1.437300971700151e-19
3 3.1301221161469963e-18
4 5.060364087770976e-17
5 6.477266032346851e-16
6 6.837114145255011e-15
7 6.120845044323533e-14
8 4.743654909350739e-13
9 3.2327129752612444e-12
10 1.961179204991822e-11
11 1.0697341118137208e-10
12 5.289240886190066e-10
13 2.3869394768447475e-09
14 9.888749261213957e-09
15 3.779699717619557e-08
16 1.33864364999026e-07
17 4.409649670556151e-07
18 1.3555589728005946e-06
19 3.900204763847324e-06
20 1.053055286238778e-05
21 2.6744261237810237e-05
22 6.402414053900026e-05
23 0.0001447502307838267
24 0.00030960466028762937
25 0.0006274654448495955
26 0.001206664317018453
27 0.0022047693693670505
28 0.0038320991419951117
29 0.006342784786750531
30 0.01000750488576195
31 0.01506506111835132
32 0.021656025357630025
33 0.029749691400380647
34 0.039082927918147124
35 0.04913282366852782
36 0.059141361823227934
37 0.06819904786822682
38 0.07537789501225069
39 0.07988768360272726
40 0.08121914499610604
41 0.07923819024010345
42 0.07420719403438261
43 0.06672894967432856
44 0.05762954744601103
45 0.047811180103357304
46 0.03811036095195148
47 0.02919091477170752
48 0.02148775670695137
49 0.015202222432128864
50 0.010337511253847628
51 0.006756543303168385
52 0.004244495151990396
53 0.002562714054031937
54 0.0014870069202407537
55 0.0008291190100736325
56 0.00044417089825373173
57 0.00022857917570952277
58 0.00011297591443114346
59 5.361568820461046e-05
60 2.4424924626544765e-05
61 1.0677562678270937e-05
62 4.477687574758782e-06
63 1.8005516173633192e-06
64 6.93962602525446e-07
65 2.5623234554785704e-07
66 9.058719287045452e-08
67 3.064641350841247e-08
68 9.915016135074625e-09
69 3.0655122349989183e-09
70 9.050559931901571e-10
71 2.549453501944104e-10
72 6.845754773738799e-11
73 1.7505126362071818e-11
74 4.258003709693145e-12
75 9.840719684624158e-13
76 2.1580525624175786e-13
77 4.4842650647638e-14
78 8.815221922185248e-15
79 1.6365812851314388e-15
80 2.864017248980018e-16
81 4.7144316855638167e-17
82 7.282455449244921e-18
83 1.0528851251920367e-18
84 1.420559295894018e-19
85 1.7826626458277874e-20
86 2.072863541660218e-21
87 2.223761653888357e-22
88 2.1900682954961092e-23
89 1.968600715052683e-24
90 1.6040450270799636e-25
91 1.1751245619633436e-26
92 7.663855838891372e-28
93 4.395042774991468e-29
94 2.1819361294283888e-30
95 9.187099492330058e-32
96 3.189965101503493e-33
97 8.769663509287953e-35
98 1.78972724679346e-36
99 2.4104070663884984e-38
100 1.6069380442589993e-40
        } \datatable
        \addplot table[x=k, y=prob] {\datatable};
    \end{axis}
\end{tikzpicture}
\end{center}
\end{exemple}

\begin{prop}
Soit $X$ une variable aléatoire suivant une loi binomiale, $\alpha\in]0;1[$ et
  $a, b\in\mathbb{R}$ deux réels. Si on a $P(X<a)\leq\dfrac{\alpha}{2}$ et
  $P(X>b)\leq\dfrac{\alpha}{2}$ alors l'intervalle $[a;b]$ est un intervalle de
  fluctuation au seuil de $1-\alpha$ associé à $X$. Dans ce cas, on dit que
  c'est un intervalle de fluctuation \textbf{centré}.
\end{prop}

\end{document}
