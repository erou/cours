\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Continuité -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Fonction continue en un point (définition par les limites)
% [x] Fonction continue sur un intervalle.
% [x] Toute fonction dérivable est continue.
% [x] Image d'une suite convergente par une fonction continue
% [x] Théorème des valeurs intermédiaires.
% [x] TVI dans le cas des fonctions continues *strictement monotones*.
%
% Capacités attendues
% -------------------
%
% [ ] Étudier les solutions d'une équations du type f(x)=k : existence, unicité,
%     encadrement.
% [ ] Pour une fonction continue f d'un intervalle dans lui-même, étudier une
%     suite définie par une relation de récurrence u_{n+1}=f(u_n).
%
% Démonstrations
% --------------
%
% **Pas de démonstration exigible dans ce chapitre.**
%
% Exemples d'algorithme
% ---------------------
%
% + Méthode de dichotomie
% + Méthode de Newton, méthode de la sécante
%
% TODO
% ====
%
% + Pour l'application du TVI, bien expliciter comment on rédige en pratique :
%   + on a toujours ou presque une fonction strictement monotone ;
%   + on prouve que c'est strictement monotone en disant que la dérivée ne
%     s'annulle pas sauf en un nombre fini de point ;
%   + on dit que la valeur recherchée appartient à l'intervalle image, ce qui
%     permet de généraliser le TVI à autre chose qu'un segment ;
%   + et on rappelle que la fonction est continue évidemment.


\title{\vspace{-20mm}Chapitre 3 : Continuité}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/continuite.html}
\vspace{-7mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Définition et propriétés}
\subsection{Notion de continuité}

\begin{defi}{Continuité}
  Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ un réel
  appartenant à $I$. On dit que la fonction $f$ est \textbf{continue en un point} $a$ si
  $f$ admet une limite en $a$ et
  \[
    \lim_{x\to a}f(x)=f(a).
  \]
  On dit que $f$ est \textbf{continue sur un intevalle} $I$ si $f$ est continue en tout
  point de $I$.
\end{defi}

\begin{intuition}
  Une fonction continue est une fonction que l'on peut tracer « à main levée »,
  c'est-à-dire dont la courbe représentative est « en un seul morceau » : elle
  n'a pas de « saut » en certaines valeurs.
\end{intuition}

\begin{exemple}
  On a représenté ci-dessous deux fonctions $f$ et $g$. Celle de gauche, nommée
  $f$, est continue alors
  que celle de droite, nommée $g$, ne l'est pas : il y a un saut en $x=1$. On a
  en particulier
  \[
    \lim_{x\to1^-}g(x) \neq \lim_{x\to1^+}g(x).
  \]
  \\\noindent
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]
        \addplot[red, domain=-5.5:5.5, samples=201] {(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \end{axis}
        \node[red] at (3.3,3.3) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
    \hfill
   \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]
        \addplot[blue!70!black, domain=-5.5:1, samples=201] {(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \addplot[blue!70!black, domain=1:5.5, samples=201] {2+(cos(deg(exp(0.3*x)))*0.25*x^4+1)/(x^2+1)};
        \end{axis}
        \node[blue!70!black] at (3.7,4.2) {$\mathscr C_g$};
      \end{tikzpicture}
    \end{center}
    \hfill
  \end{minipage}
\end{exemple}

\begin{app}
  On définit la fonction $f$ sur $\mathbb{R}$ par 
  $f(x)=
  \begin{cases}
    x+6 & \text{si } x\leq 3\\
    x^2 & \text{si } x > 3
  \end{cases}
  $. Cette fonction est-elle continue en $3$ ?
\end{app}

\subsection{Opérations et fonctions continues}

\begin{propadm}
  Toutes les fonctions de référence (polynômes, inverse, valeur absolue,
  exponentielle, racine carrée, \emph{etc.}) sont continues sur leur intervalle
  de définition.
\end{propadm}

\begin{prop}
  Toute fonction dérivable sur un intervalle $I$ est continue sur $I$.
\end{prop}
\begin{proof}
  Soit $f$ une fonction définie sur un intervalle $I$ et $x,\,a\in I$ deux réels
  de $I$ tels que $x\neq a$. On écrit
  $f(x)-f(a)=\frac{f(x)-f(a)}{x-a}\times\left( x-a \right)$ et on regarde 
  les limites quand $x$ tend vers $a$.
\end{proof}

\begin{prop}
  Soit $f$ et $g$ deux fonctions continues sur un intervalle $I$. Alors les
  fonctions $f+g$ et $f\times g$ sont continues sur $I$. Si de plus la fonction
  $g$ ne s'annule pas sur $I$, alors $\displaystyle\frac{f}{g}$ est continue sur $I$.
\end{prop}
\begin{proof}
  Ce résultat vient des opérations sur les limites.
\end{proof}

\begin{app}
  Justifier que la fonction $f$ définie sur $I=[0; +\infty[$ par $f(x)=x^2+\sqrt
    x$ est continue sur $I$.
\end{app}

\section{Le théorème des valeurs intermédiaires}
\subsection{Cas général}
\noindent
\begin{minipage}{.6\textwidth}
\begin{thmnom}{Théorème des valeurs intermédaires}
  Si $f$ est continue sur $\left[ a; b \right]$ alors, pour tout réel $k$
  compris entre $f(a)$ et $f(b)$, l'équation $f(x)=k$ admet \textbf{au moins}
  une solution dans $\left[ a; b \right]$.
\end{thmnom}
\begin{rmq}
  Autrement dit, tout réel $k$ compris entre $f(a)$ et $f(b)$ admet
  \textbf{au moins} un antécédent par $f$ dans $\left[ a; b \right]$.
\end{rmq}
\end{minipage}
\hfill
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.1, xmin=-.1, xmax=5,
          ymax=5, y=1cm, x=1cm, xlabel=$x$, ylabel=$y$]
          \addplot[green!50!black, thick, samples=301, domain=.5:4.5]{4-.5*x+cos(deg(3.14*x)};
        \end{axis}

        \node[green!50!black] at (2.5, 4) {$\mathscr C_f$};

        \draw[red!70!black, dashed, thick] (.6, .1) node[below] {$a$} --++ (0, 3.75);
        \draw[red!70!black, dashed, thick] (4.6, .1) node[below] {$b$} --++ (0, 1.75);

        \draw[blue!70!black, dashed, thick] (.1, 3.85) node[left] {$f(a)$} --++
        (0.5, 0);
        \draw[blue!70!black, dashed, thick] (.1, 1.85) node[left] {$f(b)$} --++
        (4.5, 0);
        \draw[blue!70!black, very thick] (.1, 1.85) -- (.1, 3.85);
        \draw[red!70!black, very thick] (.6, .1) -- (4.6, .1);

        \draw[orange!50!black, dotted, very thick] (.1, 3.2) node[left] {$k$} --++ (4.5, 0);
      \end{tikzpicture}
    \end{center}
  \end{minipage}

\begin{intuition}
  Ce résultat signifie qu'une fonction continue sur $\left[ a; b \right]$ passe
  au moins une fois par toutes les valeurs comprises entre $f(a)$ et $f(b)$.
\end{intuition}

\subsection{Cas des fonctions strictement monotones}

\begin{thm}
  Si $f$ est une fonction continue et strictement monotone sur $\left[ a; b
  \right]$, alors pour tout réel $k$ compris entre $f(a)$ et $f(b)$, l'équation
  $f(x)=k$ admet une \textbf{unique} solution dans $\left[ a; b \right]$.
\end{thm}
\noindent
 \begin{proof}
  L'existence de la solution vient du théorème des valeurs intermédiaires.
  L'unicité vient de la stricte monotonie : si $c_1$ et $c_2$ sont deux
  solutions distinctes, alors
    $f(c_1)<f(c_2)$ ou $f(c_1)>f(c_2)$,
  ce qui entre en contradiction avec 
  \(
   f(c_1)=f(c_2)=k
  \).
\end{proof}
\begin{exemple}~\\[-5mm]
  \begin{minipage}{.6\textwidth}
   Dans l'illustration ci-contre, on a représenté la fonction
   \[
     f:x\mapsto1+\dfrac{x+x^2}{10}.
   \]
   Cette fonction est strictement croissante.
   On voit que pour toute valeur $k\in[a;b]$, on aura une unique solution
   comprise entre $f(a)$ et $f(b)$.
  \end{minipage}
  \hfill
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}%[scale=.8]
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.1, xmin=-.1, xmax=5,
          ymax=5, y=1cm, x=1cm, xlabel=$x$, ylabel=$y$]
          \addplot[green!50!black, thick, samples=301, domain=.5:4.5]{1+.1*x+.1*x^2};
        \end{axis}

        \coordinate (a) at (.6, .1);
        \coordinate (b) at (4.6, .1);
        \coordinate (fa) at (.1, 1.175);
        \coordinate (fb) at (.1, 3.575);

        \node[green!50!black] at (4.5, 4) {$\mathscr C_f$};

        \draw[red!70!black, dashed, thick] (a) node[below] {$a$} --++
        ($(0,0)!(fa)!(0,1)-(0,.1)$);
        \draw[red!70!black, dashed, thick] (b) node[below] {$b$} --++
        ($(0,0)!(fb)!(0,1)-(0,.1)$);

        \draw[blue!70!black, dashed, thick] (fa) node[left] {$f(a)$} --++
        (0.5, 0);
        \draw[blue!70!black, dashed, thick] (fb) node[left] {$f(b)$} --++
        (4.5, 0);
        \draw[blue!70!black, very thick] (fa) -- (fb);
        \draw[red!70!black, very thick] (a) -- (b);

        \draw[orange!50!black, dotted, very thick] (.1, 2.2) node[left] {$k$} --++ (4.5, 0);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{app}
  Soit la fonction $f$ définie sur $\left[ -2; +\infty \right[$ par
  $f(x)=x^3-3x^2+3$.
  \begin{enumerate}
    \item Dresser le tableau de variations de la fonction $f$, on admettra que
      $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$.
    \item \begin{enumerate}
        \item Montrer que l'équation $f(x)=1$ admet au moins une solution dans
          $\left[ -2; +\infty \right[$.
        \item Montrer que l'équation $f(x)=5$ admet une unique solution $\alpha$
          dans $[-2; +\infty[$. Donner un encadrement au dixième près de
            $\alpha$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\section{Continuité et suites}

\begin{prop}
  Soit $f$ une fonction continue sur un intervalle $I$ et $(u_n)$ une suite
  d'éléments de $I$ convergeant vers $a\in I$. Alors
  $\displaystyle\lim_{n\to+\infty}f(u_n)=f(a)$.
\end{prop}
\begin{proof}
  On enchaîne les définitions de $\displaystyle\lim_{n\to+\infty}u_n=a$ et
  $\displaystyle\lim_{x\to a}f(x)=f(a)$.
\end{proof}

\begin{app}
  Déterminer la limite de la suite $(u_n)$ définie sur $\mathbb{N}$ par
  \[
    \forall n\in\mathbb{N},\,u_n = e^{\frac{n+1}{n^2+n+1}}.
  \]
\end{app}

\begin{thmnom}{Théorème du point fixe}
  Soit $f:I\to I$ une fonction définie et continue sur un intervalle $I$ et à
  valeurs dans $I$ et soit $(u_n)$ une suite définie par $u_0\in I$ et, pour
  tout $n\in\mathbb{N}$, par $u_{n+1}=f(u_n)$. Si $(u_n)$ converge vers $\ell\in
  I$, alors $\ell$ est solution de l'équation $f(x)=x$, c'est-à-dire qu'on a
  $f(\ell)=\ell$.
\end{thmnom}

\begin{proof}
  On regarde la limite dans l'égalité $u_{n+1}=f(u_n)$. D'après la propriété
  précédente, on peut remplacer $u_n$ par sa limite car $f$ est continue et on
  obtient $\ell=f(\ell)$.
\end{proof}

\begin{app}
  Soit $(u_n)$ la suite définie par $u_0=1$ et, pour tout $n\in\mathbb{N}$, par
  $u_{n+1}=\frac{3}{u_n+1}$. On admet que la suite $(u_n)$ converge et que, pour
  tout entier $n\in\mathbb{N}$, $u_n\in\left[ 0;3 \right]$. Déterminer la limite
  de la suite $(u_n)$.
\end{app}

\end{document}
