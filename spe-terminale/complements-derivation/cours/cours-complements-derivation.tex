\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Compléments sur la dérivation -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Composée de deux fonctions notation v∘u. Relation (v∘u)'=u'×(v'∘u) pour la
%     dérivée de la composée de deux fonctions dérivables.
% [x] Dérivée seconde d'une fonction.
% [x] Fonction convexe sur un intervalle : définition par la position relative
%     de la courbe représentative et des sécantes. Pour une fonction deux fois
%     dérivable, équivalence admise avec la position par rapport aux tangentes,
%     la croissance de f', la positivité de f''.
% [x] Point d'inflexion.
%
% Capacités attendues
% -------------------
%
% [ ] Calculer la dérivée d'une fonction donnée par une formule simple mettant
%     en jeu  opérations algébriques et composition.
% [ ] Calculer la fonction dérivée, déterminer les limites et étudier les
%     variations d'une fonction construite simplement à partir des fonctions de
%     référence.
% [ ] Démontrer des inégalités en utilisant la convexité d'une fonction.
% [ ] Esquisser l'allure de la courbe représentative d'une fonction f à partir
%     de la donnée de tableaux de variations de f, de f', ou de f''.
% [ ] Lire sur une représentation graphique de f, de f' ou de f'' les
%     intervalles où f est convexe, concave, et les points d'inflexion. Dans le
%     cadre de la résolution de problème, étudier et utiliser la convexité d'une
%     fonction.
%
% Démonstrations
% --------------
%
% + Si f'' est positive, alors la courbe représentative de f est au-dessus de
%   ses tangentes.
%
% Exemples d'algorithme
% ---------------------
%
% **Pas d'algorithme dans ce chapitre.**
%
% Approfondissements possibles
% ----------------------------
%
% + Courbe de Lorenz
% + Dérivée n-ième d'une fonction
% + Inégalité arithmético-géométrique
%

\title{\vspace{-20mm}Chapitre 4 : Compléments sur la dérivation}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/complements-derivation.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Fonctions composées}

\begin{defi}{Composée de fonction}
  Soit $u:I\to J$ une fonction définie sur $I$ et à valeurs dans $J$ et $v$ une
  fonction définie sur $J$. La \textbf{composée} de $u$ par $v$ est la fonction,
  notée $v\circ u$, définie sur $I$ par
  \[
    \forall x\in I,\, \left( v\circ u \right)(x) = v(u(x)).
  \]
\end{defi}

\begin{exemple}
  \label{ex:comp}
  Soit $u$ la fonction définie sur $\mathbb{R}$ par $\forall
  x\in\mathbb{R},\,u(x)=x^2+1$ et $v$ la fonction définie sur $\mathbb{R}_+$ par
  $\forall x\geq0$, $v(x)=\sqrt x$. On a alors 
  \begin{align*}
    \forall x\in\mathbb{R},\, (v\circ u)(x) &= v(u(x)) \\
    &= \sqrt{u(x)} \\
    &= \sqrt{x^2+1}.
  \end{align*}
\end{exemple}

\begin{rmq}
  \textbf{Attention !} L'ordre dans la composition a une importance. Dans
  l'exemple~\ref{ex:comp}, on a 
  \[
    \forall x\in\mathbb{R}_+,\,(u\circ v)(x)=u(v(x))=v(x)^2+1=(\sqrt x)^2+1=x+1.
  \]
\end{rmq}

\begin{app}
  Soit $u:\mathbb{R}\to\mathbb{R}$ la fonction définie par $\forall
  x\in\mathbb{R},\,u(x)=\cos(x)$ et $v:\mathbb{R}\to\mathbb{R}$ la fonction
  définie par $\forall x\in\mathbb{R},\,v(x) = 1-x^3$.
  \begin{enumerate}
    \item Déterminer l'expression de $v\circ u$ et $u\circ v$.
    \item A-t-on $v\circ u=u\circ v$ ?
  \end{enumerate}
\end{app}

\begin{propadm}
  \label{prop:deriv-comp}
  Soit $u:I\to J$ une fonction définie et dérivable sur un intervalle $I$ et à
  valeurs dans un intervalle $J$ et soit $v$ une fonction définie et dérivable
  sur $J$. Alors la fonction
  $v\circ u$ est dérivable sur $I$ et $(v\circ u)'=u'\times(v'\circ u)$,
  c'est-à-dire :
  \[
    \forall x \in I,\, (v\circ u)'(x) = u'(x)\times (v'\circ u)(x).
  \]
\end{propadm}

\begin{app}
  Soit $f:x\mapsto \sqrt{x^3+1}$ une fonction définie sur $\mathbb{R}_+$.
  Déterminer $f'$.
\end{app}

\begin{propnom}{Conséquences de la propriété~\ref{prop:deriv-comp}}
  Soit $u$ une fonction définie et dérivable sur un intervalle $I$.
  \begin{enumerate}
    \item On a $\left( e^u \right)'=u'e^u$.
    \item Pour tout entier naturel non nul $n\in\mathbb{N}^*$, $\left( u^n
      \right)'=nu'u^{n-1}$.
    \item Si $u$ ne s'annule pas sur $I$, alors $\displaystyle\left( \frac{1}{u}
      \right)'=-\frac{u'}{u^2}$.
    \item Si $u$ est strictement positive sur $I$, alors $\displaystyle\left( \sqrt u
      \right)'=\frac{u'}{2\sqrt u}$.
  \end{enumerate}
\end{propnom}

\begin{app}
  Calculer les dérivées des fonctions composées suivantes.
  \begin{align*}
    \textbf{1.}\;& f:x\mapsto e^{x^3+5x^2+7x} &
    \textbf{2.}\;& g:x\mapsto (x^2+6x+8)^5 &
    \textbf{3.}\;& h:x\mapsto \frac{1}{x^3+2x^2-5x+1}
  \end{align*}
\end{app}

\section{Dérivée seconde}

\begin{defi}{Dérivée seconde}
  Soit $f$ une fonction dérivable sur un intervalle $I$. On note $f'$ sa
  fonction dérivée. Lorsque $f'$ est dérivable sur $I$, on note $f''$ sa
  dérivée. La fonction $f''$ est appelée la \textbf{dérivée seconde} de $f$ sur
  $I$.
\end{defi}

\begin{app}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par $\forall
  x\in\mathbb{R},\,f(x)=4x^3+2x^2+13x+9$. Déterminer $f''$.
\end{app}

\section{Convexité}
\subsection{Fonctions convexes}

\begin{defi}{Fonction convexe}~\\[-6mm]
  \begin{minipage}{.7\textwidth}
  Soit $f$ une fonction définie sur un intervalle $I$ et $\mathscr C_f$ sa
  courbe représentative dans un repère orthogonal. On dit que $f$ est
  \textbf{convexe} sur $I$ lorsque sa courbe représentative est située
  en-dessous de chacune de ses sécantes entre les deux points d'intersection.
  \end{minipage}
  \begin{minipage}{.3\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xmin=-3.5, xmax=3.5, ymin=-1.5]
          \addplot[thick, blue!50!black, samples=101]{x^2};
        \end{axis}
        \node[blue!50!black] at (1, 3) {$\mathscr C_f$};

        \coordinate (O) at (1.75,.75);
        \coordinate (a) at ($(O)+(-0.9,1.62)$);
        \coordinate (b) at ($(O)+(0.6,0.77)$);

        \coordinate (c) at ($(O)+(-0.4,.32)$);
        \coordinate (d) at ($(O)+(1,2)$);

        \begin{scope}[red, thick, dashed]
        \drawCrossP a;
        \drawCrossP b;
        \draw (a)--(b);
        \end{scope}

        \begin{scope}[green!50!black, thick, dashed]
        \drawCrossP c;
        \drawCrossP d;
        \draw (c)--(d);
        \end{scope}

      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{defi}

\begin{exemple}
  La fonction carré, dont le graphe est donné ci-dessus, est  une fonction convexe
  sur $\mathbb{R}$.
\end{exemple}

\begin{propadm}
  \label{prop:equiv}
  Soit $f$ une fonction définie et deux fois dérivable sur un intervalle $I$.
  Les quatre propositions suivantes sont équivalentes :
  \begin{enumerate}
    \item la fonction $f$ est convexe sur $I$ ;
    \item la courbe représentative de $f$ est entièrement située au-dessus de
      ses tangentes ;
    \item la fonction $f'$ est croissante sur $I$ ;
    \item la fonction $f''$ est positive sur $I$.
  \end{enumerate}
\end{propadm}

\begin{rmq}
  Une fonction $f$ dont la courbe est située au-dessus de chacune de ses
  sécantes est appelée \textbf{concave}. On a alors l'équivalence avec le fait
  que $f'$ soit décroissante ou avec le fait que $f''$ soit négative. On a aussi
  \[
    f\text{ convexe} \Longleftrightarrow -f\text { concave}
  \]
\end{rmq}

\subsection{Point d'inflexion}

\begin{defi}{Point d'inflexion}
  Un \textbf{point d'inflexion} est un point où la courbe représentative d'une
  fonction traverse sa tangente. Lorsque la courbe représentative d'une fonction
  admet un point d'inflexion, la fonction change de convexité : une fonction
  convexe devient concave ou inversement en ce point.
\end{defi}

\begin{exemple}~\\[-5mm]
  \begin{minipage}{.5\textwidth}
    La fonction cube $x\mapsto x^3$ admet un point d'inflexion en $0$. En ce
    point, la courbe représentative de la fonction traverse sa tangente.
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}

        \coordinate (O) at (1.8,1.8);
        \coordinate (a) at ($(O)+(-.9,0)$);
        \coordinate (b) at ($(O)+(0.9,0)$);

        \begin{axis}[simple graph, x=1cm, y=1cm, xmin=-1.8, xmax=1.8, ymin=-1.8,
          ymax=1.8]
          \addplot[thick, blue!50!black, samples=101]{x^3};
        \end{axis}

        \node[blue!50!black] at (2.5, 3.1) {$\mathscr C_f$};
          \draw[red, very thick, latex-latex] (a) -- (b);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{prop}
  Soit $f$ une fonction deux fois dérivable sur un intervalle $I$. La courbe
  représentative de la fonction $f$ admet un point d'inflexion au point
  d'abscisse $a$ si, et seulement si, $f''$ s'annule en changeant de signe en
  $a$.
\end{prop}
\begin{proof}
  On utilise la propriété~\ref{prop:equiv} : le fait que $f''$ s'annule en
  changeant de signe en $a$ signifie que l'on change de convexité en $a$, et
  donc que l'on a un point d'inflexion en $a$.
\end{proof}

\begin{app}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par $\forall x
  \in\mathbb{R},\,f(x)=x^3+3x^2+0,5x+1$.
  \begin{enumerate}
    \item Établir la convexité de la fonction $f$ (c'est-à-dire déterminer les
      intervalles sur lesquels $f$ est convexe et ceux sur lesquels elle est
      concave).
    \item Déterminer les abscisses des points d'inflexion de la courbe
      représentative de $f$.
  \end{enumerate}
\end{app}

\subsection{Inégalités de convexités}

\begin{prop}
  Soit $f$ une fonction deux fois dérivable sur un intervalle $I$ et convexe.
  Pour n'importe quels réels $x$ et $y$ dans $I$ et tout $t\in[0;1]$, on a alors
  \[
    f(tx+(1-t)y) \leq tf(x)+(1-t)f(y).
  \]
\end{prop}
\begin{proof}
  Il s'agit de la traduction de la propriété «~la courbe de $f$ est sous ses
  sécantes~».
\end{proof}
\vspace{-1mm}
\begin{rmq}
  L'inégalité est dans l'autre sens pour une fonction concave.
\end{rmq}

\end{document}
