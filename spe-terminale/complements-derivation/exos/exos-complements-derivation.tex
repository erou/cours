\documentclass[11pt, twocolumn]{article}

\newcommand{\titrechapitre}{Compléments sur la dérivation -- Exercices}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{0mm}

\setlength{\columnseprule}{1pt}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin officiel
% =================
%
% Capacités attendues
% -------------------
%
% [x] Calculer la dérivée d'une fonction donnée par une formule simple mettant
%     en jeu  opérations algébriques et composition.
% [x] Calculer la fonction dérivée, déterminer les limites et étudier les
%     variations d'une fonction construite simplement à partir des fonctions de
%     référence.
% [x] Démontrer des inégalités en utilisant la convexité d'une fonction.
% [x] Esquisser l'allure de la courbe représentative d'une fonction f à partir
%     de la donnée de tableaux de variations de f, de f', ou de f''.
% [x] Lire sur une représentation graphique de f, de f' ou de f'' les
%     intervalles où f est convexe, concave, et les points d'inflexion. Dans le
%     cadre de la résolution de problème, étudier et utiliser la convexité d'une
%     fonction.

\begin{document}

\begin{exobox}
  Soit deux fonctions $u$ et $v$ telles que
  \[
    \forall x\in\mathbb{R},\,v\circ u(x)=\frac{x^6}{x^6+1}.
  \]
  Déterminer $u$ et $v$.
\end{exobox}

\begin{exobox}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par
  \[
    \forall x\in\mathbb{R}\,f(x)=(x+2)^3.
  \]
  Trouver deux fonctions $u$ et $v$ telles que $f=v\circ u$.
\end{exobox}

\begin{exobox}
  Soit $f$ la fonction définie par l'expression
  \[
    f(x)=\sqrt{3x^2-5}.
  \]
  \begin{enumerate}
    \item Quelle est l'ensemble de définition de $f$ ?
    \item Déterminer $u$ et $v$ telles que $f=v\circ u$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  On considère deux fonctions $f$ et $g$ définies par
  \[
    \forall x\in\mathbb{R},\,f(x)=3x+1\text{ et }\forall
    x\in\mathbb{R}^*,\,g(x)=\frac{1}{x}.
  \]
  Déterminer $f\circ g(6)$.
\end{exobox}

\begin{exobox}
  Soit $f$ une fonction définie par l'expression donnée. Préciser son ensemble
  de définition et de dérivabilité et déterminer sa fonction dérivée $f'$.
  \begin{align*}
    \textbf{1.}\;& f(x) = \sqrt{x+\frac{1}{x}} &
    \textbf{2.}\;& f(x) = \sqrt{x^2-3x+5} \\
    \textbf{3.}\;& f(x) = e^{x^2-5x+4} &
    \textbf{4.}\;& f(x) = e^{x+\sqrt x} \\
    \textbf{5.}\;& f(x) = \left(\frac{1}{x}+\sqrt x\right)^6 &
    \textbf{6.}\;& f(x) = (x^3+3x)^5
  \end{align*}
\end{exobox}

\begin{exodur}
  Même consigne que l'exercice précédent, avec des fonctions plus sophistiquées.
  \begin{align*}
    \textbf{1.}\;& f(x) = \frac{(x^3+4)^5}{(x^2+3x+4)^3} &
    \textbf{2.}\;& f(x) = \frac{e^{2x^3+x^2-7x+2}}{\sqrt{x^2+x+1}}
  \end{align*}
\end{exodur}

\begin{exodur}
  Même consigne que l'exercice précédent.
  \begin{align*}
    \textbf{1.}\;& f(x) = \sqrt{-x^2+3x-2}\times(x^3+5x-8)^3 \\
    \textbf{2.}\;& f(x) = e^{-x^2+7}\times\sqrt{x^2+4}
  \end{align*}
\end{exodur}

\begin{exobox}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par
  \[
    f(x) = \left( x^2+\frac{3}{2}x-10 \right)^2.
  \]
  \begin{enumerate}
    \item Déterminer la fonction dérivée $f'$.
    \item Étudier le signe de $f'$.
    \item Dresser le tableau de variation de $f$ sur $\mathbb{R}$.
    \item En déduire les abscisses des points où les tangentes sont
      horizontales.
    \item Tracer la courbe représentative $\mathscr
      C_f$ de $f$ et les tangentes horizontales.
  \end{enumerate}
\end{exobox}

\begin{exodur}
  Soit $f$ la fonction définie sur $[-1;1[$ par
    \[
      f(x) = x\sqrt{\frac{1+x}{1-x}}.
    \]
  \begin{enumerate}
    \item Déterminer, pour tout réel $x\in]-1;1[$, la fonction dérivée $f'$ de
        $f$.
    \item Étudier le signe de $f'(x)$ sur $]-1;1[$.
    \item En déduire les variations de $f$ sur $]-1; 1[$.
    \item Donner une équation de la tangente à la courbe de $f$ au point
      d'abscisse $0$.
  \end{enumerate}
\end{exodur}

\begin{exobox}
  Soit $f$ et $g$ les fonctions définies par
  \[
    \forall x\in\mathbb{R},\,f(x)=x^2-7x+10
  \]
  et $g(x)=\sqrt{f(x)}$.
  \begin{enumerate}
    \item \begin{enumerate}
        \item Résoudre l'inéquation $f(x)\geq0$.
        \item En déduire l'ensemble de définition de $g$.
      \end{enumerate}
    \item \begin{enumerate}
        \item Déterminer les limites de $g$ aux bornes de son ensemble de
          définition.
        \item Calculer $g'(x)$ et en déduire les variations de la fonction $g$.
        \item Dresser le tableau de variations de la fonction $f$ puis celui de
          $g$.
      \end{enumerate}
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Le nombre de malades touchés par une maladie contagieuse est modélisé par la
  fonction $p$ définie pour tout réel positif $t\in\mathbb{R}_+$ par
  \[
    p(t) = 100(t^2+20t)e^{-t-1}
  \]
  où $t$ représente le nombre de semaines depuis le début de l'épidémie.
  \begin{enumerate}
    \item Déterminer la fonction dérivée $p'$ de $p$ sur $\mathbb{R}_+$.
    \item Dresser le tableau de variations de $p$ sur $\mathbb{R}_+$.
    \item Quel est le nombre maximal de malades ? Au bout de combien de temps ?
  \end{enumerate}
\end{exobox}

\begin{exobox}
  On a représenté ci-dessous les courbes représentatives de quatres fonctions
  $f$, $g$, $h$ et $t$.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[simple graph]
        \addplot[red, thick, samples=101, domain={-5.5:5.5}] {e^(0.3*x)};
        \addplot[blue!50!black, thick, samples=101, domain={0:5.5}] {sqrt(x)};
        \addplot[green!50!black, thick, samples=101, domain={0:5.5}] {ln(x)};
        \addplot[orange!90!black, thick, samples=101, domain={-5.5:5.5}]
        {0.2*x^2-4.5};
      \end{axis}

      \node[orange!90!black] at (1, 1) {$\mathscr C_f$};
      \node[blue!50!black] at (5.1, 4.1) {$\mathscr C_g$};
      \node[red] at (4.8, 5) {$\mathscr C_h$};
      \node[green!50!black] at (3.2, 2.1) {$\mathscr C_t$};
    \end{tikzpicture}
  \end{center}
  Déterminer graphiquement les fontions convexes.
\end{exobox}

\begin{exobox}
  Voici le tableau de variation de la fonction dérivée $f'$ d'une fonction $f$
  définie et dérivable sur $[-8;7]$.
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt = 1.5, espcl=1.8]%, deltacl=0.4]
      {$x$/1, $f'(x)$/2}{$-8$, $2$, $4$, $7$}
      \tkzTabVar{-/$2$, +/$5$, -/$0$, +/$1$}
    \end{tikzpicture}
  \end{center}
  \begin{enumerate}
    \item En déduire le sens de variation de $f$.
    \item Déterminer la convexité de $f$.
    \item Tracer l'allure d'une courbe pouvant représenter $f$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Voici le tableau de variation de la fonction dérivée seconde $f''$ d'une fonction $f$
  définie et deux fois dérivable sur $[-5;5]$.
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt = 1.5, espcl=1.4]%, deltacl=0.4]
      {$x$/1, $f''(x)$/2}{$-5$, $1$, $2$, $3$, $5$}
      \tkzTabVar{-/$-2$, R/, +/$5$, R/, -/$-1$}
      \tkzTabIma{1}{3}{2}{$0$}
      \tkzTabIma{3}{5}{4}{$0$}
    \end{tikzpicture}
  \end{center}
  \begin{enumerate}
    \item Déterminer la convexité de $f$ et les abscisses des éventuels points
      d'inflexion.
    \item Tracer l'allure d'une courbe pouvant représenter $f$.

  \end{enumerate}
\end{exobox}

\begin{exobox}
  On considère la fonction $f:x\mapsto e^x$.
  \begin{enumerate}
    \item Justifier que $f$ est convexe sur $\mathbb{R}$.
    \item Déterminer l'équation de la tangente à la courbe représentative de
      $f$ en $0$.
    \item En déduire que $\forall x\in\mathbb{R},\,e^x\geq x+1$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Soit $n\geq2$ un entier. On considère la fonction
  \[
    f:x\mapsto(1+x)^n.
  \]
  \begin{enumerate}
  \item Justifier que $f$ est convexe sur $[-1; +\infty[$.
    \item Déterminer l'équation de la tangente à la courbe représentative de
      $f$ en $0$.
    \item En déduire l'inégalité de Bernoulli\footnote{On avait aussi prouvé ce
      résultat par récurrence dans le premier chapitre de l'année.}:
      \[\forall x\in[-1;+\infty[,\,(1+x)^n\geq1+nx.
      \]
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Soit $q$ la fonction définie sur $\mathbb{R} \setminus \{1\}$ par  
\[
q(x) = \frac{e^x}{1 - x}.
\]

\begin{enumerate}
    \item Déterminer la fonction dérivée $q'$ et la fonction dérivée seconde $q''$  
    de $q$ pour tout réel $x \neq 1$.
    \item Étudier le signe de $q''(x)$ sur $\mathbb{R} \setminus \{1\}$.
    \item 
    \begin{enumerate}
        \item En déduire la convexité de $q$ et les abscisses des éventuels points  
        d'inflexion.
        \item Déterminer une équation de la tangente à la courbe représentative  
        de $q$ au point d'abscisse $0$.
        \item En déduire une inégalité avec $q(x)$.
    \end{enumerate}
\end{enumerate}
\end{exobox}


\begin{exobox}
  On donne la représentation graphique d'une fonction $f$. Déterminer les points
  d'inflexion de $f$.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[simple graph, xmin=-3.2, xmax=3.2, ymin=-1.5, ymax=3.2,x=1cm,
        y=1cm]
        \addplot[red, thick, samples=101, domain={-5.5:5.5}] {x^4/12-x^2/2};
      \end{axis}
      \node[red] at (5.8, 4) {$\mathscr C_f$};
    \end{tikzpicture}
  \end{center}
\end{exobox}

\begin{exobox}
  On donne la représentation graphique d'une fonction $f$. Déterminer les points
  d'inflexion de $f$.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[simple graph, xmin=-2.7, xmax=4.2, ymin=-5.2, ymax=1.5,x=1cm,
        y=1cm]
        \addplot[red, thick, samples=101, domain={-5.5:5.5}]
        {0.1*(x^2-8*x+17)*e^x-5};
      \end{axis}
      \node[red] at (5, 3.5) {$\mathscr C_f$};
    \end{tikzpicture}
  \end{center}
\end{exobox}

\begin{exobox}
  On considère la fonction $f$ définie et deux fois dérivable sur $\mathbb{R}$
  par
\( 
f(x) = 7x e^{-x}
\).
\begin{enumerate}
    \item Calculer $f'(x)$ et donner les variations de $f$.
    \item \begin{enumerate}
        \item Calculer $f''(x)$.
        \item Étudier le signe de $f''(x)$ et en déduire les coordonnées des 
        éventuels points d’inflexion de la courbe représentative de la 
        fonction $f$.
    \end{enumerate}
\end{enumerate}
\end{exobox}

\begin{exobox}
  On considère la fonction $f$ définie et deux fois dérivable sur $\mathbb{R}$
  par
\( 
f(x) = (x^2 + 7x + 8)e^{-x}
\).
\begin{enumerate}
    \item Calculer $f'(x)$ et donner les variations de $f$.
    \item \begin{enumerate}
        \item Calculer $f''(x)$.
        \item Étudier le signe de $f''(x)$ et en déduire les coordonnées des 
        éventuels points d’inflexion de la courbe représentative de la 
        fonction $f$.
    \end{enumerate}
\end{enumerate}
\end{exobox}

\begin{exobox}
La courbe $\mathscr{C}_f$ donnée ci-dessous représente une fonction $f$ définie
et dérivable sur $\mathbb{R}_+$. On admet que $\mathscr{C}_f$ passe par les
points $O(0 ; 0)$, $A(1 ; 1)$ et $B\left( 2 ; \frac{4}{e} \right)$, et que l’axe
des abscisses est asymptote horizontale à la courbe $\mathscr{C}_f$.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[simple graph, xmin=-0.5, xmax=6.5, ymin=-0.5, ymax=2.5,
        x=1cm, y=1cm, xtick={0.0, 1.0, ..., 6.0}]
        \addplot[red, thick, samples=101, domain={0:6.5}] {x^2*e^(1-x)};
      \end{axis}
      \node[red] at (4, 2) {$\mathscr C_f$};
    \end{tikzpicture}
  \end{center}

\begin{enumerate}
    \item Donner $\lim\limits_{x \to +\infty} f(x)$ et $f'(2)$.
    \item On admet que $f(x) = x^2 e^{-x + 1}$.
    \begin{enumerate}
        \item Calculer $f'(x)$. En déduire les variations de la fonction $f$.
        \item Dresser le tableau de variations de $f$.
        \item Déterminer l’équation de la tangente à $\mathscr{C}_f$ au point
        d’abscisse $4$.
    \end{enumerate}
    \item
    \begin{enumerate}
        \item Calculer $f''(x)$. En déduire l’étude de la convexité de $f$.
        \item Déterminer les éventuels points d’inflexion de $\mathscr{C}_f$.
    \end{enumerate}
    \item On considère la fonction $g$ définie par :
    \[
    \forall x>0,\, g(x) = e^{-f(x)}.
    \]
    Dresser le tableau de variations de $g$.
    \item On considère la fonction $h$ définie par :
    \[
      \forall x>0,\, h(x) = \sqrt{f(x)}.
    \]
    Dresser le tableau de variations de $h$.
\end{enumerate}
\vspace{-6mm}
\begin{flushright}
  \footnotesize
  D'après BAC ES Métropole-La Réunion Sept. 2006.
\end{flushright}
\end{exobox}

\begin{exobox}
  On considère la fonction $f$ définie et deux fois dérivable par $f(x) =
  \sqrt{x^3 + 8}$.
\begin{enumerate}
    \item Donner le domaine de définition de $f$.
    \item Déterminer les limites de $f$ aux bornes de son ensemble de définition.
    \item Calculer $f'(x)$ et donner les variations de $f$.
    \item \begin{enumerate}
        \item Calculer $f''(x)$.
        \item Étudier le signe de $f''(x)$ et en déduire les coordonnées des  
        éventuels points d’inflexion de la courbe représentative de la fonction $f$.
    \end{enumerate}
\end{enumerate}
\end{exobox}

\begin{exobox} \textbf{\small D'après Centres Étrangers 2014.}
  Soit $f$ la fonction définie par
\(
f(x) = x e^{x^2 - 1}
\)
sur $\mathbb{R}$, et $\mathscr{C}_f$ sa courbe représentative dans un repère
orthonormé.
\begin{enumerate}
    \item \begin{enumerate}
        \item Montrer que, pour tout réel $x$ :
        \[
        f'(x) = (2x^2 + 1)e^{x^2 - 1}.
        \]
        \item En déduire le sens de variation de $f$.
    \end{enumerate}
    \item \begin{enumerate}
        \item Montrer que, pour tout réel $x$ :
        \[
        f''(x) = 2x(2x^2 + 3)e^{x^2 - 1}.
        \]
        \item Déterminer l’intervalle sur lequel la fonction $f$ est convexe.
    \end{enumerate}
    \item Soit $h$ la fonction définie sur $\mathbb{R}$ par :
    \[
    h(x) = x - f(x).
    \]
    \begin{enumerate}
        \item Montrer que $h(x) = x^2(1 - e^{x^2 - 1})$.
        \item On admet que l’inéquation $1 - e^{x^2 - 1} \leq 0$ a pour ensemble de
        solutions l’intervalle $[-1 ; 1]$. Déterminer le signe de $h(x)$ sur $[-1 ; 1]$
        et en déduire la position relative de la courbe $\mathscr{C}_f$ et de la
        droite $D$ d’équation $y = x$ sur $[-1 ; 1]$.
    \end{enumerate}
\end{enumerate}
\end{exobox}

\begin{exobox}
 On considère la fonction $f$ définie par :  
\[
  f(x) = (-x + 1)e^{\frac{1}{-x+1}}.
\]
On appellera $\mathscr{C}_f$ sa courbe représentative dans le plan $\mathscr{P}$,  
rapporté à un repère orthonormé.
\begin{enumerate}
    \item Déterminer l’ensemble des valeurs de $x$ pour lesquelles $f$ est définie.
    \item Déterminer les limites de $f$ aux bornes de son ensemble de définition.
    \item Déterminer les variations de la fonction. Préciser les coordonnées des  
    extrema si la fonction en possède.
    \item Donner le tableau de variations de la fonction.
    \item Donner l’allure de la courbe $\mathscr{C}_f$.
\end{enumerate}
\vspace{-5mm}
\begin{flushright}
  \footnotesize D'après Concours audioprothésiste CPDA 2015.
\end{flushright}
\end{exobox}

\begin{exodur}
  Soit $f$ définie sur $\mathbb{R}_+$ par $f(x)=\sqrt x$.
  \begin{enumerate}
    \item Étudier la convexité de $f$.
    \item Établir que $\forall x\in\mathbb{R}_+,\,\sqrt x\leq
      \frac{1}{2}x+\frac{1}{2}.$
    \item En déduire que $\sqrt 2\leq 1,5$.
  \end{enumerate}
\end{exodur}


\end{document}
