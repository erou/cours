\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Orthogonalité et distances dans l'espace -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

\newcommand{\vu}{\vect{u}}
\newcommand{\vv}{\vect{v}}
\newcommand{\vw}{\vect{w}}
\newcommand{\vi}{\vect{i}}
\newcommand{\vj}{\vect{j}}
\newcommand{\vk}{\vect{k}}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Produit scalaire de deux vecteurs de l’espace. Bilinéarité, symétrie.
% [x] Orthogonalité de deux vecteurs. Caractérisation par le produit scalaire.
% [ ] Base orthonormée, repère orthonormé.
% [ ] Coordonnées d’un vecteur dans une base orthonormée. Expressions du produit
%     scalaire et de la norme. Expression de la distance entre deux points.
% [ ] Développement de ||u+v||^2 , formules de polarisation.
% [ ] Orthogonalité de deux droites, d’un plan et d’une droite.
% [ ] Vecteur normal à un plan. Étant donnés un point A et un vecteur non nul n,
%     plan passant par A et normal à n.
% [ ] Projeté orthogonal d’un point sur une droite, sur un plan.
%
% Capacités attendues
% -------------------
%
% [ ] Utiliser le produit scalaire pour démontrer une orthogonalité, pour
%     calculer un angle, une longueur dans l’espace.
% [ ] Utiliser la projection orthogonale pour déterminer la distance d’un point
%     à une droite ou à un plan.
% [ ] Résoudre des problèmes impliquant des grandeurs et mesures : longueur,
%     angle, aire, volume.
% [ ] Étudier des problèmes de configuration dans l’espace : orthogonalité de
%     deux droites, d’une droite et d’un plan ; lieux géométriques simples, par
%     exemple plan médiateur de deux points.
%
% Démonstrations
% --------------
%
% + Le projeté orthogonal d’un point M sur un plan 𝒫 est le point de 𝒫 le plus
%   proche de M.
%
% Exemples d'algorithme
% ---------------------
%
% **Pas d'algorithme dans ce chapitre.**
%
% TODO: des dessins, quand même ??


\title{\vspace{-20mm}Chapitre 7 : Orthogonalité et distances dans l'espace}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/orthogonalite.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Orthogonalité et produit scalaire}
\subsection{Orthogonalité}

\begin{defi}{Orthogonalité de deux droites}
  Deux droites sont dites \textbf{orthogonales} lorsque leurs parallèles
  respectives passant par un même point sont perpendiculaires.
\end{defi}

\begin{defi}{Orthogonalité de deux vecteurs non nuls}
  Deux vecteurs non nuls sont dits \textbf{orthogonaux} lorsque les droites
  dirigées par ces vecteurs sont orthogonales.
\end{defi}

\begin{defi}{Orthogonalité d'une droite et d'un plan}
  Une droite est \textbf{orthogonale} à un plan lorsqu'elle est orthogonale à
  toutes les droites de ce plan.
\end{defi}

\begin{exemple}
%  \begin{minipage}{.5\textwidth}
    Dans un cube $ABCDEFGH$, les droites $\left( AB \right)$ et $\left( GF
    \right)$ sont orthogonales mais pas perpendiculaires : ces droites ne sont
    pas coplanaires.
%  \end{minipage}
%  \begin{minipage}{.5\textwidth}
%
%\begin{center}
%  \begin{tikzpicture}[3d]
%    \coordinate (A) at (3,0,0);
%    \coordinate (B) at (3,3,0);
%    \coordinate (C) at (3,3,3);
%    \coordinate (D) at (3,0,3);
%    \coordinate (E) at (0,0,0);
%    \coordinate (F) at (0,3,0);
%    \coordinate (G) at (0,3,3);
%    \coordinate (H) at (0,0,3);
%
%    \draw (B) -- (F) node[right] {$F$} -- (G) node[right] {$G$} -- (H)
%    node[left] {$H$} -- (D);
%    \draw (A) node[left] {$A$} -- (B) node[right] {$B$} -- (C)
%    node[right] {$C$} -- (D) node[left] {$D$} -- (A);
%    \draw (C) -- (G);
%    \draw[dashed] (A) -- (E) node[left] {$E$} -- (H);
%    \draw[dashed] (E) -- (F);
%
%    \draw[blue!50!black, thick, opacity=.5] ($(A)!-.5!(B)$) -- ($(A)!1.5!(B)$);
%    \draw[green!50!black, thick, opacity=.5] ($(G)!-.5!(F)$) -- ($(G)!1.5!(F)$);
%  \end{tikzpicture}
%\end{center}
%\end{minipage}
\end{exemple}

\begin{prop}
  Deux droites sont orthogonales si, et seulement si, leurs vecteurs directeurs
  respectifs sont orthogonaux.
\end{prop}

\begin{prop}
  Une droite est orthogonale à un plan si, et seulement si, un vecteur directeur
  de la droite est orthogonal à une base de ce plan, c'est-à-dire à deux
  vecteurs non colinéaires de ce plan.
\end{prop}

\begin{exemple}
  Dans le plan $ABCDEFGH$, la droite $(CG)$ est orthogonale au plan $(ABD)$.
\end{exemple}

\begin{defi}{Vecteur normal à un plan}
  On considère une droite orthogonale à un plan. Tout vecteur directeur de cette
  droite est appelé \textbf{vecteur normal} au plan.
\end{defi}

\begin{prop}
  Une droite est orthogonale à un plan si elle est orthogonale à deux droites
  sécantes de ce plan.
\end{prop}

\subsection{Le produit scalaire dans l'espace}

\begin{defi}{Produit scalaire}
  Soient $\vu$ et $\vv$ deux vecteurs de l'espace. Lorsqu'ils ne sont pas nuls,
  on définit le \textbf{produit scalaire} par
  \[
    \vu\cdot\vv=||v||\times||u||\times\cos(\vu;\vv).
  \]
  Lorsque l'un des vecteurs est nul, alors $\vu\cdot\vv=0$.
\end{defi}

\begin{exemple}
  Dans un tétraèdre régulier $ABCD$ de côté $4$ cm. On a 
  \[
  \vect{AB}\cdot\vect{AC} =AB\times AC\times\cos(\vect{AB};\vect{AC})
  = 4\times4\times\cos\left( \frac{\pi}{3} \right) = 8
\]
\end{exemple}

\begin{propnom}{Expression du produit scalaire avec un projeté orthogonal}
  Soit $\vu$ et $\vv$ deux vecteurs non nuls. On pose trois points $O$, $A$ et
  $B$ tels que $\vu=\vect{OA}$ et $\vv=\vect{OB}$. On appelle $H$ le point de
  $\left( OA \right)$ tel que $\left( BH \right)\perp\left( OA \right)$. Alors
  \[
    \vu\cdot\vv=
    \begin{cases}
      \phantom{-}OA\times OH &\text{ si } H\in[OA) \\
      -OA\times OH &\text{ sinon }
    \end{cases}
  \]
\end{propnom}

\begin{propnom}{Symétrie et linéarité}
  Soient $\vu$, $\vv$ et $\vw$ trois vecteurs et $\lambda\in\mathbb{R}$ un réel
  quelconque. Le produit scalaire est
  \begin{itemize}
    \item \textbf{Symétrique} : $\vu\cdot\vv = \vv\cdot\vu$ ;
    \item \textbf{Linéaire à gauche} : $\left( \vu+\lambda\vv
      \right)\cdot\vw=\vu\cdot\vw+\lambda\times\vv\cdot\vw$
    \item \textbf{Linéaire à droite} : $\vu\cdot\left(\lambda\vv
      +\vw\right)=\lambda\times\vu\cdot\vv+\vu\cdot\vw$
  \end{itemize}
\end{propnom}

\begin{propnom}{Identités de polarisation}
  Soient $\vu$ et $\vv$ deux vecteurs. On a alors :
  \[
    \vu\cdot\vv=\frac{1}{2}\left( ||\vu||^2+||\vv||^2-||\vu-\vv||^2 \right)
    \quad et \quad
    \vu\cdot\vv=\frac{1}{2}\left( ||\vu+\vv||^2-||\vu||^2-||\vv||^2 \right)
  \]
\end{propnom}

\begin{app}
  On considère deux vecteurs $\vu$ et $\vv$ tels que $||\vu||=3$ et $||\vv||=5$.
  De plus, on donne $||\vu-\vv||=\sqrt{22}$. Quelle est la mesure principale de
  l'angle $\left( \vu, \vv \right)$ ? Arrondir le résultat au degré près.
\end{app}

\subsection{Lien entre orthogonalité et produit scalaire}

\begin{prop}
  Deux vecteurs $\vu$ et $\vv$ sont orthogonaux si, et seulement si, leur
  produit scalaire est nul.
\end{prop}

%\begin{rmq}
%  C'est \textbf{LE} résultat à retenir ! On transforme une propriété géométrique
%  en une propriété numérique.
%\end{rmq}

\begin{app}
  Soit un cube $ABCDEFGH$. Montrer que les droites $(BG)$ et $(EC)$ sont
  orthogonales.
\end{app}

\section{Géométrie analytique}
\subsection{Distance et produit scalaire}

\begin{defi}{Base orthonormée}
  Une \textbf{base orthonormée} de l'espace est la donnée de trois vecteurs
  formant une base et tels que $||\vi||=||\vj||=||\vk||=1$ et
  $\vi\cdot\vj=\vj\cdot\vk=\vi\cdot\vk=0$.
\end{defi}
\begin{intuition}
  Une base orthonormée est une base où les vecteurs ont la même norme et sont
  orthogonaux.
\end{intuition}
\begin{defi}{Repère orthonormé}
  Un \textbf{repère orthonormé} est un repère dont la base est orthonormée.
\end{defi}
\begin{rmq}
  La plupart du temps, on fait de la géométrie analytique dans un repère
  orthonormé.%, car cela facilite les calculs de distance et de produit scalaire.
\end{rmq}

\noindent Dans toute la suite, on considère que l'espace est
  muni d'un \underline{\textbf{repère orthonormé}}.

\begin{propnom}{Formules de norme et distance}
  Pour tout vecteur
  $\vu
  \begin{pmatrix}
    x \\ y \\ z
  \end{pmatrix}$ et pour tous points $A(x_A; y_A; z_A)$ et $B(x_B; y_B; z_B)$,
  on a :
  \begin{align*}
    ||\vu|| &= \sqrt{x^2+y^2+z^2} \\
    AB &= \sqrt{(x_B-x_A)^2+(y_B-y_A)^2+(z_B-z_A)^2}.
  \end{align*}
\end{propnom}

\begin{propnom}{Expression du produit scalaire dans un repère orthonormé}
  Pour tous vecteurs
  $\vu
  \begin{pmatrix}
    x \\ y \\ z
  \end{pmatrix}$ et 
  $\vv
  \begin{pmatrix}
    x' \\ y' \\ z'
  \end{pmatrix}$, on a
  \[
    \vu\cdot\vv = xx'+yy'+zz'.
  \]
\end{propnom}

\subsection{Équation cartésienne d'un plan}

\begin{propnom}{Équation cartésienne d'un plan}
  On considère un vecteur $\vect{n}
  \begin{pmatrix}
    a \\ b \\ c
  \end{pmatrix}$
  et un point $A(x_A; y_A; z_A)$. Le plan $\mathcal P$ qui passe par le point
  $A$ et de vecteur normal $\vect n$ admet pour équation cartésienne
  $ax+by+cz+d=0$ où $d=-(ax_A+by_A+cz_A)$.
\end{propnom}
\begin{app}
  \begin{enumerate}
    \item Donner l'équation du plan de vecteur normal $\vect n
      \begin{pmatrix}
        2 \\ 1\\ 2
      \end{pmatrix}$
      et passant par le point $A(1; 0; 1)$.
    \item Donner un vecteur normal au plan d'équation $-3x+2y-z+5=0$.
    \item Donner trois points appartenant au plan d'équation $x+y+z+1=0$.
  \end{enumerate}
\end{app}

\section{Projection orthogonale}
\subsection{Définitions}

\begin{defi}{Projeté orthogonal d'un point sur un plan}
  On considère un plan $\mathcal P$ de vecteur normal $\vect n$ et
  un point $M$ extérieur à $\mathcal P$. Le \textbf{projeté orthogonal} de
  $M$ sur $\mathcal P$ est l'intersection du plan et de la droite de vecteur
  directeur $\vect n$ passant par $M$.
\end{defi}

\begin{defi}{Projeté orthogonal d'un point sur une droite}
  On considère une droite $d$ de vecteur directeur $\vu$ et un point $M$
  estérieur à cette droite. Le \textbf{projeté orthogonal} de $M$ sur $d$ est
  l'intersection du plan normal à $\vu$ passant par $M$ avec la droite $d$.
\end{defi}

\begin{app}
  On considère le plan $\mathcal P$ d'équation $3x+y-z-2=0$. Déterminer les
  coordonnées du projeté orthogonal du point $A(5; 1; 3)$ sur le plan $\mathcal
  P$.
\end{app}

\subsection{Distances}

\begin{defi}{Distance d'un point à un plan ou une droite}
  Soit $A$ un point et $\mathcal P$ un plan (ou $\mathcal D$ une droite). La
  \textbf{distance} du point $A$ au plan $\mathcal P$ (ou à la droite $\mathcal
  D$) est la plus petite des longueurs $AM$ où $M\in\mathcal P$ (ou
  $M\in\mathcal D$).
\end{defi}

\begin{propnom}{Distance et projeté orthogonal}
  Soit $A$ un point et $\mathcal P$ un plan (ou $\mathcal D$ une droite).
  Si on note $H$ le projeté orthogonal de $A$ sur la plan $\mathcal P$ (ou la
  droite $\mathcal D$), alors la distance entre $A$ et $\mathcal P$ (ou
  $\mathcal D)$ est $AH$.
\end{propnom}

\begin{intuition}
  La projection orthogonal est la plus petite distance entre un point et un
  autre objet.
\end{intuition}

\begin{prop}
  Soit $\mathcal P$ le plan d'équation $ax+by+cz+d=0$ et $A(x_A; y_A; z_A)$ un
  point. Si on note $\vect n$ un vecteur normal à $\mathcal P$ et $M(x; y; z)$
  un point de $\mathcal P$, alors la distance $d$ entre $A$ et $\mathcal P$ est
  \[
    d = \frac{\left|\vect{AM}\cdot\vect n\right|}{||\vect
    n||}=\frac{|ax_A+by_A+cz_A+d|}{\sqrt{a^2+b^2+c^2}}.
  \]
\end{prop}

\begin{app}
  Calculer la distance entre $A(-1; 3; 2)$ et $\mathcal P:x-3y+2z-4=0$.
\end{app}

\begin{app}
  Calculer la distance entre le point $A(2; -1; 2)$ et la droite $\mathcal D$
  dont on donne une représentation paramétrique : $
  \begin{cases}
    x = 2t+1\\
    y = -t\\
    z = t-1
  \end{cases}$, avec $t\in\mathbb{R}$.
\end{app}


\end{document}
