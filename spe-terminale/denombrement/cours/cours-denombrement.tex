\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Dénombrement -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\renewcommand{\emptyset}{\varnothing}
\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Principe additif : nombre d’éléments d’une réunion d’ensembles deux à deux
%     disjoints.
% [x] Principe multiplicatif : nombre d’éléments d’un produit cartésien. Nombre
%     de k-uplets (ou k-listes) d’un ensemble à n éléments.
% [ ] Nombre des parties d’un ensemble à n éléments. Lien avec les n-uplets de
%     {0,1}, les mots de longueur n sur un alphabet à deux éléments, les chemin
%     dans un arbre, les issues dans une succession de n épreuves de Bernoulli.
% [x] Nombre des k-uplets d’éléments distincts d’un ensemble à n éléments.
%     Définition de n! Nombre de permutations d’un ensemble fini à n éléments.
% [ ] Combinaisons de k éléments d’un ensemble à n éléments : parties à k
%     éléments de l’ensemble. Représentation en termes de mots ou de chemins.
% [ ] Pour 0 <= k <= n, formule du coefficient binomial k parmi n :
%     n!/((n-k)!k!)
% [ ] Explicitation pour k = 0, 1, 2. Symétrie. Relation et triangle de Pascal.
%
% Capacités attendues
% -------------------
%
% [ ] Dans le cadre d’un problème de dénombrement, utiliser une représentation
%     adaptée (ensembles, arbres, tableaux, diagrammes) et reconnaître les
%     objets à dénombrer.
% [ ] Effectuer des dénombrements simples dans des situations issues de divers
%     domaines scientifiques (informatique, génétique, théorie des jeux,
%     probabilités, etc.).
%
% Démonstrations
% --------------
%
% + Démonstration par dénombrement de la relation \sum_{k=0}^{n}\binom{n}{k}=2^n
% + Démonstrations de la relation de Pascal (par le calcul, par une méthode
%   combinatoire).
%
% Exemples d'algorithme
% ---------------------
%
% + génération de la liste des coefficients binomiaux à l'aide de la relation de
%   Pascal
% + génération des permutations d'un ensemble fini, ou tirage aléatoire d'une
%   permutation
% + génération des parties à 2; 3 élements d'un ensemble fini
%

\title{\vspace{-20mm}Chapitre 8 : Dénombrement}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/denombrement.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Définitions}
\subsection{Ensembles}

\begin{defi}{Ensemble}
  Un \textbf{ensemble} est une collection d'objets distincts $x$ qu'on appelle
  les éléments. On dit que $x$ appartient à $A$ (respectivement $x$ n'appartient
  pas à $A$) et on note $x\in A$ (respectivement $x\notin A$).
\end{defi}
\begin{exemple}
  L'ensemble $A=\left\{ 1; 2; 3 \right\}$ est composé des trois éléments $1$, $2$ et
  $3$. On a par contre $5\notin A$.
\end{exemple}
\begin{exemple}
  Un ensemble peut avoir une infinité d'éléments, c'est le cas des ensembles
  classiques $\mathbb{N}, \mathbb{Z}, \mathbb{Q}, \mathbb{R}$.
\end{exemple}

\begin{defi}{Cardinal d'un ensemble}
  Soit $A$ un ensemble fini, c'est-à-dire composé d'un nombre fini d'éléments.
  Le nombre d'éléments de $A$ est appelé \textbf{cardinal} de $A$ et est noté
  $\Card(A)$.
\end{defi}
\begin{exemple}
  Soit $A=\left\{-1; \sqrt 2; \pi\right\}$. On a $\Card\left( A \right)=3$.
\end{exemple}
\begin{app}
  On considère l'ensemble $A$ composé de tous les nombres entiers relatifs de
  valeur absolue inférieure à $2,5$. Donner $\Card\left( A \right)$.
\end{app}

\begin{rmq}
  On note aussi parfois $|A|$ ou $\#A$ le cardinal de $A$.
\end{rmq}

\begin{defi}{Ensemble vide}
  On note $\emptyset$ l'ensemble composé de zéro éléments, qu'on appelle
  \textbf{ensemble vide}. On a $\Card\left( \emptyset \right)=0$.
\end{defi}

\begin{defi}{Union et intersection}
  Soient deux ensembles $A$ et $B$. L'\textbf{union} de $A$ et $B$, notée $A\cup
  B$, est l'ensemble des éléments qui sont dans $A$ ou dans $B$ (éventuellement
  dans les deux). L'\textbf{intersection} de $A$ et $B$, notée $A\cap B$ est
  l'ensemble des élements qui sont à la fois dans $A$ et dans $B$.
\end{defi}

\begin{exemple}
  Soient $A=\left\{ 1; 2; 3 \right\}$ et $B=\left\{ 3; 4; 5 \right\}$. On a
  alors $A\cap B=\left\{ 3 \right\}$ et $A\cup B=\left\{ 1; 2; 3; 4; 5
  \right\}$.
\end{exemple}

\subsection{Réunion disjointes}

\begin{defi}{Ensembles disjoints}
  Deux ensembles $A$ et $B$ sont \textbf{disjoints} lorsque $A\cap B=\emptyset$.
\end{defi}

\begin{exemple}
  Soient $A=\left\{ \frac{1}{3}; \sqrt 7 \right\}$ et $B=\left\{ 1; 2; 3; 4; 5
  \right\}$. Les ensembles $A$ et $B$ sont disjoints.
\end{exemple}

\begin{propadm}
  Soit $n\in\mathbb{N}$ avec $n\geq2$ et $A_1, \ldots, A_n$ des ensembles deux à
  deux disjoints. On a alors
  \[
    \Card\left( A_1\cup\ldots\cup A_n \right)=\Card\left( A_1
    \right)+\cdots+\Card\left( A_n \right)=\sum_{k=1}^n\Card\left( A_k
    \right).
  \]
\end{propadm}
\begin{intuition}
  Le nombre d'éléments d'une union d'ensembles qui n'ont aucun éléments en
  commun est la somme du nombre d'éléments de chacun des ensembles.
\end{intuition}
\begin{app}
  Alice tente de récupérer le mot de passe de Bob, elle possède une liste de
  $53$ mots de passe qui commencent par une lettre et une deuxième liste de $21$
  mots de passe qui commencent par un chiffre et sait que le bon mot de passe
  est dans une des deux listes.
  \begin{enumerate}
    \item Combien de mot de passe doit-elle tester dans le pire des cas ?
  \end{enumerate}
  Alice retrouve finalement une troisième liste, contenant $17$
  mots de passe, qui contiennent tous $10$ caractères alphanumériques (chiffres
  ou lettres). Le bon mot de passe est dans l'une des trois listes.
  \begin{enumerate}
      \setcounter{enumi}{1}
    \item Peut-on savoir combien de mots de passe Alice devra tester ?
  \end{enumerate}
\end{app}

\subsection{Produit cartésien}

\begin{defi}{Produit cartésien}
  Soient $A$ et $B$ deux ensembles non vides. Le \textbf{produit cartésien} de
  $A$ et $B$ est l'ensemble, noté $A\times B$, constitué des couples $(x; y)$ où
  $x$ est un élément de $A$ et $y$ est un élément de $B$. On note
  \[
    A\times B = \left\{ (x; y),\;x\in A,\;y\in B \right\}.
  \]
\end{defi}
\begin{exemple}
  Soit $A=\left\{ 1; 2 \right\}$ et $B=\left\{ 3; 4 \right\}$. On a alors
  \[
    A\times B = \left\{ (1; 3); (2; 3); (1; 4); (2; 4) \right\}.
  \]
\end{exemple}
\begin{app}
  Soit $A=\left\{ -1; 0; 1 \right\}$ et $B=\left\{ \sqrt 2; \sqrt 3 \right\}$.
  Décrire les éléments de $A\times B$.
\end{app}
\begin{rmq}
  Si $A=\emptyset$ ou $B=\emptyset$, alors $A\times B=\emptyset$.
\end{rmq}

\begin{propadm}
  Soient $A$ et $B$ deux ensembles finis et non vides. Alors $\Card\left(
  A\times B \right) = \Card\left( A \right)\times\Card\left( B \right)$.
\end{propadm}
\begin{app}
  Bob contre-attaque et souhaite retrouver le mot de passe d'Alice. Il sait que
  le mot de passe d'Alice est composé d'un nombre à $4$ chiffres suivi d'une
  lettre minuscule parmi les 26 lettres de l'alphabet latin. Combien de mots de
  passe différents doit-il tester (au maximum) ?
\end{app}

\begin{notation}
  Soit $A$ un ensemble et $n\in\mathbb{N}$ avec $n\geq2$. Le produit cartésien
  de $A$ avec lui-même est noté $A^2$. De manière générale $A^n$ désigne le
  produit cartésien de $A$ avec lui-même $n$ fois.
\end{notation}

\begin{defi}{$n$-uplet}
  Soit $A$ un ensemble et $n\in\mathbb{N}^*$. On appelle \textbf{$n$-uplet} de $A$
  un élément de $A^n$.
\end{defi}

\begin{exemple}
  Soit $A=\left\{ 1; 2; 4 \right\}$. Alors $(4; 1; 4)$, $(2; 2; 2)$, $(2; 4;
  1)$, $(4; 4; 2)$ sont des éléments de $A^3$.
\end{exemple}

\begin{exemple}
  Les coordonnées des points dans l'espace sont des éléments de $\mathbb{R}^3$.
\end{exemple}

\begin{prop}
  Soit $A$ un ensemble fini et $n\in\mathbb{N}^*$ un entier. On a $\Card\left( A^n
  \right)=\left[ \Card(A) \right]^n$.
\end{prop}

\begin{app}
  Le digicode d'un immeuble est formé de $4$, $5$ ou $6$ chiffres allant de $1$
  à $9$ puis d'une lettre parmi A, B ou C. Combien de codes sont possibles avec
  ce système ?
\end{app}

\section{Arrangements et permutations}
\subsection{Arrangements}

\begin{defi}{Factorielle}
  Soit $n\in\mathbb{N}^*$ un entier non nul. On appelle \textbf{factorielle} de
  $n$ et on note $n!$ le nombre 
  \[
    n! = 1\times2\times\ldots\times(n-1)\times n.
  \]
\end{defi}
\begin{exemple}
  On a $3!=1\times2\times3=6$. On a $4!=24$ et $5!=120$.
\end{exemple}
\begin{rmq}
  Par convention, on a $0!=1$.
\end{rmq}

\begin{app}
  Exprimer $10!$ secondes en une durée plus facilement compréhensible (heures,
  jour, semaine, \emph{etc.}).
\end{app}

\begin{defi}{Arrangement}
  Soit $A$ un ensemble fini non vide à $n$ éléments et $k\leq n$ un entier. Un
  \textbf{arrangement} de $k$ éléments de $A$, ou $k$-arrangement est un
  $k$-uplet d'éléments distincts de $A$.
\end{defi}
\begin{exemple}
  Soit $A=\left\{ 1; 2; 3; 4 \right\}$. Les $3$-uplets $(1; 4; 2)$, $(3; 2; 1)$
  ou $(2; 4; 3)$ sont des $3$-arrangements de $A$. Mais $(3; 2; 3)$ n'est pas un
  $3$-arrangement.
\end{exemple}
\begin{intuition}
  Les $k$-arrangements de $A$ peuvent modéliser un tirage sans remise de $k$
  éléments dans $A$.
\end{intuition}

\begin{prop}
  \label{prop:nb-arrangements}
  Soit $A$ un ensemble fini non vide à $n$ élements et $k\leq n$. Le
  nombre de $k$-arrangements de $A$ est
  \[
    \arr{n}{k} = n\times(n-1)\times\cdots\times(n-k+1) = \frac{n!}{(n-k)!}.
  \]
\end{prop}
\begin{proof}
  On a $n$ choix pour le premier élément, puis $n-1$ choix pour le deuxième,
  ..., et ainsi de suite jusqu'à avoir $n-(k-1)=n-k+1$ choix pour le $k$-ième
  élement du $k$-arrangement.
\end{proof}

\begin{app}
  Combien y a-t-il de tirages possibles de $4$ boules, sans remise, dans une
  urne contenant $10$ boules numérotées de $1$ à $10$ ?
\end{app}

\begin{app}
  Combien y a-t-il de mots de passe contenant $6$ caractères alphanumériques
  dont aucun ne se répète ?
\end{app}

\subsection{Permutations}

\begin{defi}{Permutation}
  Soit $A$ un ensemble fini non vide à $n$ éléments. Une \textbf{permutation} de
  $A$ est un $n$-uplet d'éléments distincts de $A$.
\end{defi}
\begin{rmq}
  Autrement dit, une permutation est un $k$-arrangement de $A$ avec
  $k=\Card(A)=n$.
\end{rmq}
\begin{app}
  Donner toutes les permutations de $A=\left\{ 1; 2; 3 \right\}$.
\end{app}

\begin{prop}
  Le nombre de permutations d'un ensemble fini non vide à $n$ éléments est $n!$.
\end{prop}
\begin{proof}
  C'est le résultat de la propriété~\ref{prop:nb-arrangements} avec $k=n$.
\end{proof}
\begin{app}
  Dans une classe de Terminale, cinq élèves n'ont pas encore été évaluées à
  l'oral. Dans combien d'ordres différents le professeur peut-il les interroger,
  chaque élève n'étant interrogée qu'une et une seule fois ? 
  Combien y a-t-il de possibilités s'il n'a le temps d'interroger que trois
  d'entre elles ?
\end{app}

\section{Combinaisons}
\subsection{Parties d'un ensemble fini}

\begin{defi}{Partie d'un ensemble}
  Une \textbf{partie} d'un ensemble $A$ est un ensemble $B$ tel que tous les
  éléments de $B$ sont aussi des éléments de $A$. On note alors $B\subset A$.
\end{defi}
\begin{exemple}
  Si $A=\left\{ 1; 2; 3 \right\}$ alors $\{1; 3\}$ et $\emptyset$ sont des parties
  de $A$.
\end{exemple}
\begin{notation}
  On note $\P(A)$ l'ensemble des parties de $A$. L'ensemble $\P(A)$ est un
  ensemble d'ensemble.
\end{notation}
\begin{exemple}
  Soit $A=\left\{ 1; 2 \right\}$. On a $\P(A) = \left\{ \emptyset; \left\{ 1
  \right\}; \left\{ 2 \right\}; \left\{ 1; 2 \right\}\right\}$.
\end{exemple}
\begin{app}
  Soit $A=\{1; 2; 3\}$. Donner $\P(A)$.
\end{app}

\begin{prop}
  Soit $A$ un ensemble fini à $n$ éléments. Le nombre de parties de $A$ est égal
  à $2^n$.
\end{prop}
\begin{proof}
  Pour former un sous-ensemble de $A$, il faut décider, pour chaque élément de
  $A$, si l'on veut qu'il fasse ou non partie du sous-ensemble. Cela fait $2$
  choix, que l'on doit répéter $n$ fois, d'où $2^n$ possibilités. On peut
  représenter chaque sous-ensemble par un $n$-uplet de $\left\{ 0; 1 \right\}$.
\end{proof}

\subsection{Nombre de combinaisons}

\begin{defi}{Combinaison}
  Soit $A$ un ensemble fini à $n$ élements et $k\leq n$ un entier naturel
  inférieur ou égal à $n$. Une \textbf{combinaison} de $k$ éléments de $A$ est
  une partie de $A$ de cardinal $k$, c'est-à-dire composée de $k$ élements.
\end{defi}
\begin{exemple}
  Soit $A=\{1; 2; 3; 4; 5\}$. L'ensemble $\left\{ 3; 4 \right\}$ est une
  combinaison à $2$ éléments de $A$. Il s'agit de la même combinaison que
  $\left\{ 4; 3 \right\}$ (l'ordre n'a pas d'importance). Une autre combinaison
  est $\left\{ 2; 5 \right\}$.
\end{exemple}

\begin{notation}
  Le nombre de combinaison de $k$ éléments parmi $n$ éléments est noté
  $\binom{n}{k}$. On lit alors « $k$ parmi $n$ » et ces nombres s'appellent des
  \textbf{coefficients binomiaux}.
\end{notation}

\begin{prop}
  \label{prop:formule-binom}
  Soient $n$ et $k$ deux entiers naturels tels que $k\leq n$. Alors
  \[
    \binom{n}{k} = \frac{n!}{k!(n-k)!}.
  \]
\end{prop}
\begin{proof}
  Le nombre de combinaison ressemble au nombre d'arrangements. La différence
  étant que dans une combinaison, l'ordre des $k$ éléments tirés n'a pas
  d'importance, alors que c'est le cas pour les arrangements. On peut
  permuter l'ordre d'un arrangement et cela donnera toujours la même
  combinaison. Ainsi, il y a $k!$ $k$-arrangements qui donnent la même
  combinaison, et ainsi 
  \[
    \binom{n}{k}=\frac{\arr{n}{k}}{k!}=\frac{n!}{(n-k)!k!}.
  \]
\end{proof}
\begin{app}
  À la belote, on joue avec un jeu de $32$ cartes et une main de $8$ cartes.
  Combien y a-t-il de mains différentes ?
\end{app}

\begin{prop}
  Soient $n$ et $k$ deux entiers naturels avec $k\leq n$. On a les propriétés
  suivantes.
  \begin{enumerate}
    \item On a une symétrie : $\binom{n}{k}=\binom{n}{n-k}$.
    \item \textbf{Relation de Pascal} : si $1\leq k\leq n-1$,
      $\binom{n}{k}=\binom{n-1}{k-1}+\binom{n-1}{k}$.
    \item Valeurs particulières : $\binom{n}{0}=1$, $\binom{n}{1}=n$ et
      $\binom{n}{2}=\frac{n(n-1)}{2}$.
  \end{enumerate}
\end{prop}
\begin{proof}
  Ces trois propriétés peuvent se prouver en utilisant la formule de la
  propriété~\ref{prop:formule-binom}.
\end{proof}
\begin{app}
  Au poker, on joue avec un jeu de $52$ cartes et la main de départ est composée
  de $2$ cartes. Combien y a-t-il de mains de départ possibles ?
\end{app}

\begin{prop}
  Soit $n\in\mathbb{N}$ un entier naturel. Alors on a
  \[
    \sum_{k=0}^n\binom{n}{k}=2^n.
  \]
\end{prop}
\begin{proof}
  Soit $A$ un ensemble à $n$ éléments. On peut séparer $\P(A)$, l'ensemble des
  parties de $A$, en une réunion disjointe d'ensembles $A_k$ contenant chacun
  les parties de $A$ contenant $k$ éléments. On a alors
  \[
    2^n=\Card\left( \P(A) \right)=\Card\left( A_0\cup A_1\cup\cdots\cup A_n
    \right)=\sum_{k=0}^n\Card\left( A_k \right)=\sum_{k=0}^n\binom{n}{k}.
  \]
\end{proof}
\begin{app}
  Dans une grille comportant les nombres de $0$ à $9$ et les lettres de A à F,
  il faut colorier trois nombres et deux lettres. Combien de coloriages
  différents peut-on obtenir ?
\end{app}

\end{document}
