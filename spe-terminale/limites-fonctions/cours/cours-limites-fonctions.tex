\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Limites de fonctions -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{1.72cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}
\renewcommand{\P}{\mathcal P}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Limite finie ou infinie d'une fonction en 
%     [x] + ∞ ;
%     [x] – ∞ ;
%     [x] un point ;
%     [x] limites à gauche et à droite.
% [x] Asymptote parallèle à un axe de coordonnées.
%     [x] abscisses
%     [x] ordonnées
% [x] Limites faisant intervenir les fonctions de référence étudiées en classe
%     de première :
%     [x] puissances entières ;
%     [x] racine carrée ;
%     [x] fonction exponentielle.
% [x] Limites et comparaison.
% [x] Opérations sur les limites (admises).
%
% **La composition de limites se fait en contexte.** 
%
% Capacités attendues
% -------------------
%
% + Déterminer dans des cas simples la limite d’une suite ou d’une fonction en
%   un point, en ± ∞, en utilisant les limites usuelles, les croissances
%   comparées, les opérations sur les limites, des majorations, minorations ou
%   encadrements, la factorisation du terme prépondérant dans une somme.
% + Faire le lien entre l’existence d’une asymptote parallèle à un axe et celle
%   de la limite correspondante.
%
% Démonstrations
% --------------
%
% + Croissance comparée de x ↦ x^n et exp en + ∞.
%
% Exemples d'algorithme
% ---------------------
%
% Pas d'algorithme au programme pour ce chapitre.


\title{\vspace{-20mm}Chapitre 2 : Limites de fonctions}
\date{\vspace{-14mm}
\qrcode[hyperlink, height=3cm]{https://erou.forge.apps.education.fr/spe-term/limites-fonctions.html}
\vspace{-14mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\begin{intuition}
  On va s'intéresser au comportement de $f(x)$ quand $x$ se rapproche d'un réel
  ou de l'infini.
\end{intuition}

\section{Limite d'une fonction en l'infini}

Soit $f$ une fonction définie sur $\mathbb{R}$ ou sur un intervalle de la forme
$[a;+\infty[$ ou $]a;+\infty[$ avec $a\in\mathbb{R}$.

\begin{defi}{Limite infinie}
  On dit que $f(x)$ tend vers $+\infty$ quand $x$ tend vers $+\infty$ et on note
  \[
    \lim_{x\to+\infty}f(x) = +\infty
  \]
  si pour tout $A\in\mathbb{R}$, il existe $m\in\mathbb{R}$ tel que si $x\geq m$ alors
  $f(x)\geq A$.\\
  On définit de manière similaire les limites
\begin{align*}
  &\lim_{x\to+\infty}f(x) = -\infty &
  &\lim_{x\to-\infty}f(x) = +\infty &
  &\lim_{x\to-\infty}f(x) = -\infty
\end{align*}
\end{defi}

\begin{intuition}
  Autrement dit, $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$ signifie que la fonction $f$
  prend des valeurs aussi grandes que l'on veut lorsque $x$ grandit.
\end{intuition}

\begin{exemple}
  \begin{minipage}{.5\textwidth}
    Soit $f:x\mapsto\sqrt x$ définie sur $[0;+\infty[$. La fonction $f$ est
      strictement croissante et si $A=100$ par exemple, on peut prendre
      $m=10\,000$. Si $x\geq10\,000$, alors 
      \begin{align*}
      \sqrt x &\geq\sqrt{10\,000} \\
      &\geq 100.
      \end{align*}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=15,
          ymax=5]
          \addplot[red, very thick, samples=201, domain=0:15]{sqrt(x)};
        \end{axis}
        \draw[thick, dashed, blue] (0.5,2) node[left]{$A$} -- (8, 2);
        \draw[thick, dashed, green!60!black] (5,0.5) node[below]{$m$} -- (5, 3);
        \fill[opacity=.2, red] (5, 2) -- (8,2) -- (8,3) -- (5, 3);
        \node[thick, red] at (.85, 1.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
  On peut faire le même raisonnement quelque soit la valeur de $A\in\mathbb{R}$, on en déduit
  donc que 
  \[
    \lim_{x\to+\infty}f(x) = +\infty.
  \]
\end{exemple}

\begin{defi}{Limite finie en l'infini}%\\[-7mm]
  On dit que $f$ a pour limite $\ell$ quand $x$ tend vers $+\infty$ et on note
  \[
    \lim_{x\to+\infty}f(x) = \ell
  \]
  si pour tout $\varepsilon>0$, il existe $m\in\mathbb{R}$ tel que si $x\geq m$
  alors $\mid f(x) - \ell\mid < \varepsilon$.\\
  On définit de manière similaire $\displaystyle\lim_{x\to-\infty}f(x)=\ell.$
\end{defi}

\begin{intuition}
  \begin{minipage}{.45\textwidth}
  Autrement dit, $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ signifie que la fonction $f$
  prend des valeurs aussi proches de $\ell$ que l'on veut lorsque $x$ grandit.
  Formellement, pour $\varepsilon>0$, il existe $m\in\mathbb{R}$ tel que
  \[
  f(x)\in]\ell-\varepsilon; \ell+\varepsilon[
  \]
  dès que $x\geq m$.
  \end{minipage}
  \begin{minipage}{.55\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=15,
          ymax=5.5]
          \addplot[red, very thick, samples=201,
          domain=0:15]{1+4*cos(deg(x))/(x+1)};
        \end{axis}
        \draw[thick, dashed, blue] (0.5,1) node[left]{$\ell$} -- (8, 1);
        \draw[thick, dashed, green!60!black] (0.5,1.25)
        node[left]{$\ell+\varepsilon$} -- (8,
        1.25);
        \draw[thick, dashed, green!60!black] (0.5,0.75)
        node[left]{$\ell-\varepsilon$} -- (8,
        .75);
        \draw[thick, dashed, orange!90!black] (3.79,0.5) node[below]{$m$} -- (3.79,
        1.25);
        \fill[opacity=.3, orange] (3.79, .75) -- (8,.75) -- (8,1.25) -- (3.79,
        1.25);
        \node[thick, red] at (1, 2.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{intuition}

\begin{exemple}~\\[-3mm]
   \begin{minipage}{.47\textwidth}
   Soit $f$ la fonction définie sur $]0;+\infty[$ par
     \[
       \forall x>0,\,f(x)=\frac{1}{x}.
     \]
     Si on prend $\varepsilon=0,2$, alors pour $m=5$ et $x\geq m$ on a
   $f(x)\in[-0,2;0,2]$. On peut montrer que ce raisonnement se généralise
   quelque soit la
 \end{minipage}
  \begin{minipage}{.53\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-.5, xmin=-1, xmax=15,
          ymax=1.5, y=2cm]
          \addplot[red, very thick, samples=201,
          domain=.1:15]{1/x};
        \end{axis}
        \draw[thick, dashed, green!60!black] (0.5,1.4) node[left]{$0,2$} -- (8,1.4);
        \draw[thick, dashed, green!60!black] (0.5,0.6) node[left]{$-0,2$} -- (8,.6);
        \draw[thick, dashed, orange!90!black] (3,0.65) node[below]{$m=5$} -- (3, 4);
        \node[thick, red] at (1, 2.25) {$\mathscr C_f$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}
   valeur de $\varepsilon>0$, ainsi on a
   $\displaystyle\lim_{x\to+\infty}\left( f(x) \right)=0$.
\end{exemple}

\begin{defi}{Asymptote horizontale}
  Si $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ ou
  $\displaystyle\lim_{x\to-\infty}f(x)=\ell$ alors on dit qu la droite
  d'équation $y=\ell$ est une \textbf{asymptote horizontale} à la courbe
  représentative de la fonction $f$.
\end{defi}

\begin{exemple}
On reprend l'exemple de la fonction $f$ définie sur $]0;+\infty[$ par $\forall
x>0,\,f(x)=\frac{1}{x}$ et on note $\mathscr C_f$ sa courbe représentative.
L'axe des abscisse est une asymptote à $\mathscr C_f$.
\end{exemple}

\section{Limite d'une fonction en une valeur réelle}

Soit $f$ une fonction définie sur un intervalle $I$ et $a\in I$ ($a$ peut aussi
être une borne de $I$).

\begin{defi}{Limite infinie}
  On dit que $f$ a pour limite $+\infty$ lorsque $x$ tend vers $a$ si, quel que
  soit le réel $A\in\mathbb{R}$, il existe $\delta>0$ tel que pour tout $x\in
  I$, si $\mid x-a\mid<\delta$, alors $f(x)>A$. On note alors
  $\displaystyle\lim_{x\to a}f(x)=+\infty$.\\[-1mm]
  On définit de manière similaire $\displaystyle\lim_{x\to a}f(x) = -\infty$.
\end{defi}

\begin{intuition}~\\[-12mm]
  \begin{minipage}{.7\textwidth}
  Autrement dit, on a $\displaystyle \lim_{x\to a}f(x)=+\infty$ si les valeurs
  de $f(x)$ deviennent aussi grandes que l'on veut dès lors que $x$ se rapproche
  de $a$.
  \end{minipage}
  \begin{minipage}{.3\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-3, xmax=5,
          ymax=5.5]
          \addplot[red, very thick, samples=201,
          domain=-5:.95]{.5*x+.5+.1/((x-1)^2)};
          \addplot[red, very thick, samples=201,
          domain=1.05:5]{.5*x+.5+.1/((x-1)^2)};
        \end{axis}
        \draw[thick, dashed, green!40!black] (2,0.35) node[below]{$a$} -- (2,3.25);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{intuition}

\begin{defi}{Asymptote verticale}
  Lorsque la limite de $f$ en un réel $a$ est $+\infty$ ou $-\infty$, on dit que
  la droite d'équation $x=a$ est une \textbf{asymptote verticale} à la courbe
  représentative de $f$.
\end{defi}

\begin{exemple}
  L'axe des ordonnées est une asymptote verticale à la courbe représentative de
  la fonction inverse.
\end{exemple}

\begin{defi}{Limite finie}
  Dire que $f$ a pour limite $\ell$ quand $x$ tend vers $a$ signifie que, quel
  que soit $\varepsilon>0$, il existe $\delta>0$ tel que pour tout $x\in I$, si
  $\mid x-a\mid<\delta$, alors $\mid f(x)-\ell\mid<\varepsilon$.
\end{defi}

\vspace{-5mm}
\begin{intuition}
  Autrement dit, les valeurs de $f(x)$ se rapprochent aussi près que l'on veut
  de $\ell$ dès lors que $x$ se rapproche de $a$.
\end{intuition}
\vspace{-3mm}

\begin{notation}
  On dit que $f$ admet une \textbf{limite à gauche} de $a$ et on note
  $\displaystyle\lim_{x\to a^-}f(x)$ lorsque $f$ admet une limite quand $x$ tend
  vers $a$ avec $x<a$.\\
  On dit que $f$ admet une \textbf{limite à droite} de $a$ et on note
  $\displaystyle\lim_{x\to a^+}f(x)$ lorsque $f$ admet une limite quand $x$ tend
  vers $a$ avec $x>a$.
\end{notation}

\section{Propriétés sur les limites}
\subsection{Limites des fonctions de référence}

\begin{propnom}{Fonction inverse}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty}\frac{1}{x}=0 &
    \bullet& \lim_{x\to-\infty}\frac{1}{x}=0 &
    \bullet& \lim_{x\to0^-}\frac{1}{x}=-\infty &
    \bullet& \lim_{x\to0^+}\frac{1}{x}=+\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Fonction puissance}
  On a les limites suivantes.
  \begin{align*}
    \bullet\;& \forall n\in\mathbb{N},\lim_{x\to+\infty}x^n=+\infty &
    \bullet\;& \text{pour $n$ pair :} \lim_{x\to-\infty}x^n=+\infty &
    \bullet\;& \text{pour $n$ impair :} \lim_{x\to-\infty}x^n=-\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Fonction exponentielle}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty}e^x = +\infty &
    \bullet& \lim_{x\to-\infty}e^x = 0 &
    \bullet& \lim_{x\to+\infty}e^{-x} = 0 &
    \bullet& \lim_{x\to-\infty}e^{-x} = +\infty
  \end{align*}
\end{propnom}

\begin{propnom}{Racine carrée}
  On a les limites suivantes.
  \begin{align*}
    \bullet& \lim_{x\to+\infty} \sqrt x = +\infty &
    \bullet& \lim_{x\to0^+} \sqrt x = 0 &
    \bullet& \lim_{x\to+\infty}\frac{1}{\sqrt x} = 0 &
    \bullet& \lim_{x\to0^+}\frac{1}{\sqrt x} = +\infty
  \end{align*}
\end{propnom}


\begin{propadm}
  Soit $a\in\mathbb{R}$ un réel.
  \begin{multicols}{2}
    \begin{itemize}[label=$\bullet$]
      \item Si $a\geq0$, $\displaystyle\lim_{x\to a}\sqrt x=\sqrt a$.
      \item $\displaystyle\lim_{x\to a}e^x=e^a$.
      \item Si $P$ est un polynôme, $\displaystyle\lim_{x\to a}P(x)=P(a)$.
      \item Si $F$ est un quotient de polynômes défini en $a$, alors
        $\displaystyle\lim_{x\to a}F(x)=F(a)$.
      \item $\displaystyle\lim_{x\to a}\cos(x)=\cos(a)$ et
        $\displaystyle\lim_{x\to a}\sin(x)=\sin(a)$.
    \end{itemize}
  \end{multicols}
\end{propadm}

\subsection{Opérations sur les limites}
\subsubsection{Limite d'une somme de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.9\textwidth}{|c|Y|Y|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell$ & $\ell$ & $+\infty$ &
    $-\infty$ & $+\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ \\
    \hline
    \textbf{alors $f+g$ a pour limite} & $\ell+\ell'$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
où \textbf{F.I.} signifie « Forme Indéterminée » et veut dire qu'il n'est pas
possible de déterminer la limite de façon générale dans ce cas là. Il faut alors
faire au cas par cas.
\end{propadm}

\begin{rmq}
  Dans la propriété ci-dessus, la limite peut avoir lieu en un réel
  $a\in\mathbb{R}$, en $a^+$, en $a^-$, ou en $\pm\infty$.
\end{rmq}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}\left( x^2+\frac{1}{x} \right) &
    \textbf{b)}\;& \lim_{x\to0^+}\left( \frac{1}{x}+\frac{1}{\sqrt x} \right) &
    \textbf{c)}\;& \lim_{x\to-\infty}\left( e^x+e^{-x} \right)
  \end{align*}
\end{app}

\subsubsection{Limite d'un produit de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell>0$ & $\ell>0$ & $\ell < 0$ &
    $\ell<0$ & $+\infty$ & $+\infty$ & $-\infty$ & $0$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & $-\infty$ & $\pm\infty$ \\
    \hline
    \textbf{alors $f\times g$ a pour limite} & $\ell\times\ell'$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ & $+\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}e^xx^2 &
    \textbf{b)}\;& \lim_{x\to-\infty}\left( \frac{1}{x}-1 \right)x^3 &
    \textbf{c)}\;& \lim_{x\to+\infty}\left( e^x\times e^{-x} \right)
  \end{align*}
\end{app}

\subsubsection{Limite d'un quotient de fonctions}
\begin{propadm}
  Soient $f$ et $g$ deux fonctions telles que $g$ ne s'annule jamais et
  soient $\ell, \ell'\in\mathbb{R}$ deux réels. On a alors les résultats
  suivants.
  \vspace{2mm}
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell$ & $\ell$ & $+\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $\ell'\neq0$ & $\pm\infty$ & $\ell'>0$ &
    $\ell'<0$ & $\ell'>0$ & $\ell'<0$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{alors $\frac{f}{g}$ a pour limite} &
    $\frac{\ell}{\ell'}$ & $0$ & $+\infty$ & $-\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} & \textbf{F.I.} \\
    \hline
  \end{tabularx}\vspace{2mm}
  \begin{tabularx}{.98\textwidth}{|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $f$ a pour limite} & $\ell>0$ ou $+\infty$ & $\ell>0$ ou
    $+\infty$ & $\ell<0$ ou $-\infty$ & $\ell<0$ ou $-\infty$ \\
    \hline
    \textbf{et $g$ a pour limite} & $0^+$ & $0^-$ & $0^+$ & $0^-$ \\
    \hline
    \textbf{alors $\frac{f}{g}$ a pour limite} & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to +\infty}\frac{e^{-x}}{1+\sqrt x} &
    \textbf{b)}\;& \lim_{x\to 2}\frac{x^2}{(x-2)^2} &
    \textbf{c)}\;& \lim_{x\to 0^+}\frac{1+\frac{1}{x}}{2+\sqrt x}
  \end{align*}
\end{app}

\begin{app}
  Soit $f$ la fonction définie sur $\mathbb{R}$ par $\forall x\in
  \mathbb{R},\,f(x)=x^3-3x+1$.
  \begin{enumerate}
    \item Peut-on utiliser les opérations sur les limites pour déterminer la
      limite de $f$ en $+\infty$ ?
    \item \begin{enumerate}
        \item Factoriser par $x^3$ dans l'expression de $f$.
        \item En déduire la limite de $f$ en $+\infty$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{methode}
  Pour lever une indéterminée, on peut factoriser ou (plus rarement) développer.
  Dans un \textbf{quotient}, on factorise le numérateur et le dénominateur par
  le terme de plus haut degré.
\end{methode}

\subsection{Limites et comparaisons}

\begin{thmnom}{Comparaison}
  Soit $f$ et $g$ deux fonctions définies sur $I=\mathbb{R}$ ou sur un intervalle
  de la forme $I=[a;+\infty[$ ou $I=]a;+\infty[$ avec $a\in\mathbb{R}$.
    \begin{itemize}
      \item Si, pour tout $x\in I$, $f(x)\geq g(x)$, et
        $\displaystyle\lim_{x\to+\infty}g(x)=+\infty$, alors
        $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$.
      \item Si, pour tout $x\in I$, $f(x)\leq g(x)$, et
        $\displaystyle\lim_{x\to+\infty}g(x)=-\infty$, alors
        $\displaystyle\lim_{x\to+\infty}f(x)=-\infty$.
    \end{itemize}
\end{thmnom}

\begin{app}
  Déterminer la limite en $+\infty$ de la fonction $f$ définie sur
  $\mathbb{R}$ par $f:x\mapsto\sqrt{x^2+2}$.
\end{app}

\begin{thmnom}{Théorème d'encadrement (ou des « gendarmes »)}
  Soit $f$, $g$ et $h$ trois fonctions définies sur $I$, où $I=\mathbb{R}$ ou
  $I$ est un intervalle de la forme $[A;+\infty[$ ou $]A;+\infty[$. On suppose
  que, pour tout $x\in I$, on a $f(x)\leq g(x)\leq h(x)$. Si, pour un réel
  $\ell\in\mathbb{R}$, on a $\displaystyle\lim_{x\to+\infty}f(x)=\ell$ et
  $\displaystyle\lim_{x\to+\infty}h(x)=\ell$, alors
  $\displaystyle\lim_{x\to+\infty}g(x)=\ell$.
\end{thmnom}

\begin{exemple}~\\[-7mm]
  \begin{minipage}{.63\textwidth}
  On considère la fonction $f$ définie sur $]0;+\infty[$ par
    \(
      f(x)=\frac{\cos(x)}{x}
    \).
    Pour $x>0$, on a $-1\leq\cos(x)\leq1$. Ainsi
    \(
      \frac{-1}{x}\leq f(x)\leq\frac{1}{x}
    \),
    et $\displaystyle\lim_{x\to+\infty}\left(\frac{-1}{x}\right)=
    \displaystyle\lim_{x\to+\infty}\left(\frac{1}{x}\right)=0$. On en déduit
    $$\displaystyle\lim_{x\to+\infty}\left(f(x)\right)=0.$$
  \end{minipage}
  \begin{minipage}{.37\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, xtick=\empty, ytick=\empty, ymin=-1, xmin=-1, xmax=62,
          ymax=1, y=2cm, x=1mm]
          \addplot[red, thick, samples=201, domain=0.1:60]{cos(deg(x))/x)};
          \addplot[green!60!black, thick, samples=201, domain=0.1:60]{1/x)};
          \addplot[green!60!black, thick, samples=201, domain=0.1:60]{-1/x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{thmnom}{Croissance comparée}
  Pour tout $n\in\mathbb{N}$,
  $\displaystyle\lim_{x\to+\infty}\frac{e^x}{x^n}=+\infty$ et
  $\displaystyle\lim_{x\to-\infty}x^ne^x=0$.
\end{thmnom}

% \subsection{Limites et fonction composée} % --> normalement se fait « en
% contexte », donc pendant des exercices j'imagine. Pas besoin de faire un point
% de cours formel là-dessus, même si c'est plus sérieux. Tout dépend du niveau
% de la classe peut-être aussi.

\end{document}
