\documentclass[11pt, twocolumn]{article}

\newcommand{\titrechapitre}{Limites de fonctions -- Exercices}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{0mm}
\renewcommand{\P}{\mathcal P}

\setlength{\columnseprule}{1pt}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin officiel
% =================
%
% Capacités attendues
% -------------------
%
% + Déterminer dans des cas simples la limite d’une suite ou d’une fonction en
%   un point, en ± ∞, en utilisant les limites usuelles, les croissances
%   comparées, les opérations sur les limites, des majorations, minorations ou
%   encadrements, la factorisation du terme prépondérant dans une somme.
% + Faire le lien entre l’existence d’une asymptote parallèle à un axe et celle
%   de la limite correspondante.
%
% TODO: ajouter des limites de type 0/0 en un point autre que 0, par exemple
% (2x^2-x-1)/(x-1) quand x tend vers 1, pour leur montrer qu'on peut factoriser
% par autre chose que x.

\begin{document}

\begin{exobox} % exo 37 livre sco
  À partir de la définition de $\displaystyle\lim_{x\to+\infty}f(x)=+\infty$,
  donner une définition de $\displaystyle\lim_{x\to+\infty}f(x)=-\infty$, et de
$\displaystyle\lim_{x\to-\infty}f(x)=+\infty$.
\end{exobox}


\begin{exobox} % exo 40 livre sco
  On donne ci-dessous la représentation graphique d'une fonction $f$ définie sur
  $\mathscr D_f$.
  \begin{enumerate}
    \item Déterminer graphiquement $\mathscr D_f$.
    \item Conjecturer le tableau de variations de $f$ sur $\mathscr D_f$, ainsi
      que les limites éventuelles de $f$ aux bornes de $\mathscr D_f$.
    \item Déterminer une équation de chacune des éventuelles asymptotes.
  \end{enumerate}
  \noindent\vspace{-6mm}
  \begin{center}
  \includegraphics[scale=.3]{limites1.png}
  \end{center}
\end{exobox}

\begin{exobox} % ex 42 livre sco
  Soit $f$ une fonction définie sur $\mathbb{R}\setminus\left\{ -1; 2
  \right\}$ dont on donne le tableau de variations ci-dessous.
  \begin{center}
    \begin{tikzpicture}
      \tkzTabInit[lgt = 1, espcl=1.6, deltacl=0.4]{$x$/1, $f(x)$/2}{$-\infty$, $-1$, $1$, $2$, $+\infty$}
      \tkzTabVar{+/$0$, -D-/$-\infty$/$-\infty$, +/$\frac{-1}{4}$,
    -D+/$-\infty$/$+\infty$, -/$0$}
    \end{tikzpicture}
  \end{center}
  \begin{enumerate}
    \item Déterminer les limites (éventuellement à droite et à gauche) de $f$ en
      $-\infty, +\infty, -1$ et $2$.
    \item Donner une équation de chaque asymptote.
    \item Tracer une représentation graphique possible de la fonction $f$.
  \end{enumerate}
\end{exobox}

\begin{exobox} % exo 44 livre sco
  Soit $f$ la fonction définie par
  \[
    \forall x\neq 2,\,f(x) = \frac{3x^2-x+1}{x^3-8}.
  \]
  \begin{enumerate}
    \item Représenter cette fonction à la calculatrice.
    \item Quelles limites peut-on conjecturer ?
    \item Donner les équations des asymptotes.
  \end{enumerate}
\end{exobox}

\begin{exobox} % exo 45 livre sco
  Soit $f$ la fonction définie par
  \[
    \forall x\in\mathbb{R},\,f(x) = x(\sin(x)+2).
  \]
  \begin{enumerate}
    \item Représenter cette fonction à la calculatrice.
    \item La fonction $f$ semble-t-elle avoir des limites en $+\infty$ et en
      $-\infty$ ?
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Calculer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}\frac{1}{x^2}+\frac{1}{x}-1 &
    \textbf{b)}\;& \lim_{x\to+\infty}x^2 - 2 \\
    \textbf{c)}\;& \lim_{x\to1}\frac{2x-1}{x+3} &
    \textbf{d)}\;& \lim_{x\to2^-}\frac{x^2+3}{x-2}
  \end{align*}
\end{exobox}

\begin{exobox}
  Déterminer les limites en $-\infty$ et en $+\infty$ des fonctions suivantes.
  \begin{align*}
    \textbf{a)}\;& f:x\mapsto x^2+\frac{1}{e^x} &
    \textbf{b)}\;& g:x\mapsto x(2x-1) \\
    \textbf{c)}\;& h:x\mapsto \frac{-1}{x-1} &
    \textbf{d)}\;& k:x\mapsto \frac{e^{-x}+1}{e^x-1}
  \end{align*}
\end{exobox}

\begin{exobox}
  Étudier les limites à gauche et à droite quand $x$ tend vers $a$ dans les
  expressions suivantes.
  \begin{align*}
    \textbf{a)}\;& \frac{1}{x-1};\;a=1 &
    \textbf{b)}\;& \frac{-1}{2-x};\;a=2 \\
    \textbf{c)}\;& \frac{-1}{(x+1)^2};\;a=-1 &
    \textbf{d)}\;& \frac{e^{-x}}{e^x-1};\;a=0
  \end{align*}
\end{exobox}

\begin{exobox}
  1.\, Minorer les fonctions suivantes par une fonction qui tend vers $+\infty$ en
  $+\infty$.
  \begin{align*}
    \textbf{a)}\;& f:x\mapsto 2x+\sin(x) &
    \textbf{b)}\;& g:x\mapsto \frac{x^2+\cos(x)}{x+1}
  \end{align*}
  \begin{enumerate}
      \setcounter{enumi}{1}
    \item En déduire leur limite en $+\infty$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  En encadrant les expressions suivantes, donner leur limite en $+\infty$.
  \begin{align*}
    \textbf{a)}\;& \frac{\cos(x)}{x+1} &
    \textbf{b)}\;& \frac{x+\sin^2(x)}{x} \\
    \textbf{c)}\;& \frac{\sin(x)+\cos(x)}{x^2} &
    \textbf{d)}\;& \frac{x^3+2}{\left( x+\cos(x) \right)^2}
  \end{align*}
\end{exobox}

\begin{exobox}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}x^4e^{-x} &
    \textbf{b)}\;& \lim_{x\to+\infty}\frac{2x^5}{e^x} \\
    \textbf{c)}\;& \lim_{x\to+\infty}e^{-x}\left( x^2+2x-5 \right) &
    \textbf{d)}\;& \lim_{x\to+\infty}\frac{x^3-2}{e^x}
  \end{align*}
\end{exobox}

\begin{exobox}
  1.\, Dans chaque cas, déterminer des fonctions $u$ et $v$ tels que $f(x)=v(u(x))$.
  \begin{align*}
    \textbf{a)}\;& f:x\mapsto\sqrt{2x^2+3} &
    \textbf{b)}\;& f:x\mapsto e^{-2x-1} \\
    \textbf{c)}\;& f:x\mapsto\cos\left( \frac{1}{x} \right) & 
    \textbf{d)}\;& f:x\mapsto\sqrt{e^{-x}}
  \end{align*}
  \begin{enumerate}
      \setcounter{enumi}{1}
    \item En déduire la limite en de $f$ en $+\infty$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Soit la fonction $f$ définie sur $\mathbb{R}\setminus\left\{ -1
  \right\}$ par
  \[
    f(x) = \frac{x^2-3x+2}{\left( 1-x \right)^2}.
  \]
  \begin{enumerate}
    \item Justifier que la limite est une forme indéterminée quand $x$ tend vers
      $1$.
    \item \begin{enumerate}
        \item Factoriser $x^2-3x+2$.
        \item Simplifier $f(x)$.
      \end{enumerate}
    \item En déduire les limites à gauche et à droite quand $x$ tend vers $1$.
  \end{enumerate}
\end{exobox}

\begin{exobox}\textbf{Quantité conjuguée.} On considère la fonction $f$ définie
  sur $[4;+\infty[$ par
  \[
    f(x)=\sqrt{x+3}-\sqrt{x-4}.
  \]
  \begin{enumerate}
    \item Peut-on directement donner la limite en $+\infty$ ?
    \item \begin{enumerate}
        \item Justifier que pour tout $x>4$,
          \[
            f(x)\times\left( \sqrt{x+3}+\sqrt{x-4} \right)=7.
          \]
        \item En déduire que $f(x)=\frac{7}{\sqrt{x+3}+\sqrt{x-4}}$.
      \end{enumerate}
    \item En déduire la limite en $+\infty$.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Donner les limites en $+\infty$ des fonctions suivantes.
  \begin{align*}
    \textbf{a)}\;& f:x\mapsto x^3-2x^2 &
    \textbf{b)}\;& g:x\mapsto \frac{x+1}{x^2-4} \\
    \textbf{c)}\;& h:x\mapsto e^x-x^5 &
    \textbf{d)}\;& k:x\mapsto \frac{3x^2+1}{x^2+1} \\
    \textbf{e)}\;& i:x\mapsto e^{2x}-e^x &
    \textbf{f)}\;& j:x\mapsto \frac{e^{3x}-e^x+1}{e^x-2e^{4x}}
  \end{align*}
\end{exobox}

\begin{exobox}
  Déterminer les limites aux bornes de son ensemble de définition de la fonction
  $f:x\mapsto\frac{x^2+1}{2-x}$.
\end{exobox}

\begin{exobox}
  Donner les limites en $+\infty$ des fonctions suivantes.
  \begin{align*}
    \textbf{a)}\;& f:x\mapsto x+\sin(x) &
    \textbf{b)}\;& g:x\mapsto x^2+2x\cos(x) \\
    \textbf{c)}\;& k:x\mapsto \frac{0,3x^3-2}{x^2-9} &
    \textbf{d)}\;& h:x\mapsto \sqrt{x+1}-\sqrt x
  \end{align*}
\end{exobox}

\begin{exobox}
  On considère la fonction $f:x\mapsto\frac{2x}{x^2-4}$ définie sur
  $\mathbb{R}\setminus\left\{ -2; 2 \right\}$.
  \begin{enumerate}
    \item Montrer que $f$ possède une asymptote horizontale et préciser son
      équation.
    \item \begin{enumerate}
        \item Calculer les limites à droite et à gauche de $f$ en $2$.
        \item La courbe de
      $f$ possède-t-elle une asymptote verticale ? Si oui, préciser son
      équation.
      \end{enumerate}
    \item La courbe de $f$ possède-t-elle une autre asymptote verticale ?
      Justifier.
    \item Dresser le tableau de variation de $f$.
    \item En déduire une allure de la courbe de $f$.
  \end{enumerate}
\end{exobox}

\begin{exobox}\textbf{D'après BAC S, Pondichéry, 2009.}
  Soit $f$ la fonction définie sur $\mathbb{R}_+$ par $f(x)=xe^{-x^2}$.
  \begin{enumerate}
    \item Déterminer la limite de la fonction $f$ en $+\infty$.
    \item Montrer que $f$ admet un maximum en $\frac{\sqrt2}{2}$.
    \item Calculer ce maximum.
  \end{enumerate}
\end{exobox}

\begin{exobox}
  Sélima prend son vélo et effectue une distance $d$ à une vitesse de $v_1=25$
  km$\cdot$h$^{-1}$. Pour le retour, elle prend un autre moyen de transport et
  parcourt la même distance $d$ à une vitesse $v_2=x$ km$\cdot$h$^{-1}$. On note
  $v(x)$ la vitesse moyenne de Sélima sur les deux trajets. On définit ainsi une
  fonction $v$.
  \begin{enumerate}
    \item Quel est l'ensemble de définition $D$ de $v$ ?
    \item Démontrer que, pour tout $x\in D$ $$v(x)=\frac{50x}{x+25}.$$
    \item Étudier les variations de $v$ sur $D$.
    \item \begin{enumerate}
        \item Déterminer les limites de $v$ aux bornes de l'ensemble de
          définition de $v$.
        \item Interpréter ces résultats. Est-ce paradoxal ?
      \end{enumerate}
  \end{enumerate}
\end{exobox}

\begin{exobox}\textbf{D'après BAC S, Polynésie, 2016.}
  Dans le cadre d'une étude visant à limiter la consommation d'alcool, on étudie
  la concentration d'alcool dans le sang d'un étudier de corpulence moyenne. La
  concentration $C$ d'alcool dans son sang est modélisée en fonction du temps
  $t$, exprimé en heure, par la fonction $f$ définie sur $\mathbb{R}_+$ par
  $f(t) = 2te^{-t}$.
  \begin{enumerate}
    \item Étudier les variations de $f$ sur $\mathbb{R}_+$.
    \item \begin{enumerate}
        \item À quel instant la concentration d'alcool dans le sang de l'étudiant
      est-elle maximale ?
    \item Quelle est alors sa valeur ? \emph{Arrondir à $10^{-2}$ près.}
      \end{enumerate}
    \item \begin{enumerate}
        \item Rappeler la limite de $\frac{e^t}{t}$ en $+\infty$.
        \item En déduire la limite de $f$ en $+\infty$.
        \item Interpréter le résultat.
      \end{enumerate}
    \item La concentration minimale d'alcool détectable dans le sang est estimée
      à $5\times10^{-3}$ g$\cdot$L$^{-1}$.
      \begin{enumerate}
        \item Justier qu'il existe un instant $T$ à partir duquel la
          concentration d'alcool dans le sang n'est plus détectable.
        \item On donne l'algorithme suivant en Python, où $f$ est la fonction
          définie par $f(t)=2te^{-t}$.
  \begin{center}
    \begin{minipage}{.25\textwidth}
\begin{python}
t = 3
p = 0.25
C = 0.21
while (C>0.005):
    t = t+p
    C = f(t)
print(t)
\end{python}
    \end{minipage}
  \end{center}
Donner les valeurs de $p$, $t$ et $C$ à l'initialisation, à l'étape $1$,
  et à l'étape $2$. \emph{Arrondir à $10^{-2}$ près.}
\item   Que représente la valeur affichée par cet algorithme ?
     \end{enumerate}
  \end{enumerate}
\end{exobox}

\begin{exobox} % ex 106 livre scolaire
  \textbf{D'après BAC S, Nouvelle-Calédonie, Février 2018.}
  Soit $f$ la fonction définie et dérivable sur $\mathbb{R}$ telle que, pour
  tout $x\in\mathbb{R}$ $$f(x)=(1+x+x^2+x^3)e^{-2x+1}.$$
  \begin{enumerate}
    \item Démontrer que $\displaystyle\lim_{x\to-\infty}f(x)=-\infty$. \emph{On pourra
        utiliser le fait que $e^{-2x+1}=\frac{e}{\left( e^x \right)^2}$.}
      \item \begin{enumerate}
          \item Démontrer que, pour tout $x>1$
            \[
              1 < x < x^2 < x^3.
            \]
          \item En déduire que, pour $x>1$
            \[
              0<f(x)<4x^3\times e^{-2x+1}.
            \]
          \item Vérifier que, pour tout réel $x$
            \[
              4x^3e^{-2x+1} = 4\times\frac{e}{e^x}\times\frac{x^3}{e^x}.
            \]
          \item En déduire que $\displaystyle\lim_{x\to+\infty}4x^3\times
            e^{-2x+1}=0$.
          \item On note $\mathcal C_f$ la courbe représentative de $f$ dans un
            repère orthonormé. En utilisant la question précédente, déterminer
            la limite de $f$ en $+\infty$.
          \item En donner une interprétation graphique.
        \end{enumerate}
  \end{enumerate}
\end{exobox}

\begin{exobox} % ex 100 livre scolaire
  Soit une fonction $f$ définie sur $\mathbb{R}\setminus\left\{ -5
  \right\}$.\\
  \textbf{Partie A : Étude graphique}\\
  On donne la représentation graphique $\mathcal C_f$ de la fonction $f$ dans le
  repère ci-dessous.
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[simple graph, xmin=-21, xmax=11, ymin=-6.4, ymax=12.4, xtick={-20.0, -18.0,
        ..., 10.0}, ytick={-10.0, -8.0, ..., 12}, xticklabels={},
      yticklabels={}, extra x ticks={-20.0, -10.0, 2.0, 10.0}, extra y
    ticks={2.0, 10.0}, extra tick style={grid=none}, x=2.5mm, y=2.5mm]
        \addplot[red, thick, samples=201,
        domain=-21:-5.01]{(e^(2*x-1)-2*cos(deg(x)))/(x+5)+2};
        \addplot[red, thick, samples=201,
        domain=-4.99:3]{(e^(2*x-1)-2*cos(deg(x)))/(x+5)+2};
      \end{axis}
      \node[red] at (6.2, 3.8) {$\mathcal C_f$};
    \end{tikzpicture}
  \end{center}
  \begin{enumerate}
    \item À l'aide de la représentation graphique, conjecturer les limites de la
      fonction $f$ en $-\infty$, en $+\infty$ et en $-5$.
    \item Interpréter graphiquement les limites de $f$ en $-\infty$ et en $-5$.
  \end{enumerate}
  \textbf{Partie B : Étude algébrique}\\
  On admet dans la suite que la fonction $f$ est définie pour tout
  $x\in\mathbb{R}\setminus\left\{ 5 \right\}$ par
  \[
    f(x) = \frac{e^{2x-1}-2\cos(x)}{x+5}+2.
  \]
  \begin{enumerate}
    \item \begin{enumerate}
        \item Justifier que $\forall x>-5,\,\frac{e^{2x-1}-2}{x+5}\leq f(x)$.
        \item En déduire la limite de $f$ en $+\infty$.
        \item Le résultat est-il cohérent avec les considérations graphiques de
          la partie A ?
      \end{enumerate}
    \item \begin{enumerate}
        \item Montrer que, pour tout réel $x<-5$
          \[
            \frac{e^{2x-1}+2}{x+5}+2\leq f(x)\leq\frac{e^{2x-1}-2}{x+5}+2.
          \]
        \item En déduire la limite de $f$ en $-\infty$.
        \item Lé résultat est-il cohérent avec les considérations graphiques de
          la partie A ?
      \end{enumerate}
    \item Déterminer les limites de $f$ en $-5$.
  \end{enumerate}
\end{exobox}

\begin{exodur} % exo 101 livre scolaire
  Soit $f$ la fonction définie et dérivable sur $D=\mathbb{R}\setminus\left\{
    \frac{1}{3}\right\}$ par $\forall x\in D,\,f(x)=\frac{3x^2+2x+2}{3x-1}$. On
    note $\mathcal C_f$ sa courbe représentative.
    \begin{enumerate}
      \item \begin{enumerate}
          \item Déterminer les limites de $f$ aux bornes de $D$.
          \item Que peut-on en déduire pour $\mathcal C_f$ ?
        \end{enumerate}
      \item Étudier les variations de $f$ sur $D$.
      \item Représenter $\mathcal C_f$ à l'aide de la calculatrice.
      \item Sur le même graphique, tracer la droite $d$ d'équation $y=x+1$.
      \item Observer ce qui se passe pour les courbes $\mathcal C_f$ et $d$
        lorsque $x$ tend vers $+\infty$, puis quand $x$ tend vers $-\infty$.
      \item Montrer que, pour tout $x\in D$
        \[
          f(x)-(x+1)=\frac{3}{3x-1}.
        \]
      \item En déduire que $\displaystyle\lim_{x\to+\infty}\left( f(x)-(x+1)
        \right)=0$ et que $\displaystyle\lim_{x\to-\infty}\left( f(x)-(x+1)
        \right)=0$.
    \end{enumerate}
    \vspace{-3mm}
    \begin{rmq}
      Dans ce cas, on dit que la droite $d$ d'équation $y=x+1$ est
      \textbf{asymptote oblique} à $\mathcal C_f$ en $+\infty$ et en $-\infty$.
    \end{rmq}
\end{exodur}

\end{document}
