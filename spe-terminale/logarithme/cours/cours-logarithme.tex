\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Fonction logarithme népérien -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Fonction logarithme népérien, notée ln, construite comme réciproque de la
%     fonction exponentielle.
% [x] Propriétés algébriques du logarithme.
% [x] Fonction dérivée du logarithme, variations.
% [x] Limites en 0 et en + ∞, courbe représentative. Lien entre les courbes
%     représentatives des fonctions logarithme népérien et exponentielle.
% [x] Croissance comparée du logarithme népérien et de x ↦ x^n en 0 et en +∞.
%
% Capacités attendues
% -------------------
%
% [ ] Utiliser l’équation fonctionnelle de l’exponentielle ou du logarithme pour
%     transformer une écriture, résoudre une équation, une inéquation.
% [ ] Dans le cadre d’une résolution de problème, utiliser les propriétés des
%     fonctions exponentielle et logarithme.
%
% Démonstrations
% --------------
%
% + Calcul de la fonction dérivée de la fonction logarithme népérien, la
%   dérivabilité étant admise.
% + Limite en 0 de x ↦ x⋅ln(x).
%
% Exemples d'algorithme
% ---------------------
%
% [ ] Algorithme de Briggs pour le calcul du logarithme.


\title{\vspace{-20mm}Chapitre 6 : Fonction logarithme népérien}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/logarithme.html}
\vspace{-7mm}}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Fonction logarithme népérien}
\subsection{Définition et premières propriétés}

\begin{propnom}{Rappels, fonction exponentielle}~\\[-5mm]
  \begin{minipage}{.5\textwidth}
  La fonction exponentielle est continue et strictement croissante sur
  $\mathbb{R}$, on a de plus $\displaystyle\lim_{x\to-\infty}e^x=0$ et $
  \displaystyle\lim_{x\to+\infty}e^x=+\infty$. D'après le théorème des valeurs
  intermédiaires, pour tout $k>0$, l'équation $e^x=k$ admet une unique solution
  dans $\mathbb{R}$.
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph, ymin=-1.5]
          \addplot[red, thick, samples=101, domain=-5.5:2] {e^x};
        \end{axis}
        \draw[blue!50!black, thick] (0, 2.5) --++ (5.5,0) node[above left]
        {$y=k$};
        \draw[blue!50!black, thick, dashed] (3.38, 2.5) -- (3.38,.75);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{propnom}

\begin{defi}{Logarithme népérien}
  Pour tout nombre réel strictement positif $a>0$, on appelle \textbf{logarithme
  népérien} de $a$ et on note $\ln(a)$ l'unique solution réelle de l'équation
  $e^x=a$. On a ainsi 
  \[
    e^x = a \Longleftrightarrow x = \ln(a).
  \]
\end{defi}
\begin{exemple}
  \begin{multicols}{2}
    \begin{itemize}[label=$\bullet$]
      \item $e^0=1\Longleftrightarrow \ln(1)=0$
      \item $e^1=e\Longleftrightarrow \ln(e)=1$
    \end{itemize}
  \end{multicols}
\end{exemple}
\begin{defi}{Fonction logarithme népérien}
  On appelle \textbf{fonction logarithme népérien} la fonction notée $\ln$,
  définie sur $]0; +\infty[$, qui à tout $x\in]0;+\infty[$, associe son
  logarithme népérien $\ln(x)$.
\end{defi}
\begin{rmq}\textbf{Attention !}
  On ne peut pas prendre le logarithme népérien d'un nombre négatif (ou nul).
\end{rmq}

\begin{prop}
Pour tout $x\in]0;+\infty[$, on a $e^{\ln(x)}=x$.
Pour tout $x\in\mathbb{R}$, on a $\ln(e^x)=x$.
\end{prop}

\begin{exemple}
  \begin{align*}
   \bullet\;& e^{\ln(42)}=42 &
   \bullet\;& \ln(e^3)=3 &
   \bullet\;& \ln(e^{-10})=-10 &
   \bullet\;& e^{\ln(2025)}=2025
  \end{align*}
\end{exemple}

\begin{app}
  Résoudre dans $\mathbb{R}$ l'équation $e^{3x+5}=2$.
\end{app}

\subsection{Étude de la fonction logarithme népérien}

\begin{prop}
  \label{prop:derivee}
La fonction $\ln$ est dérivable sur $]0;+\infty[$ et $\forall
x\in]0;+\infty[,\;\ln'(x)=\frac{1}{x}$.
\end{prop}

\begin{app}
  Dériver les fonctions définies sur $\mathbb{R}_+^*$ dont les expressions sont
  les suivantes.
  \begin{align*}
    \textbf{a)}\;& f(x) = x\ln(x)-x &
    \textbf{b)}\;& g(x) = \ln(x+1) &
    \textbf{c)}\;& h(x) = \frac{x^2}{\ln(x+1)}
  \end{align*}
\end{app}

\begin{prop}%~\\[-6mm]
  \begin{minipage}{.6\textwidth}
    \begin{itemize}[label=$\bullet$]
      \item La fonction logarithme népérien est strictement croissante sur
      $]0;+\infty[$.
      \item $\displaystyle\lim_{x\to0^+}\ln(x)=-\infty$
      \item $\displaystyle\lim_{x\to+\infty}\ln(x)=+\infty$
    \end{itemize}
    On a donc le tableau de variations suivant.
    \begin{center}
      \begin{tikzpicture}
        \tkzTabInit[lgt = 1.5, espcl=2.0]%, deltacl=0.4]
        {$x$/1, $\ln(x)$/2}{$0$, $1$, $e$, $+\infty$}
        \tkzTabVar{D-/$-\infty$, R/, R/, +/$+\infty$}
        \tkzTabIma{1}{4}{2}{$0$}
        \tkzTabIma{1}{4}{3}{$1$}
    \end{tikzpicture}
    \end{center}
  \end{minipage}
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]%, ymax=3.2]
          \addplot[red, thick, samples=201, domain=0:.2] {ln(x)};
          \addplot[red, thick, samples=101, domain=.2:5.5] {ln(x)};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{prop}
\begin{proof}
  La croissance stricte est une conséquence de la propriété~\ref{prop:derivee}.
  Les limites sont admises.
\end{proof}

\begin{prop}
  Pour tous réels $a$ et $b$ strictement positifs :
  \[
    \ln(a)=\ln(b)\Longleftrightarrow a=b\quad\text{ et
    }\quad\ln(a)>\ln(b)\Longleftrightarrow a>b
  \]
\end{prop}
\begin{proof}
  C'est une conséquence de la croissance stricte.
\end{proof}

\begin{thmnom}{Croissances comparées}
  Soit $n\in\mathbb{N}^*$ un entier strictement positif, alors
  $\displaystyle\lim_{x\to0^+}x^n\ln(x)=0$ et
  $\displaystyle\lim_{x\to+\infty}\frac{\ln(x)}{x^n}=0$. En particulier pour
  $n=1$ on a $\displaystyle\lim_{x\to0^+}x\ln(x)=0$ et
  $\displaystyle\lim_{x\to+\infty}\frac{\ln(x)}{x}=0$. 
\end{thmnom}
\begin{proof}
  On peut poser $X=\ln(x)$ et utiliser le théorème de croissances comparées avec
  la fonction exponentielle.
\end{proof}
\begin{app}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}\left( \ln (x)-3x^2 \right) &
    \textbf{b)}\;& \lim_{x\to+\infty}\left( \frac{\ln(1+x)}{x^3} \right) &
    \textbf{c)}\;& \lim_{x\to+\infty}\left( x^2\ln(x) -x^2 \right)
  \end{align*}
\end{app}

\subsection{Propriétés graphiques}

\begin{prop}%~\\[-5mm]
  \begin{minipage}{.6\textwidth}
    \begin{enumerate}
      \item Dans un repère orthonormé, la représentation graphique de la
        fonction logarithme népérien est le symétrique de la représentation
        graphique de la fonction exponentielle par rapport à la droite
        d'équation $y=x$.
      \item La fonction logarithme népérien est concave sur $]0; +\infty[$.
      \item La droite d'équation $x=0$ est une asymptote verticale à la
        représentation graphique de la fonction logarithme népérien.
    \end{enumerate}
  \end{minipage}
  \begin{minipage}{.4\textwidth}
    \begin{center}
      \begin{tikzpicture}
        \begin{axis}[simple graph]
          \addplot[blue!50!black, thick, samples=101, domain=-5.5:2] {e^x};
          \addplot[red, thick, samples=101, domain=0:0.1] {ln(x)};
          \addplot[red, thick, samples=101, domain=0.1:5.5] {ln(x)};
          \addplot[green!50!black, thick, domain=-5.5:5.5, samples=21] {x};
        \end{axis}
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{prop}
\begin{proof}
  La symétrie se déduit de la propriété $\forall x\in\mathbb{R},\; \ln(e^x)=x$.
  La fonction est concave car sa dérivée seconde est négative. L'asymptote
  verticale vient de la limite en $0^+$.
\end{proof}

\section{Propriétés algébriques}
\subsection{Relation fonctionnelle}

\begin{prop}
  Pour tous réels $a$ et $b$ strictement positifs, on a
  \[
    \ln(a\times b) = \ln(a)+\ln(b).
  \]
\end{prop}
\begin{proof}
  Soit $a, b>0$ deux réels strictement positifs. D'une part $e^{\ln(a\times
  b)}=a\times b$ par définition du logarithme. D'autre part
  $e^{\ln(a)+\ln(b)}=e^{ln(a)}\times e^{\ln(b)}=a\times b$ d'après les
  propriétés de l'exponentielle. On trouve $e^{\ln(a\times
  b)}=e^{\ln(a)+\ln(b)}$ ce qui implique $\ln(a\times b)=\ln(a)+\ln(b)$.
\end{proof}

\begin{exemple}
  \begin{align*}
    \bullet\;& \ln(42)=\ln(6\times7)=\ln(6)+\ln(7) &
    \bullet\;& \ln(80)=\ln(2\times2\times2\times2\times5)=4\ln(2)+\ln(5)
  \end{align*}
\end{exemple}

\subsection{Conséquences de la relation fonctionnelle}

\begin{prop}
  Pour tous réels $a$ et $b$ strictement positifs et pour tout entier relatif
  $k\in\mathbb{Z}$, on a
  \begin{align*}
    \textbf{1.}\;& \ln\left( \frac{1}{a} \right)=-\ln(a) &
    \textbf{2.}\;& \ln\left( \frac{a}{b} \right)=\ln(a)-\ln(b) \\
    \textbf{3.}\;& \ln\left( a^k \right)=k\ln(a) &
    \textbf{4.}\;& \ln\left( \sqrt a \right)=\frac{1}{2}\ln(a)
  \end{align*}
\end{prop}

\begin{exemple}
  \begin{align*}
    \bullet\;& \ln(0,5)=-\ln(2) &
    \bullet\;& \ln(49)=2\ln(7) &
    \bullet\;& \forall x>0,\;\ln(2\sqrt x)=\frac{1}{2}\ln(x)+\ln(2)
  \end{align*}
\end{exemple}

\section{Fonctions de la forme $\ln(u)$}

\begin{prop}
  Soit $u$ une fonction dérivable et strictement positive sur un intervalle $I$.
  On note $\ln(u)$ la fonction qui à $x$ associe $\ln(u(x))$. La fonction
  $\ln(u)$ est alors dérivable et $(\ln(u))' = \frac{u'}{u}$, c'est-à-dire
  \[
    \forall x\in I,\;\left( \ln(u) \right)'(x) = \frac{u'(x)}{u(x)}.
  \]
\end{prop}
\begin{proof}
  C'est une application de la règle pour dériver une fonction composée.
\end{proof}
\begin{app}
  Soit la fonction $f$ définie sur $\mathbb{R}$ par $f:x\mapsto\ln\left(
  3x^2+1\right)$. Calculer $f'(x)$.
\end{app}

\begin{prop}
  Soit $u$ une fonction dérivable et strictement positive sur un intervalle $I$.
  Les fonctions $u$ et $\ln(u)$ ont le même sens de variation sur $I$.
\end{prop}
\begin{proof}
  On remarque que $\ln(u)'$ a le même signe que $u'$, on en déduit alors que les
  variations sont les mêmes.
\end{proof}

\begin{appnom}{Étude d'une fonction auxiliaire}
Soit la fonction $f$ définie sur $]0;+\infty[$ par
  \[
    f(x) = \frac{x^2-2x-2-3\ln(x)}{x}.
  \]
  \begin{enumerate}
    \item Soit $\Phi$ la fonction définie sur $\mathbb{R}_+^*$ par
      $\Phi(x)=x^2-1+3\ln(x)$.
      \begin{enumerate}
        \item Calculer $\Phi(1)$ et la limite de $\Phi$ en $0$.
        \item Étudier les variations de $\Phi$ sur $\mathbb{R}_+^*$. En déduire
          le signe de $\Phi(x)$ selon les valeurs de $x$.
      \end{enumerate}
    \item \begin{enumerate}
        \item Calculer les limites de $f$ aux bornes de son ensemble de
          définition.
      \item Montrer que, sur $]0;+\infty[$, on a $f'(x)=\frac{\Phi(x)}{x^2}$.
        \item En déduire le tableau de variations de $f$.
      \end{enumerate}
  \end{enumerate}
\end{appnom}

\end{document}
