\documentclass[11pt, twocolumn]{article}

\newcommand{\titrechapitre}{Fonction logarithme népérien -- Exercices}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{0mm}

\setlength{\columnseprule}{1pt}

\input{/home/erou/cours/latex/layout-color.tex}

% Bulletin officiel
% =================
%
% Capacités attendues
% -------------------
%
% [ ] Utiliser l’équation fonctionnelle de l’exponentielle ou du logarithme pour
%     transformer une écriture, résoudre une équation, une inéquation.
% [ ] Dans le cadre d’une résolution de problème, utiliser les propriétés des
%     fonctions exponentielle et logarithme.

\begin{document}

% Équations et inéquations
% ------------------------

\begin{exobox}
  Résoudre les équations suivantes.
  \begin{align*}
    \textbf{a)}\;& \ln(2x-1) = 0 &
    \textbf{b)}\;& \ln(x-e) = 1 \\
    \textbf{c)}\;& 2\ln(x)+1 = -3 &
    \textbf{d)}\;& e^{5-2x} = 2
  \end{align*}
\end{exobox}

\begin{exobox}
  Résoudre les inéquations suivantes.
  \begin{align*}
    \textbf{a)}\;& \ln(1-x) > 0 &
    \textbf{b)}\;& \ln(3-2x)\leq 1 \\
    \textbf{c)}\;& 3e^x-1 < 8 &
    \textbf{d)}\;& e^{2x}-3e^x\geq0
  \end{align*}
\end{exobox}

\begin{exobox}
  Résoudre les équations suivantes.
  \begin{align*}
    \textbf{a)}\;& \ln(2x-1) = 2\ln(x) \\
    \textbf{b)}\;& \ln(x+1)+\ln(x-1) = \ln(4-2x)
  \end{align*}
\end{exobox}

% Tangentes et positions relatives
% --------------------------------

\begin{exobox}
  Soit $f$ la fonction définie par $f(x) = 2 \ln x + 1$ sur $]0 ; +\infty[$. On
    note $\mathscr C_f$ sa courbe représentative.

\begin{enumerate}
    \item Déterminer l'équation de la tangente $T_1$ à $\mathscr{C}_f$ au point
      d'abscisse $1$.
    \item 
    \begin{enumerate}
        \item Montrer qu’étudier la position relative de $\mathscr{C}_f$ et de sa  
              tangente $T_1$ revient à étudier le signe de $2 (\ln(x) - x + 1)$.
        \item Soit $g(x) = \ln(x) - x + 1$. Déterminer $g'(x)$.
        \item En déduire le sens de variation de $g$.
        \item Calculer $g(1)$ et en déduire le signe de $g$ sur $]0 ; +\infty[$.
        \item En déduire la position relative de $\mathscr{C}_f$ et $T_1$.
    \end{enumerate}
\end{enumerate}
\end{exobox}

\begin{exobox}
Calculer la dérivée des fonctions suivantes sans se soucier de l'ensemble de
définition/dérivabilité.
\begin{align*}
  \textbf{a)}\;& f(x) = (\ln x + 3)(x - 2) \\
  \textbf{b)}\;& f(x) = \dfrac{x - \ln x}{3 \ln x + 1} \\
  \textbf{c)}\;& f(x) = (\ln(x) - 2x + 1)^3 \\
  \textbf{d)}\;& f(x) = \sqrt{3x - x \ln(x)}
\end{align*}
\end{exobox}

\begin{exobox}
  Soit $f$ et $g$ les fonctions définies par :  
\[
f(x) = \ln(2x + 1) \quad \text{et} \quad g(x) = \ln(4 - x).
\]
Étudier la position relative des deux courbes sur $\left]-1 ; 4\right[$.
\end{exobox}

\begin{exobox}
  Soit $f$ et $g$ les fonctions définies respectivement par  
$f(x) = \ln(x^2 + 2x + 1)$ et $g(x) = \ln(-3x + 15)$.  
Étudier la position relative des deux courbes sur $]-1 ; 5[$.
\end{exobox}

\begin{exobox}
  Déterminer les limites suivantes.
  \begin{align*}
    \textbf{a)}\;& \lim_{x\to+\infty}\left( 2x\ln(x)-4 \right) \\
    \textbf{b)}\;& \lim_{x\to0}\left( x\ln(x)+\frac{3}{x} \right) \\
    \textbf{c)}\;& \lim_{x\to+\infty}\left( 5x^2\ln(x)-4x^2+1 \right) \\
    \textbf{d)}\;& \lim_{x\to3^+}\left( \frac{\ln(x-3)}{x-3}+3x \right)
  \end{align*}
\end{exobox}

\begin{exobox}
  Déterminer le plus grand ensemble de définition possible de chacune des  
fonctions suivantes, puis calculer $f'(x)$.  
    \begin{align*}
      \textbf{a)}\;& f(x) = \ln(8x - 4) &
      \textbf{b)}\;& f(x) = \ln(x^2 + x + 1) \\
      \textbf{c)}\;& f(x) = \ln(e^x - 1) &
      \textbf{d)}\;& f(x) = \ln\left(\frac{x - 1}{2x + 4}\right) 
    \end{align*}
\end{exobox}

\begin{exobox}
  Montrer que $\ln(2)+\ln(4)+\ln(8)=6\ln(2)$.
\end{exobox}

\begin{exobox}
  Exprimer $\ln(3)+\ln(27)+\ln(81)$ en fonction de $\ln(3)$.
\end{exobox}

\begin{exobox}
  Exprimer, pour tout $x > 1$, les expressions suivantes en fonction de $\ln(x)$.
\begin{enumerate}
    \item $A(x) = \ln(3x^2)$
    \item $B(x) = \ln(x + 4) - \ln(4x + x^2)$
    \item $C(x) = \ln(x^3 - x^2) - \ln(x - 1)$
    \item $D(x) = \ln\left(\dfrac{1}{x}\right) + \ln(x^2)$
    \item $E(x) = \ln\left(\sqrt{x}\right) + \ln(x^2)$
\end{enumerate}
\end{exobox}

\begin{exobox}
  On considère deux nombres réels strictement positifs $x$ et $y$.  
Simplifier l'expression suivante :  
\[
  A = \ln(x^2y) - 2\ln(\sqrt{x}\times y^5) + \ln(y^9).
\]
\end{exobox}

\begin{exobox}
  Démontrer que, pour tout $x\in\mathbb{R}$, on a
  \[
    \ln\left( e^x+1 \right)=x+\ln\left( 1+e^{-x} \right).
  \]
\end{exobox}

\begin{exodur}
  Simplifier au maximum la somme
  \[
    S=\sum_{n=1}^{100}\ln\left( \frac{n}{n+1} \right).
  \]
\end{exodur}

\begin{exobox}
  Dans chaque cas, déterminer les entiers naturels $n\in\mathbb{N}$ tels que :
\begin{align*}
  \textbf{a)}\;& \left(\frac{2}{3}\right)^n < 10^{-4} &
  \textbf{b)}\;& \left(\frac{9}{7}\right)^n \geq 10^6 \\
  \textbf{c)}\;& 1 - \left(\frac{3}{5}\right)^n \geq 0,999 & 
  \textbf{d)}\;& 0,004 > \left(\frac{8}{9}\right)^{2n}
\end{align*}
\end{exobox}

\begin{exobox}
  Yzia possède $1\,000$ euros sur son compte en banque. Chaque mois, elle
  prélève $5\%$ de la somme qu'il lui reste. Au bout de combien de mois lui
  restera-t-il moins de $500$ euros sur son compte ?
\end{exobox}

\begin{exobox}
  On note $B(x)$ le bénéfice réalisé par un artisan glacier pour la vente de $x$
centaines de litres de sorbets fabriqués. On sait que, pour tout $x \in [1; 3]$,
\[
B(x) = -10x^2 + 10x + 20x\ln(x),
\]
où $B(x)$ est exprimé en centaines d’euros.

\begin{enumerate}
    \item On note $B'(x)$ la fonction dérivée de la fonction $B$. Montrer que,
    pour tout $x \in [1; 3]$,
    \[
    B'(x) = -20x + 20 \ln(x) + 30.
    \]

    \item On donne le tableau de variations de la fonction dérivée $B'$ sur
    l’intervalle $[1; 3]$ :
    \begin{center}
      \begin{tikzpicture}
        \tkzTabInit[lgt = 1.5, espcl=3.5, deltacl=.6]
        {$x$/1, $B'(x)$/2}{$1$, $3$}
        \tkzTabVar{+/$B'(1)$, -/$B'(3)$}
      \end{tikzpicture}
    \end{center}

    \begin{enumerate}
        \item Montrer que l’équation $B'(x) = 0$ admet une unique solution $\alpha$
          dans $[1; 3]$. Donner une valeur approchée de $\alpha$ à
          $10^{-2}$.

        \item En déduire le signe de $B'(x)$ sur l’intervalle $[1; 3]$ puis dresser
        le tableau de variations de la fonction $B$.
    \end{enumerate}

    \item L’artisan glacier a décidé de maintenir sa production à l'identique
      s'il peut atteindre un bénéfice d'au moins $850$ euros. Est-ce
      envisageable ?
\end{enumerate}
\end{exobox}

\begin{exobox}
  À la mort d’un être vivant, la proportion de carbone 14 que contient son corps  
commence à décroître.  
La durée en année $t$ écoulée depuis la mort de l’organisme peut être exprimée  
en fonction de la proportion $C$ de carbone 14 dans l’organisme au temps $t$  
par la formule suivante :  
\[
t = \frac{1}{1,21 \times 10^{-4}} \ln \left(\frac{10^{-12}}{C}\right).
\]  
\begin{enumerate}  
    \item Depuis combien de temps un animal, dont la proportion en carbone 14  
    est $C = 2 \times 10^{-13}$, est-il mort ?  
    \item Exprimer $C$ en fonction de $t$.  
    \item Quelle proportion du carbone 14 initiale reste-t-il dans le corps d’un  
    être vivant mort depuis $10\,000$ ans ?  
    \item Si la quantité de carbone 14 dans un échantillon $E_1$ est le double de  
    la quantité de carbone 14 dans un échantillon $E_2$, déterminer le temps  
    écoulé entre la mort des deux êtres vivants à l’origine de ces deux échantillons.  
\end{enumerate}
\end{exobox}

\begin{exobox}
  \textbf{Partie A.} Soit $u$ la fonction définie sur $]0 ; +\infty[$ par
$u(x) = \ln(x) + x - 3$.

\begin{enumerate}  
    \item Justifier que la fonction $u$ est strictement croissante sur l’intervalle  
    $]0 ; +\infty[$.  

    \item Démontrer que l’équation $u(x) = 0$ admet une unique solution $\alpha$  
    comprise entre $2$ et $3$.  

    \item En déduire le signe de $u(x)$ en fonction de $x$.  
\end{enumerate}  
\textbf{Partie B.} Soit $f$ la fonction définie sur l’intervalle $]0 ; +\infty[$ par
$f(x) = \left(1 - \frac{1}{x}\right) [\ln(x) - 2] + 2$.

\begin{enumerate}  
    \item Calculer $\lim\limits_{x \to 0^+} f(x)$.  
    \item  
    \begin{enumerate}  
        \item Démontrer que, pour tout réel $x>0$
        \[
        f'(x) = \frac{u(x)}{x^2}.
        \]  
        \item En déduire le sens de variation de la fonction $f$ sur  
        l’intervalle $]0 ; +\infty[$.  
    \end{enumerate}  
\end{enumerate}  
\textbf{Partie C.} Soit $\mathscr{C}$ la courbe représentative de la fonction
$f$ et $\mathscr{C}'$ celle de la fonction $\ln$.
\begin{enumerate}  
    \item Montrer que, pour tout réel de l’intervalle $]0 ; +\infty[$,  
    \[
    f(x) - \ln(x) = \frac{2 - \ln(x)}{x}.
    \]  
    \item En déduire la position relative de $\mathscr{C}$ et $\mathscr{C}'$.
\end{enumerate}
\begin{flushright}
  \small D'après BAC S, Amérique du Nord, 2015.
\end{flushright}
\end{exobox}

\begin{exobox}
  Soit les fonctions $f$ et $g$ définies par :  
    \begin{align*}
\forall x>1,\; f(x) &= \ln(x - 1) + \frac{1}{x - 1}\;;  \\
\forall x>1,\; g(x) &= (\ln(x - 1))^2 + \frac{3 - 2x}{x - 1}.
    \end{align*}
    On note $\mathscr C_f$ et $\mathscr C_g$ les courbes représentatives de $f$
    et $g$ dans un repère orthonormé.
\begin{enumerate}  
    \item Dresser le tableau de variations de $f$ en déterminant les limites en $1$  
    et $+\infty$.\\ 
    \emph{Coup de pouce :} Pour la limite en $1$, penser à faire apparaître  
        une croissance comparée en mettant au même dénominateur.  
    \item  
    \begin{enumerate}  
        \item En déduire que $\mathscr{C}_f$ est toujours au-dessus de la droite  
        $d : y = 1$.  
        \item Étudier la position relative de $\mathscr{C}_f$ et $\mathscr{C}_g$  
        sur $]1 ; +\infty[$.  
    \end{enumerate}  
\end{enumerate}  
\end{exobox}

\end{document}
