\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Limites de suites -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}
\renewcommand{\P}{\mathcal P}

% Bulletin Officiel
% =================
%
% Contenus (suites)
% -----------------
%
% [x] Def suites de limite +inf
% [x] Exemple des suites croissantes non majorées
% [x] Def suites de limite -inf
% [x] Def suites de limite l
% [x] Limites et comparaisons
% [x] Théorème des gendarmes
% [x] Opérations sur les limites
% [x] Comportement d'une suite géométrique
% [x] *ADMIS* : toute suite croissante majorée converge
%
% Contenus (récurrence)
% ---------------------
%
% [x] Définition de la preuve par récurrence
%
% Capacités attendues
% -------------------
%
% [ ] Établir la convergence d'une suite, ou sa divergence vers +inf/-inf
% [ ] Raisonner par *récurrence* pour établir une propriété d'une suite
% [ ] Étudier des phénomènes d'évolution modélisables par une suite
%
% Démonstrations
% --------------
%
% + Toute suite croissante non majorée tend vers +inf
% + Limite de q^n, après preuve par récurrence de l'inégalité de Bernoulli
% + Divergence vers +inf d'une suite minorée par une suite divergeant vers +inf
% + Limite en +inf et -inf de la fonction exponentielle
%
% Exemples d'algorithme
% ---------------------
%
% [x] Recherche de seuils
% [ ] Recherche de valeurs approchées de pi, e, \sqrt 2, \ln 2, *etc.*


\title{\vspace{-20mm}Chapitre 1 : Limites de suites}
\date{\vspace{-14mm}
\href{https://erou.forge.apps.education.fr/spe-term/limites-suites.html}{
  \includegraphics[scale=.6]{qrcode.png}}
\vspace{-14mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\section{Raisonnement par récurrence}

% *Entrée en matière catchy* : la somme des n premiers nombres impairs donne
% n^2. Comment prouver ça ?

\begin{defi}{Proposition}
  Une \textbf{proposition} mathématique est un énoncé qui peut être soit vrai,
  soit faux.
\end{defi}

\begin{exemple}
  La proposition $\P$ : « Tout nombre premier est impair. » est fausse car le
  nombre $2$ est premier et n'est pas impair.
\end{exemple}

\begin{exemple}
  La proposition $\P$ : « Le carré d'un nombre réel est positif. » est vraie.
\end{exemple}

\noindent Le \textbf{raisonnement par récurrence}, ou encore \textbf{principe de
récurrence}, est utile lorsque l'on cherche à démontrer qu'une proposition est
vraie pour tout entier naturel $n$ supérieur ou égal à un certain rang
$n_0\in\mathbb{N}$.

\begin{thmnom}{Principe de récurrence}
  Soit $n_0\in\mathbb{N}$ un entier et $\P_n$ une proposition définie pour tout
  entier naturel $n\geq n_0$. Si
  \begin{enumerate}
    \item la proposition $\P_{n_0}$ est vraie ;
    \item pour tout entier $k\geq n_0$, « $\P_k$ est vraie » implique «
      $\P_{k+1}$ est vraie » ;
  \end{enumerate}
  alors on peut conclure que, pour tout entier $n\geq n_0$, la proposition
  $\P_n$ est vraie.
\end{thmnom}

\begin{proof}[Idée de la preuve]
  On suppose par l'absurde qu'il existe au moins un entier $k>n_0$ tel que $\P_k$ est
  fausse. On choisit le plus petit entier $k$ parmi eux. Alors $\P_{k-1}$ est vraie mais
  alors $\P_k$ l'est aussi d'après le point $2$ du principe de récurrence, ce
  qui est absurde.
\end{proof}

\begin{rmq}
  En pratique, on a très souvent $n_0=0$ ou $n_0=1$.
\end{rmq}

\begin{methode}
  Afin de démontrer qu'une propriété $\P_n$ est vraie pour tout entier $n\geq
  n_0$, on procède ainsi.
  \begin{enumerate}
    \item \textbf{Initialisation :} on vérifie que $\P_{n_0}$ est vraie.
    \item \textbf{Hérédité :} on suppose qu'il existe un entier $n\in\mathbb{N}$
      tel que $\P_n$ est vraie (\textbf{hypothèse de récurrence}), et on
      démontre que $\P_{n+1}$ est vraie.
    \item \textbf{Conclusion :} on conclut que $\P_n$ est vraie pour tout entier
      $n\geq n_0$.
  \end{enumerate}
\end{methode}

\begin{app}
  Démontrer que, pour tout entier naturel $n$, $n^3-n$ est un multiple de $3$.
\end{app}

\begin{propnom}{Inégalité de Bernoulli}
  Soit $x>0$ un réel strictement positif et $n\in\mathbb{N}$ un entier. On a
  \[
    (1+x)^n \geq 1+nx.
  \]
\end{propnom}
%\begin{proof}
%  Par récurrence !
%\end{proof}

\section{Notion de limite}

\begin{intuition}
  On va s'intéresser au comportement des termes $u_n$ quand $n$ est très grand.
\end{intuition}

\begin{notation}
  Lorsqu'on s'intéresse à de grandes valeurs de $n\in\mathbb{N}$, on dit qu'on
  fait \textbf{tendre} $n$ vers $+\infty$ et on note
  \[
    n\to+\infty.
  \]
\end{notation}


\subsection{Limite finie}

\begin{defi}{Limite finie}
  Une suite $(u_n)$ a pour \textbf{limite} le réel $\ell$ lorsque tout intervalle
  ouvert contenant $\ell$ contient tous les termes de la suite à partir d'un
  certain rang.
\end{defi}

\begin{rmq}
  Plus formellement, on dit que $(u_n)$ converge vers $\ell\in\mathbb{R}$ si pour tout
  $\varepsilon>0$, on peut trouver un rang $n_0\in\mathbb{N}$ tel que pour tout
  entier $n\geq n_0$,
  on a $\ell-\varepsilon < u_n < \ell+\varepsilon$, ou encore
  $u_n\in]\ell-\varepsilon; \ell+\varepsilon[$.
\end{rmq}

\begin{notation}
  Lorsqu'une suite $(u_n)$ admet une limite $\ell$, on dit qu'elle
  \textbf{converge} et on note
  \[
    \lim_{n\to+\infty}u_n = \ell.
  \]
\end{notation}

\begin{exemple}%~\\[-7mm]
  \begin{minipage}[]{.5\textwidth}
    La suite $(u_n)$ représentée ci-contre semble avoir pour limite $\ell$.

    \begin{intuition}
    Autrement dit, on peut trouver un rang $n_0$ pour lequel les termes de la
    suite sont aussi proches que l'on veut de $\ell$.
    \end{intuition}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \includegraphics[scale=.4]{convergence.png}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{propadm}
  Si une suite $(u_n)$ a pour limite le réel $\ell$, alors cette limite est
  unique.
\end{propadm}

\begin{proof}[Idée de la preuve]
  On suppose qu'une suite $(u_n)$ a deux limites distinctes $\ell$ et $\ell'$.
  On construit deux intervalles ouverts disjoints $I$ et $J$,
  contenant respectivement $\ell$ et $\ell'$. À partir d'un certain rang, les
  termes $u_n$ doivent être à la fois dans $I$ et $J$, ce qui est impossible.
\end{proof}

\subsection{Limite infinie}

\begin{defi}{Suite de limite $+\infty$}
  Une suite $(u_n)$ a pour limite $+\infty$ lorsque, pour tout réel
  $A\in\mathbb{R}$, l'intervalle $[A; +\infty[$ contient tous les termes de la
    suite à partir d'un certain rang.
\end{defi}

\vspace{-1mm}
\begin{rmq}
  Plus formellement, on dit que $(u_n)$ a pour limite $+\infty$ si pour tout réel
  $A\in\mathbb{R}$, on peut trouver un rang $n_0\in\mathbb{N}$ tel que, pour tout entier $n\geq
  n_0$, on a $u_n\geq A$.
\end{rmq}
\vspace{-1mm}

\begin{notation}
  Lorsqu'une suite $(u_n)$ admet pour limite $+\infty$, on dit qu'elle
  \textbf{diverge} vers $+\infty$ et on note
  \[
    \lim_{n\to+\infty}u_n = +\infty.
  \]
\end{notation}

\begin{exemple}%~\\[-7mm]
  \begin{minipage}[]{.5\textwidth}
    La suite $(u_n)$ représentée ci-contre semble avoir pour limite
    $+\infty$. En effet, pour un réel $A$ choisi, on peut déterminer un rang
    $n_0\in\mathbb{N}$ à partir duquel tous les termes de la suite sont supérieurs à $A$.

    \vspace{-1mm}
    \begin{intuition}
      Les termes de la suite $(u_n)$ deviennent aussi grands que l'on souhaite.
    \end{intuition}
    \vspace{-1mm}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \includegraphics[scale=.4]{divergence+.png}
    \end{center}
  \end{minipage}
\end{exemple}

\begin{defi}{Suite de limite $-\infty$}
  Une suite $(u_n)$ a pour limite $-\infty$ lorsque, pour tout réel
  $A\in\mathbb{R}$, l'intervalle $]-\infty; A]$ contient tous les termes de la
  suite à partir d'un certain rang.
\end{defi}
\vspace{-2mm}
\begin{rmq}
  Plus formellement, on dit que $(u_n)$ a pour limite $-\infty$ si pour tout réel
  $A\in\mathbb{R}$, on peut trouver un rang $n_0\in\mathbb{N}$ tel que, pour tout entier $n\geq
  n_0$, on a $u_n\leq A$.
\end{rmq}
\vspace{-2mm}
\begin{notation}
  Lorsqu'une suite $(u_n)$ admet pour limite $-\infty$, on dit qu'elle
  \textbf{diverge} vers $-\infty$ et on note
  \[
    \lim_{n\to+\infty}u_n = -\infty.
  \]
\end{notation}

\begin{exemple}%~\\[-2mm]
  \begin{minipage}[]{.5\textwidth}
    La suite $(u_n)$ représentée ci-contre semble avoir pour limite
    $-\infty$. En effet, pour un réel $A$ choisi, on peut déterminer un rang
    $n_0\in\mathbb{N}$ à partir duquel tous les termes de la suite sont
    inférieurs à $A$.

    \begin{intuition}
      Les termes de la suite $(u_n)$ deviennent aussi petits que l'on souhaite.
    \end{intuition}
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \includegraphics[scale=.33]{divergence-.png}
    \end{center}
  \end{minipage}
\end{exemple}

\section{Propriété des limites}
\subsection{Quelques limites de référence}
\begin{prop}
  Les suites $(\sqrt n)_{n\in\mathbb{N}}$, $(n)_{n\in\mathbb{N}}$ et
  $(n^k)_{n\in\mathbb{N}}$ où $k\in\mathbb{N}^*$ est un entier non nul tendent
  vers $+\infty$ quand $n$ tend vers $+\infty$.
\end{prop}
\begin{prop}
  Les suites $\left( \cfrac{1}{\sqrt n} \right)_{n\in\mathbb{N}}$, $\left(
  \cfrac{1}{n} \right)_{n\in\mathbb{N}}$ et
  $\left( \cfrac{1}{n^k} \right)_{n\in\mathbb{N}}$ où $k\in\mathbb{N}^*$ est un entier non nul tendent
  vers $0$ quand $n$ tend vers $+\infty$.
\end{prop}
\subsection{Premières propriétés}

\begin{defi}{Suite majorée}
  Une suite $(u_n)$ est \textbf{majorée} par un réel $M$ lorsque, pour tout
  entier naturel $n$, on a $u_n\leq M$. On dit que $M$ est un \textbf{majorant}
  de $(u_n)$.
\end{defi}

\begin{defi}{Suite minorée}
  Une suite $(u_n)$ est \textbf{minorée} par un réel $m$ lorsque, pour tout
  entier naturel $n$, on a $u_n\geq m$. On dit que $m$ est un \textbf{minorant}
  de $(u_n)$.
\end{defi}

\begin{defi}{Suite bornée}
  Une suite $(u_n)$ est dite \textbf{bornée} lorsqu'elle est à la fois majorée
  et minorée. Dans ce cas, il existe deux réels $m$ et $M$ tels que, pour tout
  entier naturel $n$, on a $m\leq u_n\leq M$.
\end{defi}

\begin{exemple}
  La suite $(u_n)$ définie, pour tout $n\in\mathbb{N}$, par $u_n=\cos(n)$,
  vérifie, pour tout $n\in\mathbb{N}$, que $-1\leq u_n\leq 1$. Cette suite est
  minorée par $-1$ (mais aussi $-3$, ou $-42$) et majorée par $1$ (ou encore
  par $13$ ou $1,314$). C'est donc une suite bornée.
\end{exemple}

\begin{app}
  On considère la suite $(u_n)$ définie sur $\mathbb{N}$ par $u_n = n^2$. Cette
  suite est-elle majorée ? Minorée ? Bornée ?
\end{app}

\begin{app}
  Donner un exemple de 
  \begin{enumerate}
    \item suite minorée mais pas majorée ;
    \item suite majorée mais pas minorée ;
    \item suite bornée ;
    \item suite ni majorée ni minorée.
  \end{enumerate}
\end{app}

\begin{prop}
  Toute suite croissante non majorée a pour limite $+\infty$ quand $n$ tend vers
  $+\infty$.
\end{prop}

\begin{prop}
  Toute suite décroissante non minorée a pour limite $-\infty$ quand $n$ tend vers
  $+\infty$.
\end{prop}

\begin{thmadm}
  Une suite croissante et majorée converge. De même, une suite décroissante et
  minorée converge.
\end{thmadm}

\begin{app}
  Soit $(u_n)$ la suite définie par $u_0=2$ et, pour tout $n\in\mathbb{N}$
  entier naturel, $u_{n+1}=\frac{1}{2}u_n+2$.
  \begin{enumerate}
    \item Prouver par récurrence que pour tout $n\in\mathbb{N}$, on a $2\leq
      u_n\leq 4$.
    \item Montrer que la suite $(u_n)$ est croissante.
    \item En déduire que la suite $(u_n)$ converge.
  \end{enumerate}
\end{app}

\subsection{Opération sur les limites}
\subsubsection{Limite d'une somme de suites}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants :

\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.9\textwidth}{|c|Y|Y|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell$ & $\ell$ & $+\infty$ &
    $-\infty$ & $+\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ \\
    \hline
    \textbf{alors $(u_n + v_n)$ a pour limite} & $\ell+\ell'$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
où \textbf{F.I.} signifie « Forme Indéterminée » et veut dire qu'il n'est pas
possible de déterminer la limite de façon générale dans ce cas là. Il faut alors
faire au cas par cas.
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=\frac{1}{n}$ et $v_n=n^2$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(u_n+v_n)$.
    \item Déterminer la limite de la suite $\left( 3+\frac{1}{\sqrt n} \right)$.
  \end{enumerate}
\end{app}

\subsubsection{Limite d'un produit de suites}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites et soient $\ell, \ell'\in\mathbb{R}$
  deux réels. On a alors les résultats suivants.

\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell>0$ & $\ell>0$ & $\ell < 0$ &
    $\ell<0$ & $+\infty$ & $+\infty$ & $-\infty$ & $0$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'$ & $+\infty$ & $-\infty$ & $+\infty$ &
    $-\infty$ & $+\infty$ & $-\infty$ & $-\infty$ & $\pm\infty$ \\
    \hline
    \textbf{alors $(u_n \times v_n)$ a pour limite} & $\ell\times\ell'$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ & $+\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} \\
    \hline
  \end{tabularx}
\end{center}
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=\frac{1}{n}-2$ et $v_n=\sqrt n$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(u_n\times v_n)$.
    \item Déterminer la limite de la suite $(-1\times n)$.
  \end{enumerate}
\end{app}

\subsubsection{Limite d'un quotient de suites}
\begin{notation}
  Soit $(u_n)$ une suite. On note
  \[
    \lim_{n\to+\infty}(u_n) = 0^+ \quad\text{(respectivement
    }\lim_{n\to+\infty}(u_n)=0^-\text{)}
  \]
  lorsque $\displaystyle\lim_{n\to+\infty}(u_n)=0$ et $u_n>0$ (respectivement $u_n<0$) à partir d'un
  certain rang.
\end{notation}
\begin{exemple}
  On a $\displaystyle\lim_{n\to+\infty}\left( \frac{1}{n} \right)=0^+$ et
  $\displaystyle\lim_{n\to+\infty}\left( \frac{-1}{n} \right)=0^-$. Mais
  $\displaystyle\lim_{n\to+\infty}\left( \frac{(-1)^n}{n} \right)=0$.
\end{exemple}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites telles que $(v_n)$ ne s'annule jamais et
  soient $\ell, \ell'\in\mathbb{R}$ deux réels. On a alors les résultats
  suivants.
  \vspace{2mm}
\begin{center}
  \renewcommand{\arraystretch}{1.3}
  \begin{tabularx}{.98\textwidth}{|c|c|c|c|c|c|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell$ & $\ell$ & $+\infty$ & $+\infty$ &
    $-\infty$ & $-\infty$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $\ell'\neq0$ & $\pm\infty$ & $\ell'>0$ &
    $\ell'<0$ & $\ell'>0$ & $\ell'<0$ & $0$ & $\pm\infty$ \\
    \hline
    \textbf{alors $\left( \frac{u_n}{v_n} \right)$ a pour limite} &
    $\frac{\ell}{\ell'}$ & $0$ & $+\infty$ & $-\infty$ & $-\infty$ & $+\infty$ &
    \textbf{F.I.} & \textbf{F.I.} \\
    \hline
  \end{tabularx}\vspace{2mm}
  \begin{tabularx}{.98\textwidth}{|c|Y|Y|Y|Y|}
    \hline
    \textbf{Si $(u_n)$ a pour limite} & $\ell>0$ ou $+\infty$ & $\ell>0$ ou
    $+\infty$ & $\ell<0$ ou $-\infty$ & $\ell<0$ ou $-\infty$ \\
    \hline
    \textbf{et $(v_n)$ a pour limite} & $0^+$ & $0^-$ & $0^+$ & $0^-$ \\
    \hline
    \textbf{alors $\left( \frac{u_n}{v_n} \right)$ a pour limite} & $+\infty$ &
    $-\infty$ & $-\infty$ & $+\infty$ \\
    \hline
  \end{tabularx}

\end{center}
\end{propadm}

\begin{app}
  Soient $(u_n)$ et $(v_n)$ deux suites définies, pour tout entier naturel par
  $u_n=n^3$ et $v_n=-5+\frac{1}{n}$.
  \begin{enumerate}
    \item Déterminer la limite de la suite $(\frac{u_n}{v_n})$.
    \item Déterminer la limite de la suite $w$ définie par
      $w_n=\cfrac{\frac{5}{n}+7}{8+\frac{2}{n}}$.
  \end{enumerate}
\end{app}

\begin{app}
  Soit $(u_n)$ la suite définie sur $\mathbb{N}$ par $u_n = n^2 - n$.
  \begin{enumerate}
    \item Peut-on déterminer la limite de la suite $(u_n)$ en utilisant les
      opérations sur les limites ?
    \item \begin{enumerate}
        \item Factoriser l'expression $u_n=n^2-n$.
        \item En déduire la limite de la suite $u$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{app}
  Soit $(u_n)$ la suite définie sur $\mathbb{N}$ par $u_n =
  \cfrac{4n^2}{n+1}$.
  \begin{enumerate}
    \item Peut-on déterminer la limite de la suite $(u_n)$ en utilisant les
      opérations sur les limites ?
    \item \begin{enumerate}
        \item Mettre en facteur $n$ au dénominateur de $u_n$ et prouver que
          $u_n=\cfrac{4n}{1+\frac{1}{n}}$.
        \item En déduire la limite de la suite $u$.
      \end{enumerate}
  \end{enumerate}
\end{app}

\begin{methode}
  Pour lever une indéterminée, on peut factoriser ou (plus rarement) développer.
  Dans un \textbf{quotient}, on factorise le numérateur et le dénominateur par
  le terme de plus haut degré.
\end{methode}

\subsection{Théorèmes de comparaison}

\begin{thm}
  Soient $(u_n)$ et $(v_n)$ deux suites. On suppose qu'il existe un entier $N$
  tel pour tout $n>N$, on a $u_n\leq v_n$.
  \begin{itemize}
    \item Si $\displaystyle\lim_{n\to+\infty}(u_n) = +\infty$, alors
      $\displaystyle\lim_{n\to+\infty}(v_n)=+\infty$.
    \item Si $\displaystyle\lim_{n\to+\infty}(v_n) = -\infty$, alors
      $\displaystyle\lim_{n\to+\infty}(u_n)=-\infty$.
  \end{itemize}
\end{thm}

\begin{app}
  Déterminer la limite de la suite $(u_n)$ définie sur $\mathbb{N}$ par
  $u_n=n+2\sin(n)$.
\end{app}

\begin{thmnom}{Théorème des gendarmes}
  Soient $(u_n)$, $(v_n)$ et $(w_n)$ trois suites telles que $u_n\leq v_n\leq
  w_n$ à partir d'un certain rang. Si $(u_n)$ et $(w_n)$ convergent vers une
  même limite $\ell$, alors $(v_n)$ converge aussi vers $\ell$.
\end{thmnom}
\begin{proof}[Idée de la preuve]
  Intuitivement, c'est facile : $v$ est au milieu de deux suites qui convergent
  vers $\ell$ donc elle doit elle aussi converger vers $\ell$. Formellement
  c'est plus difficile, mais si $u_n$ et $w_n$ sont proches de $\ell$, on peut montrer
  que $v_n$ est proche de $\ell$ aussi. Faire un dessin pour s'en convaincre !
\end{proof}
\begin{app}
  Déterminer $\displaystyle\lim_{n\to+\infty}\left( 1+\cfrac{\sin(n)}{n} \right)$.
\end{app}
\begin{propadm}
  Soient $(u_n)$ et $(v_n)$ deux suites convergentes, telles que $u_n\leq v_n$ à
  partir d'un certain rang. On note $\displaystyle\lim_{n\to+\infty}(u_n)=\ell$ et
  $\displaystyle\lim_{n\to+\infty}(v_n)=\ell'$. Alors $\ell\leq\ell'$.
\end{propadm}

\subsection{Le cas des suites géométriques}
% TODO: à placer plus tôt sans doute, ou alors ne pas le mettre dans les
% exercices...
\begin{propnom}{Limite d'une suite géométrique}
  Soit $q\in\mathbb{R}$ un réel.
  \begin{itemize}
    \item Si $q>1$, alors $\displaystyle\lim_{n\to+\infty}q^n=+\infty$.
    \item Si $q=1$, alors $\displaystyle\lim_{n\to+\infty}q^n=1$.
    \item Si $-1 < q < 1$, alors $\displaystyle\lim_{n\to+\infty}q^n = 0$.
    \item Si $q\leq-1$, alors la suite $\left( q^n \right)$ n'a pas de limite.
  \end{itemize}
\end{propnom}

\begin{app}
  Déterminer la limite des suites dont le terme général, pour $n\in\mathbb{N}$, est le suivant.
  \begin{align*}
    \textbf{1.}\;& u_n = 2+\left( \frac{4}{5} \right)^n &
    \textbf{2.}\;& v_n = 2^n-100 &
    \textbf{3.}\;& w_n = \left( \cos(0) - \sin(0) \right)^n
  \end{align*}
\end{app}

\end{document}
