\documentclass[11pt]{article}

\newcommand{\titrechapitre}{Vecteurs, droites et plans de l'espace -- Cours}
\newcommand{\titreclasse}{Lycée Pierre Mendès France}
\newcommand{\pagination}{\thepage/\pageref{LastPage}}
\newcommand{\topbotmargins}{2cm}
\newcommand{\spacebelowexo}{5mm}

\input{/home/erou/cours/latex/layout-color.tex}

\newcommand{\vu}{\vect{u}}
\newcommand{\vv}{\vect{v}}
\newcommand{\vw}{\vect{w}}
\newcommand{\vi}{\vect{i}}
\newcommand{\vj}{\vect{j}}
\newcommand{\vk}{\vect{k}}

% Bulletin Officiel
% =================
%
% Contenus
% --------
%
% [x] Vecteurs de l’espace. Translations.
% [x] Combinaisons linéaires de vecteurs de l’espace.
% [x] Droites de l’espace. Vecteurs directeurs d’une droite. Vecteurs
%     colinéaires.
% [x] Caractérisation d’une droite par un point et un vecteur directeur.
% [x] Plans de l’espace. Direction d’un plan de l’espace.
% [x] Caractérisation d’un plan de l’espace par un point et un couple de
%     vecteurs non colinéaires.
% [x] **Ajouts des livres** résultats de parallélismes
% [x] Bases et repères de l’espace. Décomposition d’un vecteur sur une base.
%
% Capacités attendues
% -------------------
%
% [ ] Représenter des combinaisons linéaires de vecteurs donnés.
% [ ] Exploiter une figure pour exprimer un vecteur comme combinaison linéaire
%     de vecteurs.
% [ ] Décrire la position relative de deux droites, d’une droite et d’un plan,
%     de deux plans.
% [ ] Lire sur une figure si deux vecteurs d’un plan, trois vecteurs de
%     l’espace, forment une base.
% [ ] Lire sur une figure la décomposition d'un vecteur dans une base.
% [ ] Étudier géométriquement des problèmes simples de configurations dans
%     l’espace (alignement, colinéarité, parallélisme, coplanarité).
%
% Démonstrations
% --------------
%
% **Pas de démonstration exigible dans ce chapitre.**
%
% Exemples d'algorithme
% ---------------------
%
% **Pas d'algorithme dans ce chapitre.**
%


\title{\vspace{-20mm}Chapitre 5 : Vecteurs, droites et plans de l'espace}
\date{\vspace{-14mm}
\qrcode[hyperlink,
height=5cm]{https://erou.forge.apps.education.fr/spe-term/vecteurs-espace.html}
\vspace{-7mm}}

%\title{Chapitre 1 : Limites de suites}
%\date{}
%\author{}

\begin{document}
\maketitle\thispagestyle{fancy}

\begin{intuition}
  On va généraliser les notions de géométrie du plan (vecteurs, droites, repère,
  \emph{etc.}) à l'espace.
\end{intuition}

\section{Vecteurs, droites et plans de l'espace}
\subsection{Vecteurs}

\begin{defi}{Translation et vecteur}
  Soit $A$ et $B$ deux points de l'espace. La transformation qui à tout point
  $M$ de l'espace associe l'unique point $M'$ tel que $ABM'M$ soit un
  parallélogramme s'appelle la \textbf{translation} de \textbf{vecteur} $\vect{AB}$.
\end{defi}

\begin{notation}
  Comme dans le plan, si $ABM'M$ est un parallélogramme, les vecteurs
  $\vect{AB}$ et $\vect{MM'}$ sont égaux et on note alors
  \[
    \vect{AB}=\vect{MM'}.
  \]
  Les vecteurs $\vect{AB}$ et $\vect{MM'}$ sont deux représentants d'un même
  vecteur unique $\vect u$.
\end{notation}

\noindent
\begin{minipage}{.6\textwidth}
\begin{exemple}
  Dans le cube $ABCDEFGH$ ci-contre, les vecteurs $\vect{AC}$ et $\vect{EG}$
  sont égaux car $ACGE$ est un rectangle. 
\end{exemple}
\begin{app}
  Citer trois autres paires de vecteurs égaux.
\end{app}
\end{minipage}
\begin{minipage}{.4\textwidth}
\begin{center}
  \href{https://www.geogebra.org/m/zpt2jxwd}{
  \begin{tikzpicture}[3d]
    \draw (0, 3, 0) node[right] {$F$} -- (0,3,3) node[right] {$G$} -- (0, 0, 3)
    node[left] {$H$};
    \draw (3,0,0) node[left] {$A$} -- (3, 3, 0) node[right] {$B$} -- (3,3,3)
    node[right] {$C$} -- (3, 0, 3) node[left] {$D$} -- (3,0,0);
    \draw (3, 3, 0) -- (0, 3, 0);
    \draw (3, 3, 3) -- (0, 3, 3);
    \draw[dashed] (3, 0, 0) -- (0, 0, 0) node[left] {$E$} -- (0,0,3);
    \draw[dashed] (0, 0, 0) -- (0,3,0);
    \draw (3, 0, 3) -- (0, 0, 3);
  \end{tikzpicture}
}
\end{center}
\end{minipage}

\begin{defi}{Combinaison linéaire}
  Soit $\vu$, $\vv$ et $\vw$ trois vecteurs de l'espace. On dit que $\vw$ est
  une \textbf{combinaison linéaire} de $\vu$ et $\vv$ s'il existe deux réels
  $\alpha$ et $\beta$ avec
  \[
    \vw = \alpha\vu+\beta\vv.
  \]
\end{defi}

\begin{defi}{Vecteurs colinéaires, vecteurs coplanaires}
  Soit $\alpha\in\mathbb{R}$. Si on a deux vecteurs $\vw$ et $\vu$ tels que $\vw
  = \alpha\vu$ alors on dit que $\vu$ et $\vw$ sont \textbf{colinéaires}. Soit
  de plus $\beta\in\mathbb{R}$. Si on a trois vecteurs $\vw$, $\vu$ et $\vv$
  tels que $\vw = \alpha\vu+\beta\vv$, on dit que $\vu$, $\vv$ et $\vw$ sont
  \textbf{coplanaires}.
\end{defi}

\begin{intuition}
  Deux vecteurs colinéaires sont « sur une même ligne » et trois vecteurs
  coplanaires sont «~sur un même plan~».
\end{intuition}

\begin{exemple}
\begin{minipage}{.4\textwidth}
  Soit $ABCDEFGH$ un cube et $I$ le centre du cube, c'est-à-dire le point
  d'intersection des grandes diagonales du cube. Les vecteurs $\vect{AB}$,
  $\vect{BF}$ et $\vect{AF}$ sont coplanaires, on a par ailleurs
  \[
    \vect{AF} = \vect{AB}+\vect{BF}.
  \]
  Les vecteurs $\vect{DF}$ et $\vect{FI}$ sont colinéaires, et on a
  \[
    \vect{FI} = -\frac{1}{2}\vect{DF}.
  \]
\end{minipage}
\begin{minipage}{.6\textwidth}
\begin{center}
  \href{https://www.geogebra.org/m/zxujwhhx}{
  \begin{tikzpicture}[3d]
    \draw (0, 3, 0) node[right] {$F$} -- (0,3,3) node[right] {$G$} -- (0, 0, 3)
    node[left] {$H$};
    \draw (3,0,0) node[left] {$A$} -- (3, 3, 0) node[right] {$B$} -- (3,3,3)
    node[right] {$C$} -- (3, 0, 3) node[left] {$D$} -- (3,0,0);
    \draw (3, 3, 0) -- (0, 3, 0);
    \draw (3, 3, 3) -- (0, 3, 3);
    \draw[dashed] (3, 0, 0) -- (0, 0, 0) node[left] {$E$} -- (0,0,3);
    \draw[dashed] (0, 0, 0) -- (0,3,0);
    \draw (3, 0, 3) -- (0, 0, 3);
    \fill[opacity=.1] (5,-2,0) -- (5, 5,0) -- (-2, 5, 0) -- (-2, -2, 0);
    \draw[opacity=.1, thick] ($(3, 0, 3)!-0.5!(0,3,0)$) -- ($(3, 0,
    3)!1.5!(0,3,0)$);
    \draw ($(3, 0, 3)!0.45!(0,3,0)$) -- ($(3, 0,3)!.55!(0,3,0)$);
    \draw ($(0, 0, 0)!0.45!(3,3,3)$) -- node[above] {$I$} ($(0, 0,0)!.55!(3,3,3)$);
  \end{tikzpicture}
}
\end{center}
\end{minipage}
\end{exemple}

\begin{prop}
  Soit $A$, $B$ et $C$ trois points de l'espace distincts. Les points $A$, $B$
  et $C$ sont alignés si et seulement si les vecteurs $\vect{AB}$ et
  $\vect{AC}$ sont colinéaires.
\end{prop}

\subsection{Droites}

\begin{defi}{Droite, vecteur directeur}
  Une droite de l'espace est définie
  \begin{itemize}
    \item soit par la donnée de deux points $A$ et $B$ distincts : la droite
      $(AB)$ est alors l'ensemble des points alignés avec $A$ et $B$.
    \item Soit par la donnée d'un point $A$ et d'un vecteur non nul $\vu$ : la
      droite est alors l'ensemble des points $M$ tels que $\vect{AM}$ et $\vu$
      soient colinéaires.
  \end{itemize}
  Dans le second cas, le vecteur $\vu$ est appelé \textbf{vecteur directeur} de
  la droite.
\end{defi}

\begin{intuition}
  Une droite est un objet de dimension $1$ : on peut y suivre une seule
  direction.
\end{intuition}

\begin{propnom}{Position relative de deux droites}
  Dans l'espace, deux droites peuvent être coplanaires ou non. Si elles sont
  coplanaires, elles appartiennent à un même plan et sont donc sécantes
  (elles ont un point d'intersection) ou parallèles (elles sont
  confondues ou n'ont aucun point d'intersection).
\end{propnom}

\begin{rmq}
  Dans l'espace, deux droites non sécantes ne sont pas forcément parallèles.
\end{rmq}

\begin{minipage}{.5\textwidth}
\begin{center}
  \textbf{Droites strictement parallèles}\\[3mm]
  \begin{tikzpicture}[3d]
    \coordinate (A) at (0,0,0);
    \coordinate (B) at (1,1,0);
    \coordinate (C) at (1,0,0);
    \coordinate (D) at (2,1,0);

    \draw[red!50!black, thick] ($(A)!-1!(B)$) node[above right] {$\Delta$} --
    ($(A)!2!(B)$);
    \draw[blue!50!black, thick] ($(C)!-1!(D)$) node[below left] {$\Delta'$} --
    ($(C)!2!(D)$);

    \draw (-2.5, -2, 0) -- (-2.5, 3, 0) -- (3.5, 3, 0) -- (3.5, -2, 0) -- (-2.5, -2, 0);
  \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.5\textwidth}
  \begin{center}
    \textbf{Droites sécantes}\\[3mm]
  \begin{tikzpicture}[3d]
    \coordinate (A) at (0,0,0);
    \coordinate (B) at (1,1,0);
    \coordinate (C) at (1,0,0);
    \coordinate (D) at (-.5,1,0);

    \draw[red!50!black, thick] ($(A)!-1!(B)$) node[above right] {$\Delta$} --
    ($(A)!2!(B)$);
    \draw[blue!50!black, thick] ($(C)!-1!(D)$) node[above left] {$\Delta'$} --
    ($(C)!2!(D)$);

    \draw (-2.5, -2, 0) -- (-2.5, 3, 0) -- (3.5, 3, 0) -- (3.5, -2, 0) -- (-2.5, -2, 0);
  \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.5\textwidth}
  \begin{center}
    \textbf{Droites confondues}\\[3mm]
  \begin{tikzpicture}[3d]
    \coordinate (A) at (0,0,0);
    \coordinate (B) at (1,1,0);
    \coordinate (C) at (1,0,0);
    \coordinate (D) at (-.5,1,0);

    \draw[red!50!black, thick, opacity=.8] ($(A)!-1!(B)$) node[above right]
    {\hspace{4mm}$\Delta$} -- ($(A)!2!(B)$);
    \draw[blue!50!black, thick, opacity=.8] ($(A)!-1!(B)$) node[above]
    {$\Delta'=$} --($(A)!2!(B)$);

    \draw (-2.5, -2, 0) -- (-2.5, 3, 0) -- (3.5, 3, 0) -- (3.5, -2, 0) -- (-2.5, -2, 0);
  \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.5\textwidth}
  \begin{center}
    \textbf{Droites non coplanaires}\\[3mm]
  \begin{tikzpicture}[3d]
    \coordinate (A) at (0,0,0);
    \coordinate (B) at (1,1,0);
    \coordinate (C) at (0,1,0);
    \coordinate (D) at (0,1,1);

    \draw[red!50!black, thick] ($(A)!-1!(B)$) node[above right] {$\Delta$} --
    ($(A)!2!(B)$);
    \draw[blue!50!black, thick] ($(C)!2!(D)$) node[below left] {$\Delta'$} --
    ($(C)!0!(D)$);
    \draw[blue!50!black, thick, dashed] ($(C)!0!(D)$) --($(C)!-1.3!(D)$);
    \draw[blue!50!black, thick] ($(C)!-1.3!(D)$) --($(C)!-1.5!(D)$);
    \draw (-2.5, -2, 0) -- (-2.5, 3, 0) -- (3.5, 3, 0) -- (3.5, -2, 0) -- (-2.5, -2, 0);
  \end{tikzpicture}
\end{center}
\end{minipage}
\noindent
\begin{minipage}{.5\textwidth}
\begin{exemple}
    Dans le cube $ABCDEFGH$, les droites $(AB)$ et $(DC)$ sont parallèles. Les
    droites $(AB)$ et $(GF)$ sont non coplanaires. Les droites $(AB)$ et $(BC)$
    sont sécantes.
\end{exemple}
\begin{app}
  Quelle est la relation (sécantes, parallèles, non coplanaires) entre 
  \begin{enumerate}
    \item $(DH)$ et $(AF)$ ?
    \item $(AH)$ et $(BG)$ ?
    \item $(BG)$ et $(CF)$ ?
  \end{enumerate}
\end{app}
\end{minipage}
\begin{minipage}{.5\textwidth}
\begin{center}
  \begin{tikzpicture}[3d]
    \coordinate (A) at (3,0,0);
    \coordinate (B) at (3,3,0);
    \coordinate (C) at (3,3,3);
    \coordinate (D) at (3,0,3);
    \coordinate (E) at (0,0,0);
    \coordinate (F) at (0,3,0);
    \coordinate (G) at (0,3,3);
    \coordinate (H) at (0,0,3);

    \draw (B) -- (F) node[right] {$F$} -- (G) node[right] {$G$} -- (H)
    node[left] {$H$} -- (D);
    \draw (A) node[left] {$A$} -- (B) node[right] {$B$} -- (C)
    node[right] {$C$} -- (D) node[left] {$D$} -- (A);
    \draw (C) -- (G);
    \draw[dashed] (A) -- (E) node[left] {$E$} -- (H);
    \draw[dashed] (E) -- (F);

    \draw[blue!50!black, thick, opacity=.5] ($(A)!-.5!(B)$) -- ($(A)!1.5!(B)$);
    \draw[red!50!black, thick, opacity=.5] ($(D)!-.5!(C)$) -- ($(D)!1.5!(C)$);
    \draw[green!50!black, thick, opacity=.5] ($(G)!-.5!(F)$) -- ($(G)!1.5!(F)$);
    \draw[orange!50!black, thick, opacity=.5] ($(C)!-.5!(B)$) -- ($(C)!1.5!(B)$);
  \end{tikzpicture}
\end{center}
\end{minipage}

\subsection{Plans}

\begin{defi}{Plan}
  Un \textbf{plan} de l'espace, défini par un point $A$ et deux vecteurs non
  colinéaires $\vu$ et $\vv$, est l'ensemble des points $M$ tels que
  $\vect{AM}$, $\vu$ et $\vv$ soient coplanaires. Autrement dit, c'est
  l'ensemble des points $M$ tels que
  \[
    \vect{AM} = \alpha\vu+\beta\vv
  \]
  pour $\alpha$ et $\beta$ des réels.
\end{defi}

\begin{intuition}
  Un plan est un objet de dimension $2$ : on peut y suivre $2$ directions
  orthogonales.
\end{intuition}

\begin{rmq}
  Trois points distincts $A$, $B$, et $C$ non alignés forment aussi un plan : on
  considère le plan défini par $A$ et les vecteurs $\vect{AB}$ et
  $\vect{AC}$.
\end{rmq}

\begin{propnom}{Position relative de deux plans}
  Deux plans peuvent être parallèles : soit strictement parallèles et ils n'ont
  aucun point commun, soit confondus et ils ont tous leurs points en commun.
  Deux plans non parallèles sont sécants, leur intersection est alors une
  droite.
\end{propnom}

\noindent
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{Plans strictement parallèles}\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);

      \fill[blue!50!black, opacity=.2] (0,0,1) -- (0,4,1) -- (2,4,1) -- (2,0,1);
      \draw[blue!50!black] (0,0,1) -- (0,4,1) --
      (2,4,1) -- (2,0,1) node[above left] {$\mathscr P'$} -- (0,0,1);
    \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{Plans parallèles confondus}\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black, opacity=.8] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) -- (0,0,0);

      \fill[blue!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[blue!50!black, opacity=.8] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) -- (0,0,0);
      \node at (0,2,.5) {$\textcolor{red!50!black}{\mathscr
      P}=\textcolor{blue!50!black}{\mathscr P'}$};
    \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{Plans sécants}\\
    L'intersection est une droite.\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,.6,0) (0,2,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);
      \draw[red!50!black, dashed] (0,.6,0) -- (0,2,0);

      \fill[blue!50!black, opacity=.2] (0,1,1) -- (0,3,-1) -- (2,3,-1) -- (2,1,1);
      \draw[blue!50!black] (0,2.7, -.7) -- (0,3,-1) -- (2, 3, -1) -- (2,1,1) -- (0,1,1) -- (0,2,0);
      \draw[blue!50!black, dashed] (0,2.7, -.7) -- (0,2,0);

      \draw[thick, dotted] (0,2,0) -- (2,2,0);
    \end{tikzpicture}
  \end{center}
\end{minipage}

\begin{propnom}{Position relative d'une droite et d'un plan}
  Une droite $\Delta$ et un plan $\mathscr P$ peuvent être parallèles : soit
  strictement parallèles ils n'ont aucun point commun, soit la droite $\Delta$
  est inclue dans le plan $\mathscr P$. Si $\Delta$ et $\mathscr P$ ne sont pas
  parallèles, ils sont sécants et alors leur intersection est un point.
\end{propnom}

\noindent
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{$\mathscr P$ et $\Delta$ strictement parallèles}\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);

      \draw[blue!50!black] (0,1,.5) -- (0,3,.5) node[below right] {$\Delta$};
    \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{$\Delta$ incluse dans $\mathscr P$}\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black, opacity=.8] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) -- (0,0,0);
      \draw[blue!50!black, dashed] (1,1,0) -- (.5,3,0) node[right] {$\Delta$};

    \end{tikzpicture}
  \end{center}
\end{minipage}
\begin{minipage}{.33\textwidth}
  \begin{center}
    \textbf{$\mathscr P$ et $\Delta$ sont sécants.}\\
    L'intersection est un point.\\[3mm]
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);

      \draw[blue!50!black] (1,1,1) -- (1,2,0) (1,2.35,-.35) -- (1,3,-1);
      \draw[blue!50!black, dashed] (1,2,0) -- (1,2.35,-.35);

      \draw (.8,2,0) -- (1.2,2,0) (1,1.9,0)--(1,2.1,0);
    \end{tikzpicture}
  \end{center}
\end{minipage}

\subsection{Parallélismes}

\begin{thmnom}{Parallélisme entre une droite et un plan}~\\[-8mm]
  \begin{minipage}[b]{.6\textwidth}
  Une droite $\Delta$ est parallèle à un plan $\mathscr P$ si et seulement si il
  existe une droite $\Delta'$ du plan $\mathscr P$ parallèle à $\Delta$.
  \end{minipage}
  \begin{minipage}[b]{.4\textwidth}
    \begin{center}
    \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black, opacity=.8] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) -- (0,0,0);
      \draw[blue!50!black] (1,1,0) -- (.5,3,0) node[right] {$\Delta'$};
      \draw[green!40!black] (1,1,.8) -- (.5,3,.8) node[right] {$\Delta$};
      \node[red!50!black] at (2,0,.5) {$\mathscr P$};
    \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{thmnom}

\begin{thmnom}{Parallélisme entre deux plans}~\\[-3mm]
  \begin{minipage}{.6\textwidth}
    Un plan $\mathscr P'$ est parallèle à un plan $\mathscr P$ si et seulement si
    il existe deux droites sécantes de $\mathscr P'$ parallèles à deux droites
    sécantes de $\mathscr P$.
  \end{minipage}
  \begin{minipage}{.4\textwidth}
   \begin{center}
     \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);
      \draw[red!50!black] (1.5,1,0) -- (1,3,0) node[right] {$\Delta$};
      \draw[red!50!black] (0.5,0.5,0) node[left] {$d$} -- (1.5,2.5,0);

      \fill[blue!50!black, opacity=.2] (0,0,1) -- (0,4,1) -- (2,4,1) -- (2,0,1);
      \draw[blue!50!black] (0,0,1) -- (0,4,1) --
      (2,4,1) -- (2,0,1) node[above left] {$\mathscr P'$} -- (0,0,1);
      \draw[blue!50!black] (1.5,1,1) -- (1,3,1) node[right] {$\Delta'$};
      \draw[blue!50!black] (0.5,0.5,1) node[left] {$d'$} -- (1.5,2.5,1);
    \end{tikzpicture}
   \end{center}
  \end{minipage}
\end{thmnom}

\begin{thmnom}{Coupe de deux plans parallèles}~\\[-8mm]
  \begin{minipage}{.5\textwidth}
  Soit $\mathscr P$ et $\mathscr P'$ deux plans strictement parallèles. Tout
  plan $\pi$ qui coupe l'un de ces plans coupe l'autre et les droites
  d'intersection obtenus sont parallèles.  
  \end{minipage}
  \begin{minipage}{.5\textwidth}
    \begin{center}
      \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,.6,0) (0,2,0) -- (0,4,0) --
      (2,4,0) -- (2,0,0) node[above left] {$\mathscr P$} -- (0,0,0);
      \draw[red!50!black, dashed] (0,.6,0) -- (0,2,0);

      \fill[red!50!black, opacity=.2] (0,0,1) -- (0,4,1) -- (2,4,1) -- (2,0,1);
      \draw[red!50!black] (0,1,1) -- (0,4,1) --
      (2,4,1) -- (2,0,1) node[above left] {$\mathscr P'$} -- (0.57,0,1);
      \draw[red!50!black, dashed] (.57,0,1) -- (0,0,1) -- (0,1,1);

      \fill[blue!50!black, opacity=.2] (0,0,2) -- (0,3,-1) -- (2,3,-1) -- (2,0,2);
      \draw[blue!50!black] (0,2.7, -.7) -- (0,3,-1) -- (2, 3, -1) -- (2,0,2) --
      (0,0,2) -- (0,1,1);% -- (0,2,0);
      \draw[blue!50!black] (0,2,0) -- (0,1.7,.3);
      \draw[blue!50!black, dashed] (0,2.7, -.7) -- (0,2,0) (0,1.7,.3) -- (0,1,1);

      \draw[thick, dotted] (0,2,0) -- (2,2,0);
      \draw[thick, dotted] (0,1,1) -- (2,1,1);

      \node[blue!50!black] at (0,.5,1.9) {$\pi$};
    \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{thmnom}

\begin{thmnom}{Théorème du toit}
\href{https://www.geogebra.org/m/qyknZsWb}{
 Si deux plans sécants $\mathscr P$ et $\mathscr P'$ contiennent respectivement
 deux droites $d$ et $d'$ parallèles entre elles, alors leur intersection
 $\Delta$ est parallèle à $d$ et $d'$.
}
\end{thmnom}

\section{Repère de l'espace}
\subsection{Décomposition de vecteurs}

\begin{propnom}{Relation de Chasles}~\\[-5mm]
  \begin{minipage}{.7\textwidth}
  Comme dans le plan, si $A$, $B$ et $C$ sont trois points de l'espace, on a
  \[
    \vect{AB} + \vect{BC} = \vect{AC}.
  \]
  \end{minipage}
  \begin{minipage}{.3\textwidth}
    \begin{center}
      \begin{tikzpicture}[scale=.5]
        \draw[blue, thick, -latex] (0,0) -- node[above left] {$\vect{BC}$} (-2, -1);
     \draw[red, thick, -latex] (2,-2) -- node[below] {$\vect{AC}$} (-2, -1);
     \draw[green!50!black, thick, -latex] (2,-2) -- node[above right]
     {$\vect{AB}$} (0, 0);

      \node at (0,0.2) {$B$};
      \node at (-2.3,-1) {$C$};
      \node at (2.2,-2) {$A$};
    \end{tikzpicture}
 \end{center}
  \end{minipage}
\end{propnom}

\begin{defi}{Base}~\\[-5mm]
  \begin{minipage}{.7\textwidth}
  Trois vecteurs $\vi$, $\vj$ et $\vk$ forment une \textbf{base} de l'espace si
  aucun de ces trois vecteurs ne peut s'exprimer comme combinaison linéaire des
  deux autres.
  \end{minipage}
  \begin{minipage}{.3\textwidth}
    \begin{center}
     \begin{tikzpicture}[3d]
      \fill[red!50!black, opacity=.2] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0);
      \draw[red!50!black] (0,0,0) -- (0,4,0) -- (2,4,0) -- (2,0,0) -- (0,0,0);

      \draw[-latex] (1,1,0) -- (1.8,3,0) node[below] {$\vi$};
      \draw[-latex] (1,1,0) -- (.5,3.5,0) node[below left] {$\vj$};
      \draw[-latex] (1,1,0) -- node[above] {$\vk$} (1,3,1);
    \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{defi}

\begin{intuition}
  Trois vecteurs forment une base s'ils ne sont pas coplanaires.
\end{intuition}

\begin{prop}
  \begin{minipage}{.65\textwidth}
  Soit $(\vi, \vj, \vk)$ une base de l'espace. Tout vecteur $\vu$ peut s'écrire
  comme combinaison linéaire des vecteurs de la base :
  \[
    \vu = x\vi+y\vj+z\vk.
  \]
  Cette décomposition est unique et ont dit que
  $\begin{pmatrix}x\\y\\z\end{pmatrix}$ sont les coordonnées de $\vu$ dans la
  base $(\vi, \vj, \vk)$.
  \end{minipage}
  \begin{minipage}{.35\textwidth}
    \begin{center}
      \begin{tikzpicture}[3d]
        \coordinate (O) at (0,0,0);
        \coordinate (i) at (1,0,0);
        \coordinate (j) at (0,1,0);
        \coordinate (k) at (0,0,1);
        \coordinate (u) at (2,3,2);
        \coordinate (uxy) at (2,3,0);
        \coordinate (ux) at (2,0,0);
        \coordinate (uy) at (0,3,0);
        \coordinate (uz) at (0,0,2);


        \begin{scope}[red]
        \draw[-latex] (O) -- (ux) node[left] {$x\vi$};
        \draw[dashed] (ux) -- (uxy);
        \end{scope}
        \begin{scope}[blue!100!black]
        \draw[-latex] (O) -- (uy) node[right] {$y\vj$};
        \draw[dashed] (uy) -- (uxy);
        \end{scope}
        \begin{scope}[green!50!black]
        \draw[-latex] (O) -- (uz) node[left] {$z\vk$};
        \draw[dashed] (uz) -- (u);
        \end{scope}

        \draw[-latex] (O) -- node[left]{\small$\vi$} (i);
        \draw[-latex] (O) -- node[below]{\small$\vj$} (j);
        \draw[-latex] (O) -- node[left]{\small$\vk$} (k);
        \draw[-latex] (O) -- node[above] {$\vu$} (u);
        \draw[dashed] (u) -- (uxy);
      \end{tikzpicture}
    \end{center}
  \end{minipage}
\end{prop}

\subsection{Repère}

\begin{defi}{Repère, origine}
  On appelle \textbf{repère} $(O; \vi, \vj, \vk)$ le quadruplet où $O$ est un point de
  l'espace, appelé \textbf{origine}, et où le triplet $(\vi, \vj, \vk)$ est une
  base de l'espace.
\end{defi}

\begin{defi}{Coordonnées}
  Soit $M$ un point de l'espace et $(O; \vi, \vj, \vk)$ un repère. Il existe un
  unique triplet de réels $(x,y,z)$ tel que
  \[
    \vect{OM} = x\vi+y\vj+z\vk.
  \]
  Ces réels sont appelés les \textbf{coordonnées} de $M$ dans le repère $(O,
  \vi, \vj, \vk)$.
\end{defi}

\begin{notation}~\\[-5mm]
  \begin{minipage}{.65\textwidth}
    On note $M(x; y; z)$ ou $M\begin{pmatrix}x\\y\\z\end{pmatrix}$ les
    coordonnées de $M$. Le nombre $x$ est appelé \textbf{l'abscisse} de $M$, le
    nombre $y$ est appelé \textbf{l'ordonnée} de $M$ et le nombre $z$ est appelé
    \textbf{la cote} de $M$.
  \end{minipage}
  \begin{minipage}{.35\textwidth}
    \begin{center}
      \begin{tikzpicture}[3d]
        \coordinate (O) at (0,0,0);
        \coordinate (i) at (1,0,0);
        \coordinate (j) at (0,1,0);
        \coordinate (k) at (0,0,1);
        \coordinate (u) at (2,3,2);
        \coordinate (uxy) at (2,3,0);
        \coordinate (ux) at (2,0,0);
        \coordinate (uy) at (0,3,0);
        \coordinate (uz) at (0,0,2);


        \begin{scope}[red]
        \draw (O) -- (ux) node[left] {$x$} -- ($(O)!1.2!(ux)$);
        \draw[dashed] (ux) -- (uxy);
        \end{scope}
        \begin{scope}[blue!100!black]
        \draw (O) -- (uy) node[above] {$y$} -- ($(O)!1.2!(uy)$);
        \draw[dashed] (uy) -- (uxy);
        \end{scope}
        \begin{scope}[green!50!black]
        \draw (O) -- (uz) node[left] {$z$} -- ($(O)!1.2!(uz)$);
        \draw[dashed] (uz) -- (u);
        \end{scope}

        \draw[-latex] (O) -- node[left]{\scriptsize $\vi$} (i);
        \draw[-latex] (O) -- (j) node[below left]{\scriptsize $\vj$};
        \draw[-latex] (O) -- (k) node[left]{\scriptsize $\vk$};
        \draw[-latex] (O) -- (u) node[above right] {$M$};
        \draw[dashed] (u) -- (uxy);
        \node[above left] at (O) {\scriptsize $O$};
      \end{tikzpicture}
    \end{center}
  \end{minipage}

\end{notation}

\begin{app}
  \begin{minipage}{.75\textwidth}
    On considère le repère $(A; \vect{AB}, \vect{AE}, \vect{AD})$.
    \begin{enumerate}
      \item Donner les coordonnées de tous les sommets du cube $ABCDEFGH$.
    \end{enumerate}
    On considère maintenant $I$ le milieu de $\left[ AG \right]$ ainsi que le
    repère $(I; \frac{1}{2}\vect{EA}, \frac{1}{2}\vect{EF},
    \frac{1}{2}\vect{EH})$.
    \begin{enumerate}
        \setcounter{enumi}{1}
      \item Donner les coordonnées de tous les sommets du cube $ABCDEFGH$.
    \end{enumerate}
  \end{minipage}
\begin{minipage}{.25\textwidth}
\begin{center}
  \begin{tikzpicture}[3d, scale=.5]
    \coordinate (A) at (3,0,0);
    \coordinate (B) at (3,3,0);
    \coordinate (C) at (3,3,3);
    \coordinate (D) at (3,0,3);
    \coordinate (E) at (0,0,0);
    \coordinate (F) at (0,3,0);
    \coordinate (G) at (0,3,3);
    \coordinate (H) at (0,0,3);

    \draw (B) -- (F) node[right] {$F$} -- (G) node[right] {$G$} -- (H)
    node[left] {$H$} -- (D);
    \draw (A) node[left] {$A$} -- (B) node[right] {$B$} -- (C)
    node[right] {$C$} -- (D) node[left] {$D$} -- (A);
    \draw (C) -- (G);
    \draw[dashed] (A) -- (E) node[left] {$E$} -- (H);
    \draw[dashed] (E) -- (F);
  \end{tikzpicture}
\end{center}
\end{minipage}
\end{app}

\subsection{Opérations sur les coordonnées}
\noindent Dans toute la suite, on considère un repère $(O; \vi, \vj, \vk)$.

\begin{propnom}{Coordonnées d'un vecteur}
  Soit $A(x_A; y_A; z_A)$ et $B(x_B; y_B; z_B)$ deux points de l'espace. Alors
  le vecteur $\vect{AB}$ a pour coordonnées
  \[\vect{AB}
  \begin{pmatrix}
    x_B - x_A \\
    y_B - y_A \\
    z_B - z_A
  \end{pmatrix}.
\]
\end{propnom}

\begin{propnom}{Coordonnées d'un milieu}
  Soit $A(x_A; y_A; z_A)$ et $B(x_B; y_B; z_B)$ deux points de l'espace. On note
  $I$ le milieu de $\left[ AB \right]$. Les coordonnées de $I$ sont alors
  \[
    I\left(\frac{x_A+x_B}{2};\frac{y_A+y_B}{2};\frac{z_A+z_B}{2}\right).
  \]
\end{propnom}

\begin{propnom}{Opérations sur les coordonnées de vecteurs}
  Soit $\vu
  \begin{pmatrix}
    x\\
    y\\
    z
  \end{pmatrix}$
  et $\vv
  \begin{pmatrix}
    x'\\
    y'\\
    z'
  \end{pmatrix}$
  deux vecteurs et soit $\alpha\in\mathbb{R}$ un nombre réel. Le vecteur somme
  $\vu+\vv$ a pour coordonnées $
  \begin{pmatrix}
    x+x'\\
    y+y'\\
    z+z'
  \end{pmatrix}$
  et le vecteur $\alpha\vu$ a pour coordonnées $
  \begin{pmatrix}
    \alpha x\\
    \alpha y\\
    \alpha z
  \end{pmatrix}$.
\end{propnom}
\begin{rmq}
  Tous ces résultats sont analogues à ceux connus dans le plan : les formules
  sont les mêmes.
\end{rmq}

\subsection{Représentation paramétrique d'une droite}

\begin{prop}
  Soit $\Delta$ une droite passant par un point $A(x_A; y_A; z_A)$ et $\vu
  \begin{pmatrix}
    a\\
    b\\
    c
  \end{pmatrix}$
  un vecteur directeur de $\Delta$. Un point $M(x;y;z)$ appartient à $\Delta$ si
  et seulement si il existe un $t\in\mathbb{R}$ un réel tel que 
  $\left\{
  \begin{array}{l}
    x = at+x_A\\
    y = bt+y_A\\
    z = ct+z_A
  \end{array}\right.$.
\end{prop}

\begin{defi}{Représentation paramétrique}
  Le système d'équations
  $\left\{
  \begin{array}{l}
    x = at+x_A\\
    y = bt+y_A\\
    z = ct+z_A
  \end{array}\right.$
  avec $t$ décrivant $\mathbb{R}$ est une \textbf{représentation paramétrique}
  de la droite $\Delta$. À chaque valeur du paramètre $t\in\mathbb{R}$
  correspond un point de $\Delta$ (et réciproquement).
\end{defi}

\begin{app}
  Soit $(O;\vi,\vj\vk)$ un repère.
  \begin{enumerate}
    \item Donner une représentation paramétrique de la droite $(AB)$ où $A(1;
      -3; 1)$ et $B(-1; 1; 4)$.
    \item Les points $C(0; -1; 2,5)$ et $D(2; 4; 2)$ appartiennent-ils à $(AB)$ ?
  \end{enumerate}
\end{app}

\end{document}

