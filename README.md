# Cours

Ce projet rassemble tous les cours que j'ai donné et/ou donne encore
aujourd'hui. Certains sont sans doute désuets, d'autres ont été écrits
rapidement dans une période intense, et tous sont perfectibles.

*Vous l'aurez compris, aucune garantie ici concernant la qualité des contenus !*

## Makefile

J'utilise un makefile pour compiler mes fichiers `.tex`.

+ La commande `make` utilise pdflatex pour compiler puis visionner le `.pdf`.
+ La commande `make dys` change la police pour favoriser la lecture.

*Note :* j'utilise [evince](https://doc.ubuntu-fr.org/evince) pour visionner les
`.pdf`, mais il suffit de changer la ligne adéquate du makefile pour utiliser un
autre logiciel.
